package ec.edu.utpl.datalab.codelogs.server.domain.metrics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatMeasures;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Metric.
 */
@Entity
@Table(name = "metric")
public class Metric implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid",unique = true)
    private String uuid;

    @Column(name = "name")
    private String name;

    @Column(name = "decription")
    private String decription;

    @Column(name = "provider")
    private String provider;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @Column(name = "validated")
    private Boolean validated;

    @OneToMany(mappedBy = "metric")
    @JsonIgnore
    private Set<HeartBeatMeasures> heartBeatMeasures = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public Boolean isValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public Set<HeartBeatMeasures> getHeartBeatMeasures() {
        return heartBeatMeasures;
    }

    public void setHeartBeatMeasures(Set<HeartBeatMeasures> heartBeatMeasures) {
        this.heartBeatMeasures = heartBeatMeasures;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Metric metric = (Metric) o;
        if(metric.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, metric.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Metric{" +
            "id=" + id +
            ", uuid='" + uuid + "'" +
            ", name='" + name + "'" +
            ", decription='" + decription + "'" +
            ", provider='" + provider + "'" +
            ", createDate='" + createDate + "'" +
            ", validated='" + validated + "'" +
            '}';
    }
}
