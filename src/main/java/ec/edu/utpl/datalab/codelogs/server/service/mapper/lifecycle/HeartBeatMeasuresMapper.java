package ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatMeasures;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.enumeations.TypeInstructionMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.metrics.MetricMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.HeartBeatCodeMeasureDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    uses = {MetricMapper.class, TypeInstructionMapper.class},
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface HeartBeatMeasuresMapper {
    @Mappings({
        @Mapping(source = "id",target = "resource_id" )
    })
    HeartBeatCodeMeasureDto map(HeartBeatMeasures source);

    @Mappings({
        @Mapping(source = "resource_id",target = "id" )
    })
    HeartBeatMeasures map(HeartBeatCodeMeasureDto source);
}
