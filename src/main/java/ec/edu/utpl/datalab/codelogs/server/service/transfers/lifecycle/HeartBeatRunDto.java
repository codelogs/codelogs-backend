package ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.enumeations.TYPE_INSTRUCTION_PUBLIC;

import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public class HeartBeatRunDto {
    private static final long serialVersionUID = 1L;

    private Long resource_id;
    private String uuid;
    private boolean success;
    private String messageL1 = "ejecución existosa";
    private String messageL2 = "";
    private String messageL3 = "";
    private long time;
    private ZonedDateTime capturate_at;
    private TYPE_INSTRUCTION_PUBLIC typeInstruction;
    private GrantedContextDto workSession;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessageL1() {
        return messageL1;
    }

    public void setMessageL1(String messageL1) {
        this.messageL1 = messageL1;
    }

    public String getMessageL2() {
        return messageL2;
    }

    public void setMessageL2(String messageL2) {
        this.messageL2 = messageL2;
    }

    public String getMessageL3() {
        return messageL3;
    }

    public void setMessageL3(String messageL3) {
        this.messageL3 = messageL3;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public ZonedDateTime getCapturate_at() {
        return capturate_at;
    }

    public void setCapturate_at(ZonedDateTime capturate_at) {
        this.capturate_at = capturate_at;
    }

    public TYPE_INSTRUCTION_PUBLIC getTypeInstruction() {
        return typeInstruction;
    }

    public void setTypeInstruction(TYPE_INSTRUCTION_PUBLIC typeInstruction) {
        this.typeInstruction = typeInstruction;
    }

    public GrantedContextDto getWorkSession() {
        return workSession;
    }

    public void setWorkSession(GrantedContextDto workSession) {
        this.workSession = workSession;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeartBeatRunDto heartBeatRun = (HeartBeatRunDto) o;
        if(heartBeatRun.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, heartBeatRun.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    @Override
    public String toString() {
        return "HeatBeatRunDto{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", success=" + success +
            ", messageL1='" + messageL1 + '\'' +
            ", messageL2='" + messageL2 + '\'' +
            ", messageL3='" + messageL3 + '\'' +
            ", time=" + time +
            ", capturate_at=" + capturate_at +
            ", workSession=" + workSession +
            '}';
    }
}
