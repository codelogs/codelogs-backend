package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.time;

import ec.edu.utpl.datalab.codelogs.server.repository.reports.TimeStateProgrammerRepo;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimeStatusProgrammerDto;
import org.joda.time.DateTime;
import org.joda.time.Seconds;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class TimeStateCoreApiImpl implements TimeStateCoreApi {
    private TimeStateProgrammerRepo workActivity;
    @Inject
    public TimeStateCoreApiImpl(TimeStateProgrammerRepo workActivity) {
        this.workActivity = workActivity;
    }

    @Override
    public TimeStatusProgrammerDto timeStateUserGlobal(@PathVariable("user") String user, @RequestParam("start") String start, @RequestParam("end") String end) {
        List<TimeStatusProgrammerDto> result =  workActivity.timeStateGlobal(user,start,end);
        return preprocess(result);
    }

    @Override
    public TimeStatusProgrammerDto timeStateUserProject(@PathVariable("user") String user, @PathVariable("project") String project, @RequestParam("start") String start, @RequestParam("end") String end) {
        List<TimeStatusProgrammerDto> result =  workActivity.timeStateProject(user,project,start,end);
        return preprocess(result);
    }
    public TimeStatusProgrammerDto preprocess(List<TimeStatusProgrammerDto> result){
        TimeStatusProgrammerDto timeStatusProgrammerDto = new TimeStatusProgrammerDto();
        if(!result.isEmpty()){
            timeStatusProgrammerDto.setTimeActive(result.stream().mapToLong(TimeStatusProgrammerDto::getTimeActive).sum());
            Timestamp lastWork = result.get(0).getEndSession();
            Timestamp initWork = result.get(result.size()-1).getStartSession();

            DateTime t1 = new DateTime(lastWork);
            DateTime t2 = new DateTime(initWork);


            Seconds totalTime = Seconds.secondsBetween(t2,t1);
            timeStatusProgrammerDto.setTotalTime(totalTime.getSeconds());
            timeStatusProgrammerDto.setStartSession(initWork);
            timeStatusProgrammerDto.setEndSession(lastWork);
            timeStatusProgrammerDto.setEditor("global-scope");
            timeStatusProgrammerDto.setProject("global-scope");
        }else{
            timeStatusProgrammerDto.setTimeActive(0);
            timeStatusProgrammerDto.setEditor("unknow");
            timeStatusProgrammerDto.setTotalTime(0);
            timeStatusProgrammerDto.setEndSession(null);
            timeStatusProgrammerDto.setEndSession(null);
        }
        return timeStatusProgrammerDto;
    }
}
