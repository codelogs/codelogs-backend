package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

/**
 * * Created by rfcardenas
 */
public class PulseCodeCronoDto {
    private String filecode;
    private String capturedate;
    private String filename;
    private String pathfile;
    private int counter;

    public String getFilecode() {
        return filecode;
    }

    public void setFilecode(String filecode) {
        this.filecode = filecode;
    }

    public String getCapturedate() {
        return capturedate;
    }

    public void setCapturedate(String capturedate) {
        this.capturedate = capturedate;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getPathfile() {
        return pathfile;
    }

    public void setPathfile(String pathfile) {
        this.pathfile = pathfile;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
