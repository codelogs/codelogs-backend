package ec.edu.utpl.datalab.codelogs.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * * Created by rfcardenas
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND,reason="Aun no se ha ejecutado un análisis del código")
public class AnalysisNotFoundException extends RuntimeException {
    public AnalysisNotFoundException(String project_uuid) {
        super(String.format("No se pudo encontrar un análisis para el proyecto %s",project_uuid));
    }
}
