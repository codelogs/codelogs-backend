package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

import java.sql.Timestamp;

/**
 * * Created by rfcardenas
 */
public class WorkSessionLessZTM {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String uuid;
    private Timestamp sessionStart;
    private Timestamp sessionEnd;
    private Long accumTime;
    private String editor;
    private Long terminalUptime;
    private String terminalArchitecture;
    private String terminalOs;
    private String terminalIp;
    private String terminalMac;
    private String terminalName;
    private Integer terminalCpu;
    private Integer terminalTemperature;
    private Integer terminalCores;
    private Long terminalRam;
    private Long terminalRamAv;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Timestamp getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(Timestamp sessionStart) {
        this.sessionStart = sessionStart;
    }

    public Timestamp getSessionEnd() {
        return sessionEnd;
    }

    public void setSessionEnd(Timestamp sessionEnd) {
        this.sessionEnd = sessionEnd;
    }

    public Long getAccumTime() {
        return accumTime;
    }

    public void setAccumTime(Long accumTime) {
        this.accumTime = accumTime;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Long getTerminalUptime() {
        return terminalUptime;
    }

    public void setTerminalUptime(Long terminalUptime) {
        this.terminalUptime = terminalUptime;
    }

    public String getTerminalArchitecture() {
        return terminalArchitecture;
    }

    public void setTerminalArchitecture(String terminalArchitecture) {
        this.terminalArchitecture = terminalArchitecture;
    }

    public String getTerminalOs() {
        return terminalOs;
    }

    public void setTerminalOs(String terminalOs) {
        this.terminalOs = terminalOs;
    }

    public String getTerminalIp() {
        return terminalIp;
    }

    public void setTerminalIp(String terminalIp) {
        this.terminalIp = terminalIp;
    }

    public String getTerminalMac() {
        return terminalMac;
    }

    public void setTerminalMac(String terminalMac) {
        this.terminalMac = terminalMac;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public Integer getTerminalCpu() {
        return terminalCpu;
    }

    public void setTerminalCpu(Integer terminalCpu) {
        this.terminalCpu = terminalCpu;
    }

    public Integer getTerminalTemperature() {
        return terminalTemperature;
    }

    public void setTerminalTemperature(Integer terminalTemperature) {
        this.terminalTemperature = terminalTemperature;
    }

    public Integer getTerminalCores() {
        return terminalCores;
    }

    public void setTerminalCores(Integer terminalCores) {
        this.terminalCores = terminalCores;
    }

    public Long getTerminalRam() {
        return terminalRam;
    }

    public void setTerminalRam(Long terminalRam) {
        this.terminalRam = terminalRam;
    }

    public Long getTerminalRamAv() {
        return terminalRamAv;
    }

    public void setTerminalRamAv(Long terminalRamAv) {
        this.terminalRamAv = terminalRamAv;
    }
}
