package ec.edu.utpl.datalab.codelogs.server.security.ktoken;

import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * * Created by rfcardenas
 */
public class KwtToken extends AbstractAuthenticationToken {
    String token;

    public KwtToken(String token) {
        super(null);
        this.token = token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
