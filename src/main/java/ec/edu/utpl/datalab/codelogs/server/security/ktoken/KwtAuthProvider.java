package ec.edu.utpl.datalab.codelogs.server.security.ktoken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;


/**
 * * Created by rfcardenas
 */
public class KwtAuthProvider implements AuthenticationProvider {
    private final Logger log = LoggerFactory.getLogger(KwtAuthProvider.class);


    private TokenProviderKw tokenProviderKw = new TokenProviderKw();


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String token = authentication.getCredentials().toString();
        return tokenProviderKw.getAuthentication(token);
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.isAssignableFrom(KwtToken.class);
    }
}
