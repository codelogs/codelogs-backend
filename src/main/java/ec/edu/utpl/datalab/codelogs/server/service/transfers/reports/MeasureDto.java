package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

/**
 * * Created by rfcardenas
 */
public class MeasureDto
{
    private Long id;
    private String uuid;
    private String captureDate;
    private Double value;

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(String captureDate) {
        this.captureDate = captureDate;
    }
}
