package ec.edu.utpl.datalab.codelogs.server.repository.reports;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LanguajeUsageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * * Created by rfcardenas
 */
@Repository
public class LanguajeUsageRepositoryImpl implements LanguajeUsageRepository {
    private static final Logger log = LoggerFactory.getLogger(LanguajeUsageRepositoryImpl.class);
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String PROJECT_LANGUAJE_USAGE ="SELECT lg.nombre as languaje , count(fc.extension)  as usage\n" +
        "                                        FROM file_code fc \n" +
        "                                                    LEFT JOIN languaje lg  ON lg.extension = fc.extension\n" +
        "                                                    LEFT JOIN PROJECT  pr ON PR.id = project_id \n" +
        "                                                    LEFT JOIN PROGRAMMER PRG ON PRG.ID  =  PR.PROGRAMMER_ID \n" +
        "                                                    WHERE PRG.USERNAME  = :username \n" +
        "                                        GROUP BY languaje\n" +
        "                                        ORDER BY usage DESC\n";


    private final String LANG_USAGE_GLOBAL_RANGE = "SELECT \n" +
        "  languaje.nombre as languaje , count(languaje.extension)  as usage\n" +
        "FROM \n" +
        "  heart_beats_code, \n" +
        "  work_session, \n" +
        "  languaje,\n" +
        "  project JOIN programmer ON project.programmer_id = programmer.id\n" +
        "WHERE \n" +
        "  heart_beats_code.work_session_id = work_session.id AND\n" +
        "  heart_beats_code.lang = languaje.extension AND\n" +
        "  work_session.project_id = project.id AND\n" +
        "  work_session.session_start::date >= :fromDate AND \n" +
        "  work_session.session_end::date <= :toDate AND \n" +
        "  programmer.username = :username\n" +
        "GROUP BY languaje";


    private final String LANG_USAGE_PROJECT_RANGE = "SELECT \n" +
        "  languaje.nombre as languaje , count(languaje.extension)  as usage\n" +
        "FROM \n" +
        "  heart_beats_code, \n" +
        "  work_session, \n" +
        "  languaje,\n" +
        "  project JOIN programmer ON project.programmer_id = programmer.id\n" +
        "WHERE \n" +
        "  project.uuid = :project_uuid AND \n" +
        "  heart_beats_code.work_session_id = work_session.id AND\n" +
        "  heart_beats_code.lang = languaje.extension AND\n" +
        "  work_session.project_id = project.id AND\n" +
        "  work_session.session_start::date >= :fromDate AND \n" +
        "  work_session.session_end::date <= :toDate AND \n" +
        "  programmer.username = :username\n" +
        "GROUP BY languaje";

    @Inject
    public LanguajeUsageRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    @Override
    public List<LanguajeUsageDto> usageLangList(String username) {
        log.info("Query app lang ussage" + username);
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("username", username);
        return namedParameterJdbcTemplate
            .query(PROJECT_LANGUAJE_USAGE,queryParams,new BeanPropertyRowMapper<>(LanguajeUsageDto.class));
    }

    @Override
    public List<LanguajeUsageDto> usageLangWorkProjectRange(String username, String project_uuid, String fromDate, String toDate) {
        log.info("Query app lang ussage" + username);
        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("username", username);
        queryParams.addValue("project_uuid", project_uuid);
        queryParams.addValue("fromDate", fromDate, Types.DATE);
        queryParams.addValue("toDate", toDate,Types.DATE);
        return namedParameterJdbcTemplate
            .query(LANG_USAGE_PROJECT_RANGE, queryParams, new BeanPropertyRowMapper<>(LanguajeUsageDto.class));
    }

    @Override
    public List<LanguajeUsageDto> usageLangWorkGlobalRange(String username, String fromDate, String toDate) {
        log.info("Query app lang ussage" + username);
        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("username", username);
        queryParams.addValue("fromDate", fromDate, Types.DATE);
        queryParams.addValue("toDate", toDate,Types.DATE);


        return namedParameterJdbcTemplate
            .query(LANG_USAGE_GLOBAL_RANGE, queryParams, new BeanPropertyRowMapper<>(LanguajeUsageDto.class));
    }

}
