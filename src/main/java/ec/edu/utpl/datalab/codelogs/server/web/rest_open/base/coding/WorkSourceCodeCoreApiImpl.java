package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.coding;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.FileCode;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.FileCodeRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes.FileCodeMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.FileCodeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
public class WorkSourceCodeCoreApiImpl implements WorkSourceCodeCoreApi {
    private final Logger log = LoggerFactory.getLogger(WorkSourceCodeCoreApiImpl.class);
    private FileCodeRepository fileCodeRepository;
    private FileCodeMapper fileCodeMapper;

    @Inject
    public WorkSourceCodeCoreApiImpl(FileCodeRepository fileCodeRepository, FileCodeMapper fileCodeMapper) {
        this.fileCodeRepository = fileCodeRepository;
        this.fileCodeMapper = fileCodeMapper;
    }

    @Override
    public ResponseEntity<List<FileCodeDto>> heartBeats(@PathVariable("project") String project) {
        Collection<FileCode> result = fileCodeRepository.collectionFilesByProject(project);

        List<FileCodeDto> response = result.stream()
            .map(fileCodeMapper::map)
            .collect(Collectors.toList());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
