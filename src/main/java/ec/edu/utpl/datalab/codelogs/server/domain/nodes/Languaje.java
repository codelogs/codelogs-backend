package ec.edu.utpl.datalab.codelogs.server.domain.nodes;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Languaje.
 */
@Entity
@Table(name = "languaje")
public class Languaje implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid",unique = true)
    private String uuid;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "extension")
    private String extension;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Languaje languaje = (Languaje) o;
        if(languaje.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, languaje.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Languaje{" +
            "id=" + id +
            ", uuid='" + uuid + "'" +
            ", nombre='" + nombre + "'" +
            ", extension='" + extension + "'" +
            ", createDate='" + createDate + "'" +
            '}';
    }
}
