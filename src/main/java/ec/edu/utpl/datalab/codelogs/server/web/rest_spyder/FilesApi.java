package ec.edu.utpl.datalab.codelogs.server.web.rest_spyder;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.FileCodePack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URISyntaxException;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/spyder",
    authorizations = {},
    tags = {"spyder"},
    description = "API interactuar servidor con el monitor spyder")
@RequestMapping("api/v1/spyder")
public interface FilesApi {
    @ApiOperation(
        value = "API el monitor registra los archivos de codigo fuente, en caso de estar ya registrados" +
            "le retorna el contexto para el proyecto" ,
        nickname = "get_lang_usage",
        response = GrantedContextDto.class,
        httpMethod = "POST",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/node/file",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<GrantedContextPack> createFileNode(
        @RequestBody FileCodePack dto
    ) throws URISyntaxException;

}
