package ec.edu.utpl.datalab.codelogs.server.repository.security;

import ec.edu.utpl.datalab.codelogs.server.domain.security.TokenAccess;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;

/**
 * Spring Data JPA repository for the User entity.
 */
public interface TokenAccessRepository extends JpaRepository<TokenAccess, Long> {

    @Query("SELECT tk FROM TokenAccess tk left join tk.programmer p  where p.username = :username_programmer")
    Optional<TokenAccess> findByProgrammer(String username_programmer);

}
