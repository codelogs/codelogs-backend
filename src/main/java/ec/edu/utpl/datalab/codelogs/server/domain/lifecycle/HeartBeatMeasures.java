package ec.edu.utpl.datalab.codelogs.server.domain.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.enumeration.TYPE_INSTRUCTION;
import ec.edu.utpl.datalab.codelogs.server.domain.metrics.CodeAnalysis;
import ec.edu.utpl.datalab.codelogs.server.domain.metrics.Metric;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Measure.
 */
@Entity
@Table(name = "heart_beats_measure",
    uniqueConstraints = {@UniqueConstraint(columnNames = {"metric_id", "code_analysis_id"})})
public class HeartBeatMeasures implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid",unique = true)
    private String uuid;

    @Column(name = "value")
    private Double value;

    @ManyToOne
    @JoinColumn(name="code_analysis_id")
    private CodeAnalysis codeAnalysis;

    @Column(name ="type_instruction")
    private TYPE_INSTRUCTION instruction;

    @ManyToOne
    @JoinColumn(name = "metric_id")
    private Metric metric;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public CodeAnalysis getCodeAnalysis() {
        return codeAnalysis;
    }

    public void setCodeAnalysis(CodeAnalysis codeAnalysis) {
        this.codeAnalysis = codeAnalysis;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeartBeatMeasures heartBeatMeasures = (HeartBeatMeasures) o;
        if(heartBeatMeasures.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, heartBeatMeasures.id);
    }

    public TYPE_INSTRUCTION getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION instruction) {
        this.instruction = instruction;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "HeartBeatMeasures{" +
            "id=" + id +
            ", uuid='" + uuid + '\'' +
            ", value=" + value +
            ", codeAnalysis=" + codeAnalysis +
            ", instruction=" + instruction +
            ", metric=" + metric +
            '}';
    }
}
