package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.sessions;

import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.WorkSessionDto;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class SessionUserApiImpl implements SessionUserApi {
    private SessionCoreApi sessionCoreApi;

    @Inject
    public SessionUserApiImpl(SessionCoreApi sessionCoreApi) {
        this.sessionCoreApi = sessionCoreApi;
    }

    @Override
    public ResponseEntity<List<WorkSessionDto>> getAllSessions(Pageable pageable) throws URISyntaxException {
        String username = SecurityUtils.getCurrentUserLogin();
        return sessionCoreApi.getAllSessions(username,pageable);
    }

    @Override
    public ResponseEntity<List<WorkSessionDto>> getSessionsRecent(Pageable pageable) throws URISyntaxException {
        String username = SecurityUtils.getCurrentUserLogin();
        return sessionCoreApi.getSessionsRecent(username,pageable);
    }
}
