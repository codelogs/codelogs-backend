package ec.edu.utpl.datalab.codelogs.server.repository.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimeStatusProgrammerDto;

import java.util.List;

/**
 * * Created by rfcardenas
 */

public interface TimeStateProgrammerRepo {
    List<TimeStatusProgrammerDto> queryActivityWork(String projectuuid);

    /**
     * Estados de tiempo global, retorna los datos desc por la fecha de trabajo
     * @param username
     * @param fromDate
     * @param toDate
     * @return
     */
    List<TimeStatusProgrammerDto>   timeStateGlobal(String username, String fromDate, String toDate);

    /**
     * Estadosd e tiempo para in proyecto, retorna datos ordenados en forma desc por fecha
     * @param username
     * @param project_uuid
     * @param fromDate
     * @param toDate
     * @return
     */
    List<TimeStatusProgrammerDto> timeStateProject(String username, String project_uuid, String fromDate, String toDate);
}
