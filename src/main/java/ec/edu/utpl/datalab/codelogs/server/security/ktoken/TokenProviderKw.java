package ec.edu.utpl.datalab.codelogs.server.security.ktoken;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Objects;

public class TokenProviderKw {
    private final Logger log = LoggerFactory.getLogger(TokenProviderKw.class);
    private static final String AUTHORITIES_KEY = "auth";
    private final String secretKey;
    private long tokenValidityInSeconds;

    public TokenProviderKw() {
        this.secretKey = "bbed4a6b-46a5-4bfd-9a29-d5cb9f8bc4ab";
        tokenValidityInSeconds = 86400*10000;
    }


    public String createToken(String username) {
        Objects.requireNonNull(username,"El token tiene que tener informacion sobre el usuario");
        long now = (new Date()).getTime();
        System.out.println("TOKEN vald " + tokenValidityInSeconds);
        Date validity =  new Date(now + this.tokenValidityInSeconds);

        return Jwts.builder()
            .setSubject(username)
            .signWith(SignatureAlgorithm.HS512, secretKey)
            .setExpiration(validity)
            .compact();
    }

    public Authentication getAuthentication(String token) {
        Claims claims = Jwts.parser()
            .setSigningKey(secretKey)
            .parseClaimsJws(token)
            .getBody();

        Collection<? extends GrantedAuthority> authorities = Arrays.asList((GrantedAuthority) () -> "Machine",(GrantedAuthority) () -> "Normal");

        System.out.println(authorities);

        User principal = new User(claims.getSubject(), "", authorities);

        return new UsernamePasswordAuthenticationToken(principal, "",authorities);
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            log.info("Invalid JWT signature: " + e.getMessage());
            return false;
        }
    }


}
