package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.activity;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeProjectDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "api/v1/work_time",
    authorizations = {},
    tags = {"work_time","current_user","time"},
    description = "Retorna la cantidad de tiempo invertido programando partes del código del usuario actualmente logeado")
@RequestMapping("api/v1/work_time")
public interface WorkTimeUserApi {
    /**
     * Retorna el tiempo de trabajo del usuario actualmente loegado
     *
     * @param start Fecha
     * @param end   Fecha
     * @return
     */
    @ApiOperation(
        value = "Retorna datos simples del usuario actualmente logeado sobre la cantidad de tiempo de programación  en un intervalo de fechas" +
            "EJM : start = 2015-12-12 end = 2017-03-12 no proporciona detalles sobre el proyecto",
        nickname = "get_work_time_curent_user",
        response = LoggTimeDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/lapse",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<List<LoggTimeDto>> getSimpleLogTime(
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );
    /**
     * Retorna la cantidad de tiempo trabajo de un usuario en un proyecto
     * @param project
     * @param start
     * @param end
     * @return
     */
    @ApiOperation(
        value = "Retorna información detallada sobre el tiempo asignado en un proyecto específico en un rango de fechas " +
            "del usuario actualmente logeado",
        nickname = "get_work_time_in_project",
        response = LoggTimeProjectDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/projects/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<LoggTimeProjectDto> getWorkTimeProject(
        @PathVariable("project") String project,
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );

    @ApiOperation(
        value = "Retorna el total del tiempo del programador actualmente logeado en los diferentes proyectos en  un rango de fechas",
        nickname = "get_work_time_all_projects",
        response = LoggTimeProjectDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/projects/all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<List<LoggTimeProjectDto>> getGlobalWorkTimeAllProjects(
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );
}
