package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.coding;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.SuperDataSet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/coding/update_pattern",
    authorizations = {},
    tags = {"coding","update pattern"},
    description = "API proporciona datos estrachamente relacionados con la codificacion, patron de actualizacion," +
        "problemas etc.")
@RequestMapping("/api/v1/coding/update_pattern")
public interface UpdatePatterCoreApi {

    @ApiOperation(
        value = "Retorna el patron de programación global de un usuario",
        nickname = "patron de programación",
        response = SuperDataSet.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    SuperDataSet dataSetUpdatePatternUser(@PathVariable("user") String user);

    @ApiOperation(
        value = "Retorna el patron de programación de un usuario en un proyecto",
        nickname = "patron de programación",
        response = SuperDataSet.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}/project/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    SuperDataSet dataSetUpdatePatternProjectUser(@PathVariable("user") String user, @PathVariable("project") String project);
}
