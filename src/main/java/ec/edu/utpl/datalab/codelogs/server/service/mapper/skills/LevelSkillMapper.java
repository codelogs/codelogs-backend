package ec.edu.utpl.datalab.codelogs.server.service.mapper.skills;

import ec.edu.utpl.datalab.codelogs.server.domain.programmer_skills.LevelSkill;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.skill.LevelSkillDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LevelSkillMapper {
    LevelSkill map(LevelSkillDto source);
    LevelSkillDto map(LevelSkill source);
}
