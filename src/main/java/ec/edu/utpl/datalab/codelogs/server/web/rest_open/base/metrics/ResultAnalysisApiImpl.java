package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.metrics;

import ec.edu.utpl.datalab.codelogs.server.domain.metrics.CodeAnalysis;
import ec.edu.utpl.datalab.codelogs.server.repository.metrics.CodeAnalysisRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.metrics.CodeAnalysisMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.metrics.CodeAnalysisDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
@RestController
public class ResultAnalysisApiImpl implements ResultAnalysisApi {
    private CodeAnalysisRepository codeAnalysisRepository;
    private CodeAnalysisMapper codeAnalysisMapper;

    @Inject
    public ResultAnalysisApiImpl(CodeAnalysisRepository codeAnalysisRepository, CodeAnalysisMapper codeAnalysisMapper) {
        this.codeAnalysisRepository = codeAnalysisRepository;
        this.codeAnalysisMapper = codeAnalysisMapper;
    }

    @Override
    public ResponseEntity<CodeAnalysisDto> getAnalisis(@PathVariable("project_uuid") String project_uuid) throws URISyntaxException {
        Optional<CodeAnalysis> result = codeAnalysisRepository.findAnalysisByProject(project_uuid);
        if(result.isPresent()){
            return ResponseEntity.ok()
                .body(codeAnalysisMapper.map(result.get()));
        }else{
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body(null);
        }
    }
}
