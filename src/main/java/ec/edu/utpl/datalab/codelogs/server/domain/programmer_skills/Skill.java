package ec.edu.utpl.datalab.codelogs.server.domain.programmer_skills;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Languaje;
import ec.edu.utpl.datalab.codelogs.server.domain.security.Programmer;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * * Created by rfcardenas
 */
@Entity
@Table(name = "skill")
public class Skill {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "lastUpdate")
    private ZonedDateTime lastUpdate;
    @Column(name = "total_usage")
    private long total_usage;

    @OneToOne
    private Languaje languaje;
    @ManyToOne
    private Programmer programmer;
    @ManyToOne
    private LevelSkill levelSkill;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public long getTotal_usage() {
        return total_usage;
    }

    public void setTotal_usage(long total_usage) {
        this.total_usage = total_usage;
    }

    public Languaje getLanguaje() {
        return languaje;
    }

    public void setLanguaje(Languaje languaje) {
        this.languaje = languaje;
    }

    public Programmer getProgrammer() {
        return programmer;
    }

    public void setProgrammer(Programmer programmer) {
        this.programmer = programmer;
    }

    public LevelSkill getLevelSkill() {
        return levelSkill;
    }

    public void setLevelSkill(LevelSkill levelSkill) {
        this.levelSkill = levelSkill;
    }

}
