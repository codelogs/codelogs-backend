package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.projects;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URISyntaxException;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/projects",
    authorizations = {},
    tags = {"projects"},
    description = "Projects")
@RequestMapping("/api/v1/projects")
public interface ProjectsCoreApi {
    @ApiOperation(
        value = "retorna la lista de proyectos de un programador",
        nickname = "projects",
        response = ProjectDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/user/{user}/all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<List<ProjectDto>> getAllProjects(@PathVariable(name = "user") String user, Pageable pageable) throws URISyntaxException;


    /**
     *
     * @param user nombre usuario al que corresponden los datos
     * @param pageable numero maximo de sessiones por pagina
     * @return
     * @throws URISyntaxException
     */
    @ApiOperation(
        value = "retorna la lista de proyectos reciente en los que ha trabajado un programador",
        nickname = "projects",
        response = ProjectDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/user/{user}/recent",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity getRecentProjects(@PathVariable(name = "user") String user, Pageable pageable) throws URISyntaxException;


    @ApiOperation(
        value = "retorna información detallada de un proyecto",
        nickname = "projects",
        response = ProjectDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/{projectUid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<ProjectDto> getProjectInformation(@PathVariable(name = "projectUid") String projectUid, Pageable pageable) throws URISyntaxException;
}
