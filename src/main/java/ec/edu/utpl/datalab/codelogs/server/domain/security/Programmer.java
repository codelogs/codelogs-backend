package ec.edu.utpl.datalab.codelogs.server.domain.security;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Programmer.
 */
@Entity
@Table(name = "programmer")
public class Programmer implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username",unique = true,nullable = false)
    private String username;

    @Column(name = "status")
    private Boolean status;

    @OneToOne
    private User user;

    @OneToMany(mappedBy = "programmer")
    @JsonIgnore
    private Set<Project> projects = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public Boolean isStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Programmer programmer = (Programmer) o;
        if(programmer.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, programmer.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Programmer{" +
            "id=" + id +
            ", username='" + username + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
