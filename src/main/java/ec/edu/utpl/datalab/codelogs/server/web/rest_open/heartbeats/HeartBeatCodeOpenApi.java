package ec.edu.utpl.datalab.codelogs.server.web.rest_open.heartbeats;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.HeartBeatCodeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URISyntaxException;
import java.util.Collection;

/**
 * * Created by rfcardenas
 */
@Api(value = "api/v1/openapi/heartbeatcode", authorizations = {
}, tags = "heartbeatcode")
@RequestMapping("api/v1/")
public interface HeartBeatCodeOpenApi {
    @ApiOperation(
        value = "Open API HeartBeats de código, Retorna los pulsos de código, los cuales son resultado" +
            "de la actividad del estudiante" ,
        nickname = "heartbeatcode",
        response = GrantedContextDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/heartbeatcode/worksession/{worksession_id}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<Collection<HeartBeatCodeDto>> getBeatsCode(
        @PathVariable("project_uuid") String project_uuid,
        Pageable pageable
    ) throws URISyntaxException;
}
