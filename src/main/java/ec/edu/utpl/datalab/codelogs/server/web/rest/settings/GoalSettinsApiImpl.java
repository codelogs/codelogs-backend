package ec.edu.utpl.datalab.codelogs.server.web.rest.settings;

import ec.edu.utpl.datalab.codelogs.server.repository.security.UserRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.settings.GoalSettingsRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.settings.GoalSettingMapper;
import ec.edu.utpl.datalab.codelogs.server.service.settings.GoalSettingService;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.settings.GoalSettingsTransfer;
import ec.edu.utpl.datalab.codelogs.server.web.rest.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.concurrent.TimeUnit;

/**
 * * Created by rfcardenas
 */
@RestController
public class GoalSettinsApiImpl implements GoalSettinsApi {
    private final Logger log = LoggerFactory.getLogger(GoalSettinsApiImpl.class);

    //Programar una hora al dia
    private static final Long GOAL_DAY = TimeUnit.HOURS.convert(1, TimeUnit.SECONDS);
    //Programar 5 horas a la semana
    private static final Long GOAL_WEEK = TimeUnit.HOURS.convert(5, TimeUnit.SECONDS);
    //Programar 20 horas a la semana
    private static final Long GOAL_MONTH = TimeUnit.HOURS.convert(20, TimeUnit.SECONDS);

    private UserRepository userRepository;
    private GoalSettingsRepository goalSettingsRepository;
    private GoalSettingMapper goalSettingMapper;
    private GoalSettingService goalSettingService;

    @Inject
    public GoalSettinsApiImpl(UserRepository userRepository, GoalSettingsRepository goalSettingsRepository, GoalSettingMapper goalSettingMapper, GoalSettingService goalSettingService) {
        this.userRepository = userRepository;
        this.goalSettingsRepository = goalSettingsRepository;
        this.goalSettingMapper = goalSettingMapper;
        this.goalSettingService = goalSettingService;
    }

    @Override
    public ResponseEntity<?> createDefault() throws URISyntaxException {
        log.info("Creando Goals por defecto");
        return ResponseEntity.ok(
            goalSettingMapper.map(goalSettingService.createDefaultGoals())
        );
    }

    @Override
    public ResponseEntity<?> updateGoal(@RequestBody GoalSettingsTransfer dto) throws URISyntaxException {
        return ResponseEntity.ok(
            goalSettingMapper.map(goalSettingService.updateGoals(
                dto.getGoalMonth(),
                dto.getGoalWeek(),
                dto.getGoalDay()
            ))
        );
    }

    @Override
    public ResponseEntity<?> resetGoals() throws URISyntaxException {
        goalSettingService.reset();
        return ResponseEntity.ok()
            .headers(HeaderUtil.createAlert("goals.reset","goals reiniciado"))
            .body(null);
    }

    @Override
    public ResponseEntity<GoalSettingsTransfer> getGoals() throws URISyntaxException {
        return ResponseEntity.ok(goalSettingMapper.map(goalSettingService.getGoals()));
    }
}
