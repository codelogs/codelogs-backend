package ec.edu.utpl.datalab.codelogs.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * * Created by rfcardenas
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND,reason="WorkSessionNotFound in this server")
public class WorkSessionEmpty extends Exception {
    public WorkSessionEmpty(String nivel) {
        super("El Objeto de transferencia no tiene la clave otorgada para el nivel "+nivel );
    }
}
