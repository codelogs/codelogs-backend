package ec.edu.utpl.datalab.codelogs.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * * Created by rfcardenas
 */
@ResponseStatus(value= HttpStatus.BAD_REQUEST,reason="La clave otorgada no fue encontrada")
public class GrantedContextInvalid extends RuntimeException {
    public GrantedContextInvalid() {
        super("la clave otorgada no fue econtrada para ");
    }
}
