/**
 * View Models used by Spring MVC REST controllers.
 */
package ec.edu.utpl.datalab.codelogs.server.web.rest.vm;
