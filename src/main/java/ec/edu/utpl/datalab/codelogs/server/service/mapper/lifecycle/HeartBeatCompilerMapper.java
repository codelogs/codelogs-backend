package ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCompile;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextRct;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.enumeations.TypeInstructionMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.coding.BuildRunDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.HeartBeatCompileDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    uses = {GrantedContextMapper.class, GrantedContextRct.class, TypeInstructionMapper.class},
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface HeartBeatCompilerMapper{
    @Mappings({
        @Mapping(source = "resource_id",target = "id" )
    })
    HeartBeatCompile map(HeartBeatCompileDto source);

    @Mappings({
        @Mapping(source = "id",target = "resource_id" )
    })
    HeartBeatCompileDto map(HeartBeatCompile source);

    @Mappings({
        @Mapping(source = "id",target = "resource_id" )
    })
    BuildRunDto mapTo(HeartBeatCompile source);
}
