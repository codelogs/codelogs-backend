package ec.edu.utpl.datalab.codelogs.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * * Created by rfcardenas
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND,reason="This User is not found in the system")
public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String user) {
        super(String.format("El usuario %s no fue encontrado en la base de datos de codelogs",user));
    }
}
