package ec.edu.utpl.datalab.codelogs.server.repository.settings;

import ec.edu.utpl.datalab.codelogs.server.domain.settings.ThemeSettings;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * * Created by rfcardenas
 */
public interface ThemeSettingsRepository extends JpaRepository<ThemeSettings,Long> {
}
