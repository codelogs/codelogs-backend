package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

import java.util.ArrayList;
import java.util.List;

/**
 * * Created by rfcardenas
 */
public class Dataset {
    private String name;
    private String unit;
    private String type;
    private int maxDecimal;
    private List<MeasureDto> data = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<MeasureDto> getData() {
        return data;
    }

    public void setData(List<MeasureDto> data) {
        this.data = data;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getMaxDecimal() {
        return maxDecimal;
    }

    public void setMaxDecimal(int maxDecimal) {
        this.maxDecimal = maxDecimal;
    }
}
