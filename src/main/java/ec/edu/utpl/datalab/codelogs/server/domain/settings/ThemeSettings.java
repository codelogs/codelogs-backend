package ec.edu.utpl.datalab.codelogs.server.domain.settings;

import ec.edu.utpl.datalab.codelogs.server.domain.security.User;

import javax.persistence.*;

/**
 * * Created by rfcardenas
 */
@Entity
@Table(name = "theme_settings")
public class ThemeSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "css")
    private String css;
    @OneToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
