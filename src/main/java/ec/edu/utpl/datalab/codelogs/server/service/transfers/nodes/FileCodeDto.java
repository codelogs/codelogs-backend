package ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A FileCode.
 */
public class FileCodeDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    private String uuid;
    private String name;
    private String pathFile;
    private String extension;
    private ZonedDateTime createDate;
    private ZonedDateTime updateDate;
    private Integer codeLines;
    private Boolean available;
    private Integer updateCounter;
    private Long fixedTimeactivity;
    private GrantedContextDto project;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getCodeLines() {
        return codeLines;
    }

    public void setCodeLines(Integer codeLines) {
        this.codeLines = codeLines;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Integer getUpdateCounter() {
        return updateCounter;
    }

    public void setUpdateCounter(Integer updateCounter) {
        this.updateCounter = updateCounter;
    }

    public Long getFixedTimeactivity() {
        return fixedTimeactivity;
    }

    public void setFixedTimeactivity(Long fixedTimeactivity) {
        this.fixedTimeactivity = fixedTimeactivity;
    }

    public Boolean getAvailable() {
        return available;
    }

    public GrantedContextDto getProject() {
        return project;
    }

    public void setProject(GrantedContextDto project) {
        this.project = project;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FileCodeDto fileCode = (FileCodeDto) o;
        if(fileCode.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, fileCode.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FileCode{" +
            "id=" + id +
            ", uuid='" + uuid + "'" +
            ", name='" + name + "'" +
            ", pathFile='" + pathFile + "'" +
            ", extension='" + extension + "'" +
            ", createDate='" + createDate + "'" +
            ", updateDate='" + updateDate + "'" +
            ", codeLines='" + codeLines + "'" +
            ", available='" + available + "'" +
            ", updateCounter='" + updateCounter + "'" +
            ", fixedTimeactivity='" + fixedTimeactivity + "'" +
            '}';
    }
}
