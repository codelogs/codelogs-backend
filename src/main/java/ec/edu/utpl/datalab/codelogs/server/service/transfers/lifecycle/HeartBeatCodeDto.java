package ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.enumeations.TYPE_INSTRUCTION_PUBLIC;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.FileCodeGrantedContextDto;
import org.springframework.hateoas.ResourceSupport;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A PulseCode.
 */
public class HeartBeatCodeDto extends ResourceSupport implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long resource_id;

    private String uuid;

    private ZonedDateTime captureDate;

    private String pathFile;

    private Long linesAdd;

    private Long linesDel;

    private Long levenshtein;

    private Long timeFixed;

    private String blockMap;

    private String brachname;

    private TYPE_INSTRUCTION_PUBLIC instruction;

    private FileCodeGrantedContextDto fileCode;

    private GrantedContextDto workSession;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(ZonedDateTime captureDate) {
        this.captureDate = captureDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public Long getLinesAdd() {
        return linesAdd;
    }

    public void setLinesAdd(Long linesAdd) {
        this.linesAdd = linesAdd;
    }

    public Long getLinesDel() {
        return linesDel;
    }

    public void setLinesDel(Long linesDel) {
        this.linesDel = linesDel;
    }

    public Long getLevenshtein() {
        return levenshtein;
    }

    public void setLevenshtein(Long levenshtein) {
        this.levenshtein = levenshtein;
    }

    public Long getTimeFixed() {
        return timeFixed;
    }

    public void setTimeFixed(Long timeFixed) {
        this.timeFixed = timeFixed;
    }

    public String getBlockMap() {
        return blockMap;
    }

    public void setBlockMap(String blockMap) {
        this.blockMap = blockMap;
    }

    public String getBrachname() {
        return brachname;
    }

    public void setBrachname(String brachname) {
        this.brachname = brachname;
    }

    public TYPE_INSTRUCTION_PUBLIC getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION_PUBLIC instruction) {
        this.instruction = instruction;
    }

    public FileCodeGrantedContextDto getFileCode() {
        return fileCode;
    }

    public void setFileCode(FileCodeGrantedContextDto fileCode) {
        this.fileCode = fileCode;
    }

    public GrantedContextDto getWorkSession() {
        return workSession;
    }

    public void setWorkSession(GrantedContextDto workSession) {
        this.workSession = workSession;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeartBeatCodeDto heartBeatCode = (HeartBeatCodeDto) o;
        if(heartBeatCode.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, heartBeatCode.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    @Override
    public String toString() {
        return "HeartBeatCodeDto{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", captureDate=" + captureDate +
            ", pathFile='" + pathFile + '\'' +
            ", linesAdd=" + linesAdd +
            ", linesDel=" + linesDel +
            ", levenshtein=" + levenshtein +
            ", timeFixed=" + timeFixed +
            ", blockMap='" + blockMap + '\'' +
            ", brachname='" + brachname + '\'' +
            ", instruction=" + instruction +
            ", fileCode=" + fileCode +
            ", workSession=" + workSession +
            '}';
    }
}
