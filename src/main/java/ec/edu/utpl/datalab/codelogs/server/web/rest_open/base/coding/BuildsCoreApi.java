package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.coding;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.coding.BuildRunDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/coding/build_pattern",
    authorizations = {},
    tags = {"coding","build pattern"},
    description = "API los resultados de las compilaciones del programa, problemas comunes" +
        "problemas etc.")
@RequestMapping("/api/v1/coding/build_pattern")
public interface BuildsCoreApi {
    @ApiOperation(
        value = "Retorna el patron de compilación de un proyecto",
        nickname = "patron de programación",
        response = BuildRunDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping(value = "/project/{project_uuid}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    List<BuildRunDto> getBuildCollectionProject(
        @PathVariable("project_uuid") String project_uuid,
        Pageable pageable);

    /**
     * Retornal el patron de compilacion de una session
     * @param session_uuid
     * @param pageable
     * @return
     */
    @ApiOperation(
        value = "Retorna el patron de compilación de una sesión",
        nickname = "patron de programación",
        response = BuildRunDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/work_session/{session_uuid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    List<BuildRunDto> getBuildCollectionSession(
        @PathVariable("session_uuid") String session_uuid,
        Pageable pageable);
}
