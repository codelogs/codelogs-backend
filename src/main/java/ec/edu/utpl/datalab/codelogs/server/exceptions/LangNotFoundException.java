package ec.edu.utpl.datalab.codelogs.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * * Created by rfcardenas
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND,reason="Can not find a language definition")
public class LangNotFoundException extends RuntimeException{
    public LangNotFoundException() {
        super();
    }

    public LangNotFoundException(String message) {
        super(message);
    }

    public LangNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public LangNotFoundException(Throwable cause) {
        super(cause);
    }

    protected LangNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
