package ec.edu.utpl.datalab.codelogs.server.web.rest_spyder;

import ec.edu.utpl.datalab.codelogs.server.domain.enumeration.TYPE_INSTRUCTION;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCompile;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatRun;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.exceptions.WorkSessionNotFound;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.HeartBeatCompileRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.HeartBeatRunRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.WorkSessionRepository;
import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatBuildRunPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.TYPE_INSTRUCTION_PACK;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

/**
 * * Created by rfcardenas
 */
@RestController
public class BeatBuildRunApiImpl implements BeatBuildRunApi {
    private final Logger log = LoggerFactory.getLogger(BeatBuildRunApiImpl.class);


    private WorkSessionRepository workSessionRepository;
    private HeartBeatRunRepository runRepository;
    private HeartBeatCompileRepository compileRepository;
    private GrantedContextMapper contextMapper;

    @Inject
    public BeatBuildRunApiImpl(WorkSessionRepository workSessionRepository,
                               HeartBeatRunRepository runRepository,
                               HeartBeatCompileRepository compileRepository,
                               GrantedContextMapper contextMapper) {
        this.workSessionRepository = workSessionRepository;
        this.runRepository = runRepository;
        this.compileRepository = compileRepository;
        this.contextMapper = contextMapper;
    }

    @Override
    public ResponseEntity<GrantedContextPack> unitRuns(@RequestBody HeartBeatBuildRunPack data) throws URISyntaxException {
        log.info("Registrando run/build result {}",data );

        String username = SecurityUtils.getCurrentUserLogin();
        Optional<WorkSession> session = workSessionRepository.findOneByUuid(data.getWorkSession().getUuid());
        if(!session.isPresent())
            throw new WorkSessionNotFound(username,data.getWorkSession().getUuid());
        if( data.getInstruction() == TYPE_INSTRUCTION_PACK.HEART_BEAT_COMPILE_SUCCESS ||
            data.getInstruction() == TYPE_INSTRUCTION_PACK.HEART_BEAT_COMPILE_FAILED){
            HeartBeatCompile beatCompile = new HeartBeatCompile();
            String uuid = UUID.randomUUID().toString();
            beatCompile.setUuid(uuid);
            beatCompile.setTime(data.getTime());
            beatCompile.setCapturate_at(ZonedDateTime.now());
            beatCompile.setMessageL1(data.getMessageL1());
            beatCompile.setMessageL2(data.getMessageL2());
            beatCompile.setMessageL3(data.getMessageL3());
            beatCompile.setWorkSession(session.get());
            switch (data.getInstruction()){
                case HEART_BEAT_COMPILE_FAILED:
                    beatCompile.setSuccess(false);
                    beatCompile.setInstruction(TYPE_INSTRUCTION.HEART_BEAT_COMPILE_FAILED);
                    break;
                case HEART_BEAT_COMPILE_SUCCESS:
                    beatCompile.setSuccess(true);
                    beatCompile.setInstruction(TYPE_INSTRUCTION.HEART_BEAT_COMPILE_SUCCESS);
                    break;
            }
            compileRepository.save(beatCompile);
            return ResponseEntity.ok(contextMapper.mapToPack(beatCompile));

        }else if(data.getInstruction() == TYPE_INSTRUCTION_PACK.HEART_BEAT_RUN_FAILED ||
            data.getInstruction() == TYPE_INSTRUCTION_PACK.HEART_BEAT_RUN_SUCCESS){
            HeartBeatRun beatRun  = new HeartBeatRun();
            String uuid = UUID.randomUUID().toString();
            beatRun.setUuid(uuid);
            beatRun.setTime(data.getTime());
            beatRun.setCapturate_at(ZonedDateTime.now());
            beatRun.setMessageL1(data.getMessageL1());
            beatRun.setMessageL2(data.getMessageL2());
            beatRun.setMessageL3(data.getMessageL3());
            beatRun.setWorkSession(session.get());
            switch (data.getInstruction()){
                case HEART_BEAT_RUN_FAILED:
                    beatRun.setSuccess(false);
                    beatRun.setInstruction(TYPE_INSTRUCTION.HEART_BEAT_RUN_FAILED);
                    break;
                case HEART_BEAT_RUN_SUCCESS:
                    beatRun.setSuccess(true);
                    beatRun.setInstruction(TYPE_INSTRUCTION.HEART_BEAT_RUN_SUCCESS);
                    break;
            }
            runRepository.save(beatRun);
            return ResponseEntity.ok(contextMapper.mapToPack(beatRun));
        }
        return ResponseEntity.unprocessableEntity().body(null);
    }
}
