package ec.edu.utpl.datalab.codelogs.server.service.transfers.goals;


import java.util.Date;

/**
 * * Created by rfcardenas
 */
public class GoalTransffer {
    private Date date;
    private Long totalSeconds;
    private Long goalSeconds;
    private String comment;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getTotalSeconds() {
        return totalSeconds;
    }

    public void setTotalSeconds(Long totalSeconds) {
        this.totalSeconds = totalSeconds;
    }

    public Long getGoalSeconds() {
        return goalSeconds;
    }

    public void setGoalSeconds(Long goalSeconds) {
        this.goalSeconds = goalSeconds;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "GoalTransffer{" +
            "date=" + date +
            ", totalSeconds=" + totalSeconds +
            ", goalSeconds=" + goalSeconds +
            ", comment='" + comment + '\'' +
            '}';
    }
}
