package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;

import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;

/**
 * * Created by rfcardenas
 */
public class ProjectDataAnalysisDto {
    private Long id;
    private String uuid;
    private ZonedDateTime createDate;
    private ZonedDateTime lastUpdateDate;
    private Long timeExecution;
    private Set<MeasureDto> measures = new HashSet<>();
    private ProjectDto project;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(ZonedDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Long getTimeExecution() {
        return timeExecution;
    }

    public void setTimeExecution(Long timeExecution) {
        this.timeExecution = timeExecution;
    }

    public Set<MeasureDto> getMeasures() {
        return measures;
    }

    public void setMeasures(Set<MeasureDto> measures) {
        this.measures = measures;
    }

    public ProjectDto getProject() {
        return project;
    }
}
