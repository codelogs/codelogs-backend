package ec.edu.utpl.datalab.codelogs.server.service.transfers.metrics;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.HeartBeatCodeMeasureDto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A ProjectAnalysis.
 */
public class CodeAnalysisDto implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long resource_id;
    private String uuid;
    private ZonedDateTime createDate;
    private ZonedDateTime lastUpdateDate;
    private Long timeExecution;
    private Set<HeartBeatCodeMeasureDto> heartBeatCodeMeasures = new HashSet<>();
    private GrantedContextDto project;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(ZonedDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Long getTimeExecution() {
        return timeExecution;
    }

    public void setTimeExecution(Long timeExecution) {
        this.timeExecution = timeExecution;
    }

    public Set<HeartBeatCodeMeasureDto> getHeartBeatCodeMeasures() {
        return heartBeatCodeMeasures;
    }

    public void setHeartBeatCodeMeasures(Set<HeartBeatCodeMeasureDto> heartBeatCodeMeasures) {
        this.heartBeatCodeMeasures = heartBeatCodeMeasures;
    }

    public GrantedContextDto getProject() {
        return project;
    }

    public void setProject(GrantedContextDto project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CodeAnalysisDto codeAnalysis = (CodeAnalysisDto) o;
        if(codeAnalysis.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, codeAnalysis.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    @Override
    public String toString() {
        return "CodeAnalysisDto{" +
            "id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", createDate=" + createDate +
            ", lastUpdateDate=" + lastUpdateDate +
            ", timeExecution=" + timeExecution +
            ", heartBeatCodeMeasures=" + heartBeatCodeMeasures +
            ", project=" + project +
            '}';
    }
}
