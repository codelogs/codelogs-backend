package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.projects;

import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class ProjectUserApiImpl implements ProjectUserApi {
    private final Logger log = LoggerFactory.getLogger(ProjectUserApiImpl.class);
    private ProjectsCoreApi projectsCoreApi;

    @Inject
    public ProjectUserApiImpl(ProjectsCoreApi projectsCoreApi) {
        this.projectsCoreApi = projectsCoreApi;
    }

    @Override
    public ResponseEntity<List<ProjectDto>> getAllProjects(Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get all Projects");
        String user = SecurityUtils.getCurrentUserLogin();
        return projectsCoreApi.getAllProjects(user,pageable);
    }

    @Override
    public ResponseEntity<List<ProjectDto>> getRecentProjects(Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get all Projects");
        String user = SecurityUtils.getCurrentUserLogin();
        return projectsCoreApi.getRecentProjects(user,pageable);
    }


}
