package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.activity;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.exceptions.ProjectNotFoundException;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.ProjectRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.reports.LoggTimeRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes.ProjectMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeProjectDto;
import ec.edu.utpl.datalab.codelogs.server.web.rest.errors.CustomParameterizedException;
import ec.edu.utpl.datalab.codelogs.server.web.rest_open.util.DateIntervalUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.springframework.http.ResponseEntity.status;

/**
 * * Created by rfcardenas
 */
@RestController
public class WorkTimeCoreApiImpl implements WorkTimeCoreApi {
    private final Logger log = LoggerFactory.getLogger(WorkTimeCoreApiImpl.class);

    private ProjectRepository projectRepository;
    private LoggTimeRepository loggTimeRepo;
    private ProjectMapper projectMapper;
    private DateIntervalUtil dateIntervalUtil;

    @Inject
    public WorkTimeCoreApiImpl(ProjectRepository projectRepository, LoggTimeRepository loggTimeRepo, ProjectMapper projectMapper, DateIntervalUtil dateIntervalUtil) {
        this.projectRepository = projectRepository;
        this.loggTimeRepo = loggTimeRepo;
        this.projectMapper = projectMapper;
        this.dateIntervalUtil = dateIntervalUtil;
    }

    @Override
    public ResponseEntity<List<LoggTimeDto>> getSimpleLogTime(@PathVariable("user") String user, @RequestParam("start") String start, @RequestParam("end") String end) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
        try {
            Date testStart = sdf.parse(start);
            Date testEnd = sdf.parse(end);
            if(Objects.isNull(testStart) || Objects.isNull(testEnd)){
                ResponseEntity.badRequest();
            }
        }catch (ParseException ex){
            new CustomParameterizedException(ex.getMessage(),"range");
        }
        List<LoggTimeDto> result = loggTimeRepo.customRangeLoggTime(user,start,end);

        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<LoggTimeProjectDto> getWorkTimeProject(@PathVariable("user") String user, @PathVariable("project") String project, @RequestParam("start") String start, @RequestParam("end") String end) {
        Optional<Project> projectresult = projectRepository.findProjectByUuidAndUser(user,project);
        if(projectresult.isPresent()){
            List<LoggTimeDto> loggTimeDto = loggTimeRepo.customRangeLoggTimeProject(user,project,start,end);
            LoggTimeProjectDto loggTimeProjectDto =  new LoggTimeProjectDto();
            loggTimeProjectDto.setProject(projectMapper.map(projectresult.get()));
            loggTimeProjectDto.setUser_owner(user);
            loggTimeProjectDto.setTimeDtoList(loggTimeDto);
            return ResponseEntity.ok(loggTimeProjectDto);
        }else{
            throw new ProjectNotFoundException("El proyecto no encontrado para el usuario " + user);
        }
    }

    @Override
    public ResponseEntity<List<LoggTimeProjectDto>> getGlobalWorkTimeAllProjects(@RequestParam("start") String start, @RequestParam("end") String end, @PathVariable("user") String user) {
        List<LoggTimeProjectDto> listToReponse = new ArrayList<>();
        List<Project> projects = projectRepository.findAllFromUser(user);

        for (Project project : projects) {
            List<LoggTimeDto> logTimeList = loggTimeRepo.customRangeLoggTimeProject(user, project.getUuid(), start, end);
            LoggTimeProjectDto loggTimeProjectDto = new LoggTimeProjectDto();
            loggTimeProjectDto.setUser_owner(user);
            loggTimeProjectDto.setProject(projectMapper.map(project));
            loggTimeProjectDto.setTimeDtoList(logTimeList);
            listToReponse.add(loggTimeProjectDto);
        }
        if(listToReponse.isEmpty()){
            return status(HttpStatus.NO_CONTENT).body(listToReponse);
        }
        return ResponseEntity.ok(listToReponse);
    }


}
