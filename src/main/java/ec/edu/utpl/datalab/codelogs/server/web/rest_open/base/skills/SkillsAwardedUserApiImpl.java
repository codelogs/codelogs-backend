package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.skills;

import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.skill.SkillDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class SkillsAwardedUserApiImpl implements SkillsAwardedUserApi {
    private final Logger log = LoggerFactory.getLogger(SkillsAwardedUserApiImpl.class);

    private SkillsAwardedCoreApi skillsAwardedCoreApi;

    @Inject
    public SkillsAwardedUserApiImpl(SkillsAwardedCoreApi skillsAwardedCoreApi) {
        this.skillsAwardedCoreApi = skillsAwardedCoreApi;
    }

    @Override
    public List<SkillDto> getSkills() {
        log.info("Petición habilidades del usuario actualmente logeado");
        String currentUser = SecurityUtils.getCurrentUserLogin();
        return skillsAwardedCoreApi.getAllSkills(currentUser);
    }
}
