package ec.edu.utpl.datalab.codelogs.server.service.transfers.settings;

import org.springframework.hateoas.ResourceSupport;

/**
 * * Created by rfcardenas
 */
public class GoalSettingsTransfer extends ResourceSupport {
    private Long resource_id;
    private Long goalDay;
    private Long goalWeek;
    private Long goalMonth;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public Long getGoalDay() {
        return goalDay;
    }

    public void setGoalDay(Long goalDay) {
        this.goalDay = goalDay;
    }

    public Long getGoalWeek() {
        return goalWeek;
    }

    public void setGoalWeek(Long goalWeek) {
        this.goalWeek = goalWeek;
    }

    public Long getGoalMonth() {
        return goalMonth;
    }

    public void setGoalMonth(Long goalMonth) {
        this.goalMonth = goalMonth;
    }
}
