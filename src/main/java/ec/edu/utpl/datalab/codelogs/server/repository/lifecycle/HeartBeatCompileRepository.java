package ec.edu.utpl.datalab.codelogs.server.repository.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCompile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * * Created by rfcardenas
 */
public interface HeartBeatCompileRepository extends JpaRepository<HeartBeatCompile,Long> {
    @Query("SELECT hc FROM HeartBeatCompile hc WHERE hc.workSession.uuid =:worksession_uuid")
    Page<HeartBeatCompile> findAllByWorksession(@Param("worksession_uuid") String worksession_uuid, Pageable pageable);

    @Query("SELECT hc FROM HeartBeatCompile hc WHERE hc.workSession.project.uuid =:project_uuid")
    Page<HeartBeatCompile> findAllByProject(@Param("project_uuid") String project_uuid, Pageable pageable);
}
