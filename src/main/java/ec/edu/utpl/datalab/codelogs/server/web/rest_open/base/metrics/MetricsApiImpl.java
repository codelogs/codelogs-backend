package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.metrics;

import ec.edu.utpl.datalab.codelogs.server.repository.metrics.MetricRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.metrics.MetricMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.metrics.MetricDto;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
@RestController
public class MetricsApiImpl implements MetricsApi  {
    private MetricRepository metricRepository;
    private MetricMapper metricMapper;

    @Inject
    public MetricsApiImpl(MetricRepository metricRepository, MetricMapper metricMapper) {
        this.metricRepository = metricRepository;
        this.metricMapper = metricMapper;
    }

    @Override
    public ResponseEntity<List<MetricDto>> getAllMetrics(Pageable pageable) throws URISyntaxException {
        return ResponseEntity.ok()
            .body(metricRepository.findAll(pageable)
                .getContent()
                .stream()
                .map(metricMapper::map)
                .collect(Collectors.toList()));
    }
}
