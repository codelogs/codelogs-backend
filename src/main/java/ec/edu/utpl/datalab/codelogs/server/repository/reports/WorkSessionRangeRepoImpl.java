package ec.edu.utpl.datalab.codelogs.server.repository.reports;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.WorkSessionLessZTM;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.sql.Types;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@Repository
public class WorkSessionRangeRepoImpl implements WorkSessionRangeRepo {
    private static final String WORK_SESSION_RANGE= "SELECT \n" +
        "  work_session.*\n" +
        "FROM \n" +
        "  work_session  JOIN project  ON work_session.project_id= project.id WHERE \n" +
        "  project.uuid= :project AND\n" +
        "  work_session.session_start::date >= :startDate AND\n" +
        "  work_session.session_end::date   <= :endDate ORDER BY work_session.session_start ASC \n";


    @Inject
    public WorkSessionRangeRepoImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<WorkSessionLessZTM> workSession(String project, String startDate, String endDate) {

        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("startDate", startDate, Types.DATE);
        queryParams.addValue("endDate", endDate,Types.DATE);
        queryParams.addValue("project", project);

        return namedParameterJdbcTemplate
            .query(WORK_SESSION_RANGE,queryParams,new BeanPropertyRowMapper<>(WorkSessionLessZTM.class));
    }

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
}
