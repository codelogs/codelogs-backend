package ec.edu.utpl.datalab.codelogs.server.service.mapper.metrics;

import ec.edu.utpl.datalab.codelogs.server.domain.metrics.CodeAnalysis;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextRct;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle.HeartBeatMeasuresMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes.ProjectMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.metrics.CodeAnalysisDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    uses = {ProjectMapper.class,MetricMapper.class, HeartBeatMeasuresMapper.class, GrantedContextMapper.class, GrantedContextRct.class},
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CodeAnalysisMapper {
    @Mappings({
        @Mapping(source = "id",target = "resource_id" ),
        @Mapping(source = "heartBeatMeasures",target = "heartBeatCodeMeasures" )
    })
    CodeAnalysisDto map(CodeAnalysis source);

    @Mappings({
        @Mapping(source = "resource_id",target = "id" ),
        @Mapping(source = "heartBeatCodeMeasures",target = "heartBeatMeasures" )
    })
    CodeAnalysis map(CodeAnalysisDto source);
}
