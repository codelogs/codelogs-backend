package ec.edu.utpl.datalab.codelogs.server.domain.lifecycle;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * A WorkSession.
 */
@Entity
@Table(name = "work_session")
public class WorkSession implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid",unique = true)
    private String uuid;

    @Column(name = "session_start")
    private ZonedDateTime sessionStart;

    @Column(name = "session_end")
    private ZonedDateTime sessionEnd;

    @Column(name = "accum_time")
    private Long accumTime;

    @Column(name = "editor")
    private String editor;

    @Column(name = "terminal_uptime")
    private Long terminalUptime;

    @Column(name = "terminal_architecture")
    private String terminalArchitecture;

    @Column(name = "terminal_os")
    private String terminalOs;

    @Column(name = "terminal_ip")
    private String terminalIp;

    @Column(name = "terminal_mac")
    private String terminalMac;

    @Column(name = "terminal_name")
    private String terminalName;

    @Column(name = "terminal_cpu")
    private Integer terminalCpu;

    @Column(name = "terminal_temperature")
    private Integer terminalTemperature;

    @Column(name = "terminal_cores")
    private Integer terminalCores;

    @Column(name = "terminal_ram")
    private Long terminalRam;

    @Column(name = "terminal_ram_av")
    private Long terminalRamAv;

    @ManyToOne
    private Project project;


    @OneToMany(mappedBy = "workSession")
    @JsonIgnore
    private Collection<HeartBeatCompile> compiles = new ArrayList<>();

    @OneToMany(mappedBy = "workSession")
    @JsonIgnore
    private Collection<HeartBeatRun> runs = new ArrayList<>();


    @OneToMany(mappedBy = "workSession")
    @JsonIgnore
    private Set<HeartBeatCode> pulses = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(ZonedDateTime sessionStart) {
        this.sessionStart = sessionStart;
    }

    public ZonedDateTime getSessionEnd() {
        return sessionEnd;
    }

    public void setSessionEnd(ZonedDateTime sessionEnd) {
        this.sessionEnd = sessionEnd;
    }

    public Long getAccumTime() {
        return accumTime;
    }

    public void setAccumTime(Long accumTime) {
        this.accumTime = accumTime;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Long getTerminalUptime() {
        return terminalUptime;
    }

    public void setTerminalUptime(Long terminalUptime) {
        this.terminalUptime = terminalUptime;
    }

    public String getTerminalArchitecture() {
        return terminalArchitecture;
    }

    public void setTerminalArchitecture(String terminalArchitecture) {
        this.terminalArchitecture = terminalArchitecture;
    }

    public String getTerminalOs() {
        return terminalOs;
    }

    public void setTerminalOs(String terminalOs) {
        this.terminalOs = terminalOs;
    }

    public String getTerminalIp() {
        return terminalIp;
    }

    public void setTerminalIp(String terminalIp) {
        this.terminalIp = terminalIp;
    }

    public String getTerminalMac() {
        return terminalMac;
    }

    public void setTerminalMac(String terminalMac) {
        this.terminalMac = terminalMac;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public Integer getTerminalCpu() {
        return terminalCpu;
    }

    public void setTerminalCpu(Integer terminalCpu) {
        this.terminalCpu = terminalCpu;
    }

    public Integer getTerminalTemperature() {
        return terminalTemperature;
    }

    public void setTerminalTemperature(Integer terminalTemperature) {
        this.terminalTemperature = terminalTemperature;
    }

    public Integer getTerminalCores() {
        return terminalCores;
    }

    public void setTerminalCores(Integer terminalCores) {
        this.terminalCores = terminalCores;
    }

    public Long getTerminalRam() {
        return terminalRam;
    }

    public void setTerminalRam(Long terminalRam) {
        this.terminalRam = terminalRam;
    }

    public Long getTerminalRamAv() {
        return terminalRamAv;
    }

    public void setTerminalRamAv(Long terminalRamAv) {
        this.terminalRamAv = terminalRamAv;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Collection<HeartBeatCompile> getCompiles() {
        return compiles;
    }

    public void setCompiles(Collection<HeartBeatCompile> compiles) {
        this.compiles = compiles;
    }

    public Collection<HeartBeatRun> getRuns() {
        return runs;
    }

    public void setRuns(Collection<HeartBeatRun> runs) {
        this.runs = runs;
    }

    public Set<HeartBeatCode> getPulses() {
        return pulses;
    }

    public void setPulses(Set<HeartBeatCode> pulses) {
        this.pulses = pulses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WorkSession workSession = (WorkSession) o;
        if(workSession.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, workSession.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "WorkSession{" +
            "id=" + id +
            ", uuid='" + uuid + "'" +
            ", sessionStart='" + sessionStart + "'" +
            ", sessionEnd='" + sessionEnd + "'" +
            ", accumTime='" + accumTime + "'" +
            ", editor='" + editor + "'" +
            ", terminalUptime='" + terminalUptime + "'" +
            ", terminalArchitecture='" + terminalArchitecture + "'" +
            ", terminalOs='" + terminalOs + "'" +
            ", terminalIp='" + terminalIp + "'" +
            ", terminalMac='" + terminalMac + "'" +
            ", terminalName='" + terminalName + "'" +
            ", terminalCpu='" + terminalCpu + "'" +
            ", terminalTemperature='" + terminalTemperature + "'" +
            ", terminalCores='" + terminalCores + "'" +
            ", terminalRam='" + terminalRam + "'" +
            ", terminalRamAv='" + terminalRamAv + "'" +
            '}';
    }
}
