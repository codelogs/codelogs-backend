package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.skills;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.skill.LevelSkillDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.skill.SkillDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@RequestMapping("/api/v1/skills/catalog")
@Api(value = "/api/v1/skills",
    authorizations = {},
    tags = {"skills"},
    description = "Retorna un punto en la serie de tiempo, de la actividad del programdor")
public interface LevelsSkillsCoreApi {
    @ApiOperation(
        value = "retorna una lista de habilidades registradas, este es un catalogo de las habilidades" +
            "que se pueden ganar en un lenguaje de programación",
        nickname = "get_skills_user",
        response = SkillDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    List<LevelSkillDto> getAllLevels();
}
