package ec.edu.utpl.datalab.codelogs.server.repository.reports;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimeStatusProgrammerDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@Repository
public class TimeStateProgrammerRepoImpl implements TimeStateProgrammerRepo {
    private static final Logger log = LoggerFactory.getLogger(TimeStateProgrammerRepoImpl.class);
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    private final String WORK_ACTIVITY_LAST ="SELECT ws.accum_time timeActive ,DATE_PART('minute',ws.session_start) - DATE_PART('minute',ws.session_end) totalTime \n" +
        ", ws.session_start session_start, ws.session_end lastbeat, ws.editor ide , p.name project, ws.uuid uuid_session \n" +
        "        FROM WORK_SESSION ws\n" +
        "        LEFT JOIN PROJECT p ON ws.project_id = p.id\n" +
        "        where p.uuid = :uuid_project \n" +
        "        ORDER BY ws.id desc\n" +
        "        limit 1";


    private final String  TIME_STATE_RANGE_GLOBAL = "SELECT \n" +
        "  work_session.accum_time, work_session.session_start, work_session.session_end, work_session.editor ,project.name as project, work_session.uuid\n" +
        "FROM  \n" +
        "  work_session JOIN project ON work_session.project_id =  project.id\n" +
        "  JOIN programmer ON project.programmer_id = programmer.id\n" +
        "  WHERE work_session.session_start::date>=:fromDate AND\n" +
        "  work_session.session_end::date<=:toDate AND\n" +
        "  programmer.username  = :username\n" +
        "  ORDER BY work_session.session_start desc";

    private final String  TIME_STATE_RANGE_PROJECT = "SELECT \n" +
        "  work_session.accum_time, work_session.session_start, work_session.session_end, work_session.editor ,project.name as project, work_session.uuid\n" +
        "FROM  \n" +
        "  work_session JOIN project ON work_session.project_id =  project.id\n" +
        "  JOIN programmer ON project.programmer_id = programmer.id\n" +
        "  WHERE work_session.session_start::date>=:fromDate AND\n" +
        "  work_session.session_end::date<=:toDate AND\n" +
        "  programmer.username  = :username\n AND" +
        "  project.uuid  = :project_uuid\n " +
        "  ORDER BY work_session.session_start desc";

    @Inject
    public TimeStateProgrammerRepoImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }
    @Override
    public List<TimeStatusProgrammerDto> queryActivityWork(String uuid_project) {
       throw new UnsupportedOperationException("Query no soportada");
    }

    @Override
    public List<TimeStatusProgrammerDto> timeStateGlobal(String username, String fromDate, String toDate) {
        log.info("Query custom range logg time user {}  start {} end {}" ,username,fromDate,toDate);
        RowMapper<TimeStatusProgrammerDto> rowMapper = (rs, rowNum) -> this.mapRow(rs,rowNum);

        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("username", username);
        queryParams.addValue("fromDate", fromDate, Types.DATE);
        queryParams.addValue("toDate", toDate,Types.DATE);
        return namedParameterJdbcTemplate
            .query(TIME_STATE_RANGE_GLOBAL, queryParams, rowMapper);
    }


    @Override
    public List<TimeStatusProgrammerDto> timeStateProject(String username, String project_uuid, String fromDate, String toDate) {
        log.info("Query custom range logg time user {}  start {} end {}" ,username,fromDate,toDate);
        RowMapper<TimeStatusProgrammerDto> rowMapper = (rs, rowNum) -> this.mapRow(rs,rowNum);

        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("username", username);
        queryParams.addValue("project_uuid", project_uuid);
        queryParams.addValue("fromDate", fromDate, Types.DATE);
        queryParams.addValue("toDate", toDate,Types.DATE);
        return namedParameterJdbcTemplate
            .query(TIME_STATE_RANGE_PROJECT, queryParams, rowMapper);
    }

    private TimeStatusProgrammerDto mapRow(ResultSet rs, int rowNum) throws SQLException{

        TimeStatusProgrammerDto activityPulseDto = new TimeStatusProgrammerDto();
        activityPulseDto.setTimeActive(rs.getLong(1));
        activityPulseDto.setStartSession(rs.getTimestamp(2));
        activityPulseDto.setEndSession(rs.getTimestamp(3));
        activityPulseDto.setEditor(rs.getString(4));
        activityPulseDto.setProject(rs.getString(5));
        return activityPulseDto;
    }
}
