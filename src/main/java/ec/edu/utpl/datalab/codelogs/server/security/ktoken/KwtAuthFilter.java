package ec.edu.utpl.datalab.codelogs.server.security.ktoken;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.util.StringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * * Created by rfcardenas
 */
public class KwtAuthFilter extends AbstractAuthenticationProcessingFilter {

    public KwtAuthFilter(String defaultFilterProcessesUrl) {
        super(defaultFilterProcessesUrl);
    }

    private TokenProviderKw tokenProvider = new TokenProviderKw();

    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
        String header = ((HttpServletRequest)req).getHeader(TokenAuthorization.TOKEN_HEADER);
        //This filter only applies if the header is present
        if(header!= null && header.startsWith("KWT-Bearer ")) {
            System.out.println("********************************* OK EXE JWT");
            String jwt = resolveToken((HttpServletRequest) req);
            if (StringUtils.hasText(jwt)) {
                if (this.tokenProvider.validateToken(jwt)) {
                    Authentication authentication = this.tokenProvider.getAuthentication(jwt);
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
            super.doFilter(req, res, chain);
        }else{
            chain.doFilter(req, res);
        }
        //On success keep going on the chain
        this.setAuthenticationSuccessHandler(new AuthenticationSuccessHandler() {
            @Override
            public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
                chain.doFilter(request, response);
            }
        });
    }
    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        String tokenReq = resolveToken(httpServletRequest);
        if(tokenReq!=null){
            KwtToken apiToken = new KwtToken(tokenReq);
            apiToken.setAuthenticated(false);
            return getAuthenticationManager().authenticate(apiToken);
        }else{
            return null;
        }
    }
    private String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader(TokenAuthorization.TOKEN_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("KWT-Bearer ")) {
            String jwt = bearerToken.substring(11, bearerToken.length());
            return jwt;
        }
        return null;
    }


}
