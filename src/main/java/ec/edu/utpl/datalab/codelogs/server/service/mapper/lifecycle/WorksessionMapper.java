package ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextRct;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.WorkSessionDto;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.WorkSessionPack;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    uses =
        {
            GrantedContextMapper.class,
            GrantedContextRct.class,
            HeartBeatRunMapper.class,
            HeartBeatCompilerMapper.class
        },
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface WorksessionMapper {
    @Mappings({
        @Mapping(source = "resource_id", target = "id", ignore = true),
    })
    WorkSession map(WorkSessionDto source);
    WorkSessionDto map(WorkSession source);
    @Mappings({
        @Mapping(source = "resource_id", target = "id", ignore = true),
    })
    WorkSession map(WorkSessionPack source);

}
