package ec.edu.utpl.datalab.codelogs.server.service.transfers.skill;

/**
 * * Created by rfcardenas
 */
public class LevelSkillDto {
    private Long id;
    private int level;
    private long min_points;
    private long max_points;
    private String name;
    private String label;
    private String acronimo;
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getMin_points() {
        return min_points;
    }

    public void setMin_points(long min_points) {
        this.min_points = min_points;
    }

    public long getMax_points() {
        return max_points;
    }

    public void setMax_points(long max_points) {
        this.max_points = max_points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "LevelSkillDto{" +
            "id=" + id +
            ", level=" + level +
            ", min_points=" + min_points +
            ", max_points=" + max_points +
            ", name='" + name + '\'' +
            ", label='" + label + '\'' +
            ", acronimo='" + acronimo + '\'' +
            ", description='" + description + '\'' +
            '}';
    }
}
