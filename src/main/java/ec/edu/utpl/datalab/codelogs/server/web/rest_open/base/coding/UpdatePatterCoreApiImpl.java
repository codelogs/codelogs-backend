package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.coding;

import ec.edu.utpl.datalab.codelogs.server.repository.reports.WorkActivityRepository;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.Dataset;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.MeasureDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.SuperDataSet;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.WorkActivityPulseDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class UpdatePatterCoreApiImpl implements UpdatePatterCoreApi {
    private WorkActivityRepository workActivityRepository;

    @Inject
    public UpdatePatterCoreApiImpl(WorkActivityRepository workActivityRepository) {
        this.workActivityRepository = workActivityRepository;
    }

    @Override
    public SuperDataSet dataSetUpdatePatternUser(@PathVariable("user") String user) {
        List<WorkActivityPulseDto> data = workActivityRepository.workActivity(user);
        SuperDataSet dataSet = summaryBehaviorPattern(data);
        return dataSet;
    }

    @Override
    public SuperDataSet dataSetUpdatePatternProjectUser(@PathVariable("user") String user, @PathVariable("project") String project) {
        List<WorkActivityPulseDto> data = workActivityRepository.workActivity(user,project);
        SuperDataSet dataSet = summaryBehaviorPattern(data);
        return dataSet;
    }
    public SuperDataSet summaryBehaviorPattern(List<WorkActivityPulseDto> data){
        Dataset vecData[] = new Dataset[3];
        vecData[0] =  new Dataset();
        vecData[0].setName("lines add");
        vecData[0].setUnit("lines add");
        vecData[0].setMaxDecimal(0);
        vecData[0].setType("line");
        vecData[1] =  new Dataset();
        vecData[1].setName("lines del");
        vecData[1].setUnit("lines del");
        vecData[1].setMaxDecimal(0);
        vecData[1].setType("area");
        vecData[2] =  new Dataset();
        vecData[2].setName("Effort");
        vecData[2].setUnit("Effort");
        vecData[2].setMaxDecimal(0);
        vecData[2].setType("area");

        SuperDataSet superDataSet = new SuperDataSet();
        superDataSet.setDatasets(Arrays.asList(vecData));
        HashSet<String> x = new HashSet<>();
        data.forEach(dto -> {
            x.add(dto.getCapturedate());
        });
        superDataSet.setxData(x);
        //populate lines add
        data.stream().filter(dto -> dto.getLinesadd()>0)
            .forEach(dto -> {
                MeasureDto measureDto = new MeasureDto();
                measureDto.setCaptureDate(dto.getCapturedate());
                measureDto.setValue(dto.getLinesadd());
                vecData[0].getData().add(measureDto);
            });
        data.stream().filter(dto -> dto.getLinesdel()>0)
            .forEach(dto -> {
                MeasureDto measureDto = new MeasureDto();
                measureDto.setCaptureDate(dto.getCapturedate());
                measureDto.setValue(dto.getLinesdel());
                vecData[1].getData().add(measureDto);
            });
        data.stream().filter(dto -> dto.getLevenshtein()>0)
            .forEach(dto -> {
                MeasureDto measureDto = new MeasureDto();
                measureDto.setCaptureDate(dto.getCapturedate());
                measureDto.setValue(dto.getLevenshtein());
                vecData[2].getData().add(measureDto);
            });

        return superDataSet;

    }

}
