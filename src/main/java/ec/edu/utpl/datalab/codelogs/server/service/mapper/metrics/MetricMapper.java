package ec.edu.utpl.datalab.codelogs.server.service.mapper.metrics;

import ec.edu.utpl.datalab.codelogs.server.domain.metrics.Metric;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.metrics.MetricDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MetricMapper {
    @Mappings({
        @Mapping(source = "id",target = "resource_id" )
    })
    MetricDto map(Metric source);

    @Mappings({
        @Mapping(source = "resource_id",target = "id" )
    })
    Metric map(MetricDto source);
}
