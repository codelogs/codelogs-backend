package ec.edu.utpl.datalab.codelogs.server.repository.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.PulseCodeCronoDto;

import java.util.List;

/**
 * Resumen detallado de las actividades de programacion de un usuario,
 * este resumen es creado unicamente para las ultimas sessiones y se presenta la actividad
 * en los ultimos 5 archivos
 * * Created by rfcardenas
 */
public interface PulsesCronoRepository {
    List<PulseCodeCronoDto> getSummaryLastWorkFile(String uuid_project, String username);
}
