package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.time;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimePointDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimeStatusProgrammerDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/time/points",
    authorizations = {},
    tags = {"time"},
    description = "Api util para acceder a los datos de actividad de programación")
@RequestMapping("/api/v1/time/points")
public interface TimeLinePointCoreApi{
    @ApiOperation(
        value = "Retorna información de un punto de tiempo en un proyecto",
        response = TimeStatusProgrammerDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/projects/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    TimePointDto getTimePoint(
        @PathVariable("project") String project,
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );
}
