package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.goals;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.coding.BuildRunDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.goals.GoalTransffer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URISyntaxException;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/goals/",
    authorizations = {},
    tags = {"goals","goal progress"},
    description = "API retorna el avance de una meta")
@RequestMapping("/api/v1/goals/")
public interface GoalProgressApi {
    @ApiOperation(
        value = "Retorna el avance de las metas del usuario actualmente logeado",
        nickname = "metas semanales",
        response = BuildRunDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping(value = "week/project/{project_uuid}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    List<GoalTransffer> getWeekGoalProgress(
        @PathVariable("project_uuid") String project_uuid) throws URISyntaxException;
}
