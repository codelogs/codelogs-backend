/**
 * Paquete contiene entidades del ciclo de la actividad del estudiante
 * conteniendo datos y metricas a recolectar
 * * Created by rfcardenas
 */
package ec.edu.utpl.datalab.codelogs.server.domain.lifecycle;
