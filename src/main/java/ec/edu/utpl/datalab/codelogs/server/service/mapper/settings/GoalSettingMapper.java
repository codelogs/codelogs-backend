package ec.edu.utpl.datalab.codelogs.server.service.mapper.settings;

import ec.edu.utpl.datalab.codelogs.server.domain.settings.GoalSettings;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.settings.GoalSettingsTransfer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface GoalSettingMapper {
    @Mappings({
        @Mapping(source = "id",target = "resource_id",ignore = false),
    })
    GoalSettingsTransfer map(GoalSettings source);

    @Mappings({
        @Mapping(source = "resource_id",target = "id",ignore = false),
    })
    GoalSettings map(GoalSettingsTransfer source);
}
