package ec.edu.utpl.datalab.codelogs.server.domain.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.enumeration.TYPE_INSTRUCTION;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.FileCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A PulseCode.
 */
@Entity
@Table(name = "heart_beats_code")
public class HeartBeatCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid",unique = true)
    private String uuid;

    @Column(name = "capture_date")
    private ZonedDateTime captureDate;

    @Column(name = "path_file")
    private String pathFile;


    @Column(name = "lines_add")
    private Long linesAdd;

    @Column(name = "lines_del")
    private Long linesDel;

    @Column(name = "levenshtein")
    private Long levenshtein;

    @Column(name = "time_fixed")
    private Long timeFixed;

    @Column(name = "block_map")
    private String blockMap;

    @Column(name = "brachname")
    private String brachname;

    @Column(name = "lang")
    private String lang;

    @Column(name ="type_instruction")
    private TYPE_INSTRUCTION instruction;

    @ManyToOne
    private FileCode fileCode;

    @ManyToOne
    private WorkSession workSession;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(ZonedDateTime captureDate) {
        this.captureDate = captureDate;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public Long getLinesAdd() {
        return linesAdd;
    }

    public void setLinesAdd(Long linesAdd) {
        this.linesAdd = linesAdd;
    }

    public Long getLinesDel() {
        return linesDel;
    }

    public void setLinesDel(Long linesDel) {
        this.linesDel = linesDel;
    }

    public Long getLevenshtein() {
        return levenshtein;
    }

    public void setLevenshtein(Long levenshtein) {
        this.levenshtein = levenshtein;
    }

    public Long getTimeFixed() {
        return timeFixed;
    }

    public void setTimeFixed(Long timeFixed) {
        this.timeFixed = timeFixed;
    }

    public String getBlockMap() {
        return blockMap;
    }

    public void setBlockMap(String blockMap) {
        this.blockMap = blockMap;
    }

    public String getBrachname() {
        return brachname;
    }

    public void setBrachname(String brachname) {
        this.brachname = brachname;
    }

    public FileCode getFileCode() {
        return fileCode;
    }

    public void setFileCode(FileCode fileCode) {
        this.fileCode = fileCode;
    }

    public WorkSession getWorkSession() {
        return workSession;
    }

    public void setWorkSession(WorkSession workSession) {
        this.workSession = workSession;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public TYPE_INSTRUCTION getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION instruction) {
        this.instruction = instruction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeartBeatCode heartBeatCode = (HeartBeatCode) o;
        if(heartBeatCode.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, heartBeatCode.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "HeartBeatCode{" +
            "id=" + id +
            ", uuid='" + uuid + '\'' +
            ", captureDate=" + captureDate +
            ", pathFile='" + pathFile + '\'' +
            ", linesAdd=" + linesAdd +
            ", linesDel=" + linesDel +
            ", levenshtein=" + levenshtein +
            ", timeFixed=" + timeFixed +
            ", blockMap='" + blockMap + '\'' +
            ", brachname='" + brachname + '\'' +
            ", lang='" + lang + '\'' +
            ", instruction=" + instruction +
            ", fileCode=" + fileCode +
            ", workSession=" + workSession +
            '}';
    }
}
