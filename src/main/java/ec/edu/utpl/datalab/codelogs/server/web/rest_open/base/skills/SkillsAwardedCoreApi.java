package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.skills;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.skill.SkillDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/skills",
    authorizations = {},
    tags = {"skills"},
    description = "Habilidades adquiridas en los lenguajes de programación segun el tiempo de practica")
@RequestMapping("/api/v1/skills")
public interface SkillsAwardedCoreApi {
    @ApiOperation(
        value = "API retorna las habilidades de un usuario en los lenguajes de programación utilizados" +
            "estas habilidades estan fundamentadas en el tiempo que un programador dedica a un lenguaje" +
            "de programación en especifico",
        nickname = "get_skills_user",
        response = SkillDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    List<SkillDto> getAllSkills(@PathVariable("user") String user);
}
