package ec.edu.utpl.datalab.codelogs.server.service.mapper.settings;

import ec.edu.utpl.datalab.codelogs.server.domain.settings.ThemeSettings;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.settings.ThemeSettingsTransfer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ThemelSettingMapper {
    @Mappings({
        @Mapping(source = "id",target = "resource_id",ignore = false),
    })
    ThemeSettingsTransfer map(ThemeSettings source);

    @Mappings({
        @Mapping(source = "resource_id",target = "id",ignore = false),
    })
    ThemeSettings map(ThemeSettingsTransfer source);
}
