package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.sessions;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.WorkSessionDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URISyntaxException;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/sessions/",
    authorizations = {},
    tags = {"sessions"},
    description = "Sesiones de trabajo")
@RequestMapping("/api/v1/sessions/")
public interface SessionUserApi {
    @ApiOperation(
        value = "retorna las sesiones  del usuario alctualmente logeado",
        nickname = "sesiones",
        response = WorkSessionDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping(value = "/all",
        produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<WorkSessionDto>> getAllSessions(Pageable pageable) throws URISyntaxException;

    @ApiOperation(
        value = "retorna las sesiones recientes del usuario actualmente logeado",
        nickname = "sesiones",
        response = WorkSessionDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping(value = "/recent",
        produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<WorkSessionDto>> getSessionsRecent(Pageable pageable) throws URISyntaxException;
}
