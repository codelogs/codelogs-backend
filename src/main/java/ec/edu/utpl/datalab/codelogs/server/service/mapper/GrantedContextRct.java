package ec.edu.utpl.datalab.codelogs.server.service.mapper;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCode;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.FileCode;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring")
public interface GrantedContextRct {
    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    HeartBeatCode mapToPulse(GrantedContextDto source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    WorkSession mapToWorkSession(GrantedContextDto source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    Project mapToProject(GrantedContextDto source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    FileCode mapToFile(GrantedContextDto source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    Project mapToProject(GrantedContextPack source);
}
