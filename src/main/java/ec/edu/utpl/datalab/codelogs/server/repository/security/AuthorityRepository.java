package ec.edu.utpl.datalab.codelogs.server.repository.security;

import ec.edu.utpl.datalab.codelogs.server.domain.security.Authority;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, String> {
}
