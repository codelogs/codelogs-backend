package ec.edu.utpl.datalab.codelogs.server.service.transfers.security;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

import java.time.ZonedDateTime;

/**
 * * Created by rfcardenas
 */
@JsonRootName("spider_kwtoken")
public class TokenAccessDto {
    @JsonProperty("token")
    private String apikey;
    @JsonProperty("header")
    private String header;
    @JsonProperty("enable")
    private Boolean enable;
    @JsonProperty("create_date")
    private ZonedDateTime createAt;
    @JsonProperty("generation_number")
    private int generation;

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public ZonedDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(ZonedDateTime createAt) {
        this.createAt = createAt;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    @Override
    public String toString() {
        return "TokenAccessDto{" +
            "apikey='" + apikey + '\'' +
            ", header='" + header + '\'' +
            ", enable=" + enable +
            ", createAt=" + createAt +
            ", generation=" + generation +
            '}';
    }
}
