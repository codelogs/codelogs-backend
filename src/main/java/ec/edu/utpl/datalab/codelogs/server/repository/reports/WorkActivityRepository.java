package ec.edu.utpl.datalab.codelogs.server.repository.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.WorkActivityPulseDto;

import java.util.List;

/**
 * * Created by rfcardenas
 */
public interface WorkActivityRepository {
       List<WorkActivityPulseDto> workActivity(String user);
    List<WorkActivityPulseDto> workActivity(String user, String projectUuid);
}
