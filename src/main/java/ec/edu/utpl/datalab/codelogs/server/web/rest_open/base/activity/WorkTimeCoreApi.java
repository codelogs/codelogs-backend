package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.activity;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.security.AuthoritiesConstants;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeProjectDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@RequestMapping("api/v1/work_time")
@Api(value = "api/v1/work_time",
    authorizations = {},
    tags = {"work_time","time"},
    description = "Proporciona información sobre el trabajo del programador (Actividad conforme codifica la solución)")
public interface WorkTimeCoreApi {

    @ApiOperation(
        value = "Retorna datos simples sobre la cantidad de tiempo de programación  en un intervalo de fechas" +
            "EJM : start = 2015-12-12 end = 2017-03-12 no proporciona detalles sobre el proyecto",
        nickname = "get_work_time",
        response = LoggTimeDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}/lapse",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<List<LoggTimeDto>> getSimpleLogTime(
        @PathVariable("user") String user,
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );



    /**
     * @param user
     * @param project
     * @param start
     * @param end
     * @return
     */
    @ApiOperation(
        value = "Retorna información detallada sobre el tiempo asignado en un proyecto específico en un rango de fechas",
        nickname = "get_work_time_in_project",
        response = LoggTimeProjectDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}/projects/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured({AuthoritiesConstants.USER})
    ResponseEntity<LoggTimeProjectDto> getWorkTimeProject(
        @PathVariable("user") String user,
        @PathVariable("project") String project,
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );

    @ApiOperation(
        value = "Retorna el total del tiempo de un programador en los diferentes proyectos en un rango de fechas",
        nickname = "get_work_time_all_projects",
        response = LoggTimeProjectDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}/projects/all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured({AuthoritiesConstants.USER})
    ResponseEntity<List<LoggTimeProjectDto>> getGlobalWorkTimeAllProjects(
        @RequestParam("start") String start,
        @RequestParam("end") String end,
        @PathVariable("user") String user
    );
}
