package ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Languaje;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.LanguajeDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface LanguajeMapper {
    Languaje map(LanguajeDto source);
    LanguajeDto map(Languaje source);
}
