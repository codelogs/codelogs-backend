package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.coding;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.HeartBeatRunRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle.HeartBeatRunMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.coding.BuildRunDto;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;


/**
 * * Created by rfcardenas
 */
@RestController
public class RunCoreApiImpl implements RunCoreApi {
    private EntityLinks entityLinks;
    private HeartBeatRunRepository runRepository;
    private HeartBeatRunMapper beatRunMapper;

    @Inject
    public RunCoreApiImpl(EntityLinks entityLinks, HeartBeatRunRepository runRepository, HeartBeatRunMapper beatRunMapper) {
        this.entityLinks = entityLinks;
        this.runRepository = runRepository;
        this.beatRunMapper = beatRunMapper;
    }

    @Override
    public List<BuildRunDto> getBuildCollectionProject(@PathVariable("project_uuid") String project_uuid, Pageable pageable) {
        Link link = entityLinks.linkForSingleResource(Project.class,project_uuid)
            .withRel("project");
        return runRepository.findAllByProject(project_uuid,pageable)
            .getContent()
            .stream()
            .map((entry)->{
                BuildRunDto data = beatRunMapper.mapTo(entry);
                data.add(link);
                return data;
            })
            .collect(Collectors.toList());
    }

    @Override
    public List<BuildRunDto> getBuildCollectionSession(@PathVariable("session_uuid") String session_uuid, Pageable pageable) {


        return runRepository.findAllByWorksession(session_uuid,pageable)
            .getContent()
            .stream()
            .map(beatRunMapper::mapTo)
            .collect(Collectors.toList());
    }
}
