package ec.edu.utpl.datalab.codelogs.server.web.rest_open.security;

import ec.edu.utpl.datalab.codelogs.server.domain.security.Programmer;
import ec.edu.utpl.datalab.codelogs.server.domain.security.TokenAccess;
import ec.edu.utpl.datalab.codelogs.server.domain.security.User;
import ec.edu.utpl.datalab.codelogs.server.repository.security.ProgrammerRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.security.TokenAccessRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.security.UserRepository;
import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.security.ktoken.TokenProviderKw;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.security.TokenAccessDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.time.ZonedDateTime;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
@RestController
public class TokenResource implements TokenGeneratorApi{
    private final Logger log = LoggerFactory.getLogger(TokenResource.class);

    private TokenAccessRepository tokenMonitorRepository;
    private UserRepository userRepository;
    private ProgrammerRepository programmerRepository;
    @Inject
    public TokenResource(TokenAccessRepository tokenMonitorRepository, UserRepository userRepository, ProgrammerRepository programmerRepository) {
        this.tokenMonitorRepository = tokenMonitorRepository;
        this.userRepository = userRepository;
        this.programmerRepository = programmerRepository;
    }

    @Override
    public ResponseEntity<TokenAccessDto> generate(){
        String userLogin = SecurityUtils.getCurrentUserLogin();
        Optional<User> userData = userRepository.findOneByLogin(userLogin);
        Optional<Programmer> referenceProgrammer = programmerRepository.findOneByUsername(userLogin);
        if(!userData.isPresent()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }
        if(!referenceProgrammer.isPresent()){
            log.info("registrando programador ref");
            Programmer programmer = new Programmer();
            programmer.setUser(userData.get());
            programmer.setUsername(userData.get().getLogin());
            programmerRepository.save(programmer);
            referenceProgrammer = Optional.ofNullable(programmer);
        }
        if(referenceProgrammer.isPresent()){
            // se guarda el usuario
            // se crea access token
            TokenProviderKw token = new TokenProviderKw();
            String tokenkey = token.createToken(userLogin);
            TokenAccess apiAccess = new TokenAccess();
            apiAccess.setApikey(tokenkey);
            apiAccess.setCreateAt(ZonedDateTime.now());
            apiAccess.setEnable(true);
            apiAccess.setProgrammer(referenceProgrammer.get());
            apiAccess.setHeader("KBearer");
            System.out.println("API ACCSS " + apiAccess);
            tokenMonitorRepository.save(apiAccess);

            TokenAccessDto tokenMonitorDto = new TokenAccessDto();
            tokenMonitorDto.setApikey(apiAccess.getApikey());
            System.out.println(tokenMonitorDto);
            return ResponseEntity.ok(tokenMonitorDto);
        }
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(null);
    }
}
