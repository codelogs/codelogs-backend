package ec.edu.utpl.datalab.codelogs.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * * Created by rfcardenas
 */
@ResponseStatus(value= HttpStatus.EXPECTATION_FAILED,reason="no se puede crear el token de acceso")
public class TokenFaildCreate  extends RuntimeException{
    public TokenFaildCreate(String  username) {
        super("No se puede crear un token de acceso para el usuario " + username);
    }
}
