package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.coding;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.SuperDataSet;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
@RestController
public class UpdatePatternUserApiImpl implements UpdatePatternUserApi {
    private UpdatePatterCoreApi updatePatterCoreApi;
    private EntityLinks entityLinks;

    @Inject
    public UpdatePatternUserApiImpl(UpdatePatterCoreApi updatePatterCoreApi, EntityLinks entityLinks) {
        this.updatePatterCoreApi = updatePatterCoreApi;
        this.entityLinks = entityLinks;
    }

    @Override
    public SuperDataSet dataSetUpdatePattern() {
        String username = SecurityUtils.getCurrentUserLogin();
        SuperDataSet data = updatePatterCoreApi.dataSetUpdatePatternUser(username);
        return data;
    }

    @Override
    public SuperDataSet dataSetUpdatePatternProject(@PathVariable("project") String project) {
        String username = SecurityUtils.getCurrentUserLogin();
        Link link = entityLinks.linkToSingleResource(Project.class, project)
            .withRel("project")
            .withSelfRel();
        SuperDataSet dataSet = updatePatterCoreApi.dataSetUpdatePatternProjectUser(username, project);
        dataSet.add(link);
        return dataSet;
    }
}
