package ec.edu.utpl.datalab.codelogs.server.repository.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LanguajeUsageDto;

import java.util.List;

/**
 * * Created by rfcardenas
 */
public interface LanguajeUsageRepository {
    List<LanguajeUsageDto> usageLangList(String username);
    List<LanguajeUsageDto> usageLangWorkProjectRange(String username, String project_uuid, String fromDate, String toDate);
    List<LanguajeUsageDto> usageLangWorkGlobalRange(String username, String fromDate, String toDate);
}
