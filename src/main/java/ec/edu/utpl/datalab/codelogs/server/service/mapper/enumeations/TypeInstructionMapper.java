package ec.edu.utpl.datalab.codelogs.server.service.mapper.enumeations;

import ec.edu.utpl.datalab.codelogs.server.domain.enumeration.TYPE_INSTRUCTION;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.enumeations.TYPE_INSTRUCTION_PUBLIC;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.TYPE_INSTRUCTION_PACK;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface TypeInstructionMapper {
    @Mappings({
        @Mapping(source = "HEART_BEAT_COMPILE_SUCCESS", target = "HEART_BEAT_COMPILE_SUCCESS"),
        @Mapping(source = "HEART_BEAT_COMPILE_FAILED", target = "HEART_BEAT_COMPILE_FAILED"),
        @Mapping(source = "HEART_BEAT_RUN_SUCCESS", target = "HEART_BEAT_RUN_SUCCESS"),
        @Mapping(source = "HEART_BEAT_RUN_FAILED", target = "HEART_BEAT_RUN_FAILED"),
        @Mapping(source = "HEART_BEAT_CODE_ACTIVITY", target = "HEART_BEAT_CODE_ACTIVITY"),
        @Mapping(source = "HEART_BEAT_CODE_METRIC", target = "HEART_BEAT_CODE_METRIC"),
        @Mapping(source = "HEART_BEAT_CODE_ANALYSIS", target = "HEART_BEAT_CODE_ANALYSIS"),
    })
    TYPE_INSTRUCTION map(TYPE_INSTRUCTION_PUBLIC source);

    @Mappings({
        @Mapping(source = "HEART_BEAT_COMPILE_SUCCESS", target = "HEART_BEAT_COMPILE_SUCCESS"),
        @Mapping(source = "HEART_BEAT_COMPILE_FAILED", target = "HEART_BEAT_COMPILE_FAILED"),
        @Mapping(source = "HEART_BEAT_RUN_SUCCESS", target = "HEART_BEAT_RUN_SUCCESS"),
        @Mapping(source = "HEART_BEAT_RUN_FAILED", target = "HEART_BEAT_RUN_FAILED"),
        @Mapping(source = "HEART_BEAT_CODE_ACTIVITY", target = "HEART_BEAT_CODE_ACTIVITY"),
        @Mapping(source = "HEART_BEAT_CODE_METRIC", target = "HEART_BEAT_CODE_METRIC"),
        @Mapping(source = "HEART_BEAT_CODE_ANALYSIS", target = "HEART_BEAT_CODE_ANALYSIS"),
    })
    TYPE_INSTRUCTION map(TYPE_INSTRUCTION_PACK source);

    @Mappings({
        @Mapping(source = "HEART_BEAT_COMPILE_SUCCESS", target = "HEART_BEAT_COMPILE_SUCCESS"),
        @Mapping(source = "HEART_BEAT_COMPILE_FAILED", target = "HEART_BEAT_COMPILE_FAILED"),
        @Mapping(source = "HEART_BEAT_RUN_SUCCESS", target = "HEART_BEAT_RUN_SUCCESS"),
        @Mapping(source = "HEART_BEAT_RUN_FAILED", target = "HEART_BEAT_RUN_FAILED"),
        @Mapping(source = "HEART_BEAT_CODE_ACTIVITY", target = "HEART_BEAT_CODE_ACTIVITY"),
        @Mapping(source = "HEART_BEAT_CODE_METRIC", target = "HEART_BEAT_CODE_METRIC"),
        @Mapping(source = "HEART_BEAT_CODE_ANALYSIS", target = "HEART_BEAT_CODE_ANALYSIS"),
    })
    TYPE_INSTRUCTION_PUBLIC map(TYPE_INSTRUCTION source);


}
