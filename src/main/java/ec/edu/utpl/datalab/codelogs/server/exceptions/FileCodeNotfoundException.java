package ec.edu.utpl.datalab.codelogs.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * * Created by rfcardenas
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND,reason="This file not found in server")
public class FileCodeNotfoundException extends RuntimeException {
    public FileCodeNotfoundException(String uuid) {
        super(String.format("El archivo de codigo %s no fue encontrado en este servidor",uuid));
    }


    public FileCodeNotfoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileCodeNotfoundException(Throwable cause) {
        super(cause);
    }

    protected FileCodeNotfoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
