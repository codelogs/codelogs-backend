package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.projects;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.WorkSessionRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.ProjectRepository;
import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes.ProjectMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;
import ec.edu.utpl.datalab.codelogs.server.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
@RestController
public class ProjectsCoreApiImpl implements ProjectsCoreApi {
    private final Logger log = LoggerFactory.getLogger(ProjectsCoreApiImpl.class);

    private ProjectRepository projectRepository;
    private ProjectMapper projectMapper;
    private WorkSessionRepository workSessionRepository;

    @Inject
    public ProjectsCoreApiImpl(ProjectRepository projectRepository,
                               ProjectMapper projectMapper,
                               WorkSessionRepository workSessionRepository) {
        this.projectRepository = projectRepository;
        this.projectMapper = projectMapper;
        this.workSessionRepository = workSessionRepository;
    }

    @Override
    public ResponseEntity<List<ProjectDto>> getAllProjects(@PathVariable(name = "user") String user, Pageable pageable) throws URISyntaxException {
        log.debug("REST request to get all Projects");
        String currentUserLogin = SecurityUtils.getCurrentUserLogin();
        List response = Collections.emptyList();
        if (Objects.isNull(user)) {
            log.info("Request current user {}", currentUserLogin);
            Page<Project> page = projectRepository.findAllFromUser(currentUserLogin, pageable);
            List<ProjectDto> managedUserVMs = page.getContent().stream()
                .map(projectMapper::map)
                .collect(Collectors.toList());
            HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ui/projects");
            return new ResponseEntity<>(managedUserVMs, headers, HttpStatus.OK);
        }
        log.info("Request current user admin {} -> target user {}", currentUserLogin, user);
        Page<Project> page = projectRepository.findAllFromUser(currentUserLogin, pageable);
        List<ProjectDto> managedUserVMs = page.getContent().stream()
            .map(projectMapper::map)
            .collect(Collectors.toList());
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/ui/projects");
        return new ResponseEntity<>(managedUserVMs, headers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<ProjectDto>> getRecentProjects(@PathVariable(name = "user") String user, Pageable pageable) throws URISyntaxException {
        String currentUserLogin = SecurityUtils.getCurrentUserLogin();
        List response = Collections.emptyList();
        if(Objects.isNull(user)){
            log.info("Request current user {}",currentUserLogin);
            user = currentUserLogin;
        }
        /**if(!SecurityRequest.userRequestIsInRole(AuthoritiesConstants.ADMIN)){
         username = currentUserLogin;
         log.info("No es admin request to current user {}",username);
         }**/
        Page<WorkSession> workPage = workSessionRepository.findForUserPag(user,pageable);
        System.out.println(workPage.getContent());
        List collection = new ArrayList();
        for (WorkSession workSession : workPage.getContent()) {
            ProjectDto project = projectMapper.map(workSession.getProject());
            if(!collection.contains(project)){
                collection.add(project);
            }
        }

        // no trabaja, muchas sessiones para un mismo proyecto, estudiar el comportamiento
        // de set con un conjunto repetido de datos
        /**Set<ProjectDto> dtoList = workPage.getContent().stream()
            .map(WorkSession::getProject)
            .map(projectMapper::map)
            .collect(Collectors.toSet());**/


        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(workPage, "/api/ui/projects");
        return new ResponseEntity<>(collection, headers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<ProjectDto> getProjectInformation(@PathVariable(name = "projectUid") String projectUid, Pageable pageable) throws URISyntaxException {
        /**if(!SecurityRequest.userRequestIsInRole(AuthoritiesConstants.ADMIN,AuthoritiesConstants.)){
         username = currentUserLogin;
         log.info("No es admin request to current user {}",username);
         }**/
        Optional<Project> result = projectRepository.findOneByUuid(projectUid);
        if(!result.isPresent()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(null);
        }
        return ResponseEntity.ok(projectMapper.map(result.get()));
    }
}
