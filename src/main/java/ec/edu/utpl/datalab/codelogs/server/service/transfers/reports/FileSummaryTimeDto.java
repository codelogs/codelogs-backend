package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

/**
 * * Created by rfcardenas
 */
public class FileSummaryTimeDto {
    private String session;
    private String file_code;
    private String file_code_name;
    private long time;

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }

    public String getFile_code() {
        return file_code;
    }

    public void setFile_code(String file_code) {
        this.file_code = file_code;
    }

    public String getFile_code_name() {
        return file_code_name;
    }

    public void setFile_code_name(String file_code_name) {
        this.file_code_name = file_code_name;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}
