package ec.edu.utpl.datalab.codelogs.server.domain.settings;

import ec.edu.utpl.datalab.codelogs.server.domain.security.User;

import javax.persistence.*;

/**
 * * Created by rfcardenas
 */
@Entity
@Table(name = "goal_settings")
public class GoalSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    // Tiempo en segundos
    @Column(name="goalDay")
    private Long goalDay;
    // Tiempo en segundos
    @Column(name="goalWeek")
    private Long goalWeek;
    // Tiempo en segundos
    @Column(name="goalMonth")
    private Long goalMonth;
    @OneToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getGoalDay() {
        return goalDay;
    }

    public void setGoalDay(Long goalDay) {
        this.goalDay = goalDay;
    }

    public Long getGoalWeek() {
        return goalWeek;
    }

    public void setGoalWeek(Long goalWeek) {
        this.goalWeek = goalWeek;
    }

    public Long getGoalMonth() {
        return goalMonth;
    }

    public void setGoalMonth(Long goalMonth) {
        this.goalMonth = goalMonth;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "GoalSettings{" +
            "id=" + id +
            ", goalDay=" + goalDay +
            ", goalWeek=" + goalWeek +
            ", goalMonth=" + goalMonth +
            ", user=" + user +
            '}';
    }
}
