package ec.edu.utpl.datalab.codelogs.server.repository.metrics;

import ec.edu.utpl.datalab.codelogs.server.domain.metrics.CodeAnalysis;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface CodeAnalysisRepository extends JpaRepository<CodeAnalysis,Long> {
    @Query("select pa from CodeAnalysis pa left join pa.project pr  where pr.id = :id")
    Optional<CodeAnalysis> findAnalysisByProject(@Param("id") Long id);

    @Query("SELECT pa FROM CodeAnalysis pa LEFT JOIN pa.project pr  where pr.uuid = :project_uuid")
    Optional<CodeAnalysis> findAnalysisByProject(@Param("project_uuid") String project_uuid);
}
