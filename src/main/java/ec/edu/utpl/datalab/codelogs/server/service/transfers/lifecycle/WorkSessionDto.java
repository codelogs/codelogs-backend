package ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import org.springframework.hateoas.ResourceSupport;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public class WorkSessionDto extends ResourceSupport implements Serializable {
    private static final long serialVersionUID = 1L;
    private Long resource_id;
    private String uuid;
    private ZonedDateTime sessionStart;
    private ZonedDateTime sessionEnd;
    private Long accumTime;
    private String editor;
    private Long terminalUptime;
    private String terminalArchitecture;
    private String terminalOs;
    private String terminalIp;
    private String terminalMac;
    private String terminalName;
    private Integer terminalCpu;
    private Integer terminalTemperature;
    private Integer terminalCores;
    private Long terminalRam;
    private Long terminalRamAv;
    private GrantedContextDto project;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getSessionStart() {
        return sessionStart;
    }

    public void setSessionStart(ZonedDateTime sessionStart) {
        this.sessionStart = sessionStart;
    }

    public ZonedDateTime getSessionEnd() {
        return sessionEnd;
    }

    public void setSessionEnd(ZonedDateTime sessionEnd) {
        this.sessionEnd = sessionEnd;
    }

    public Long getAccumTime() {
        return accumTime;
    }

    public void setAccumTime(Long accumTime) {
        this.accumTime = accumTime;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Long getTerminalUptime() {
        return terminalUptime;
    }

    public void setTerminalUptime(Long terminalUptime) {
        this.terminalUptime = terminalUptime;
    }

    public String getTerminalArchitecture() {
        return terminalArchitecture;
    }

    public void setTerminalArchitecture(String terminalArchitecture) {
        this.terminalArchitecture = terminalArchitecture;
    }

    public String getTerminalOs() {
        return terminalOs;
    }

    public void setTerminalOs(String terminalOs) {
        this.terminalOs = terminalOs;
    }

    public String getTerminalIp() {
        return terminalIp;
    }

    public void setTerminalIp(String terminalIp) {
        this.terminalIp = terminalIp;
    }

    public String getTerminalMac() {
        return terminalMac;
    }

    public void setTerminalMac(String terminalMac) {
        this.terminalMac = terminalMac;
    }

    public String getTerminalName() {
        return terminalName;
    }

    public void setTerminalName(String terminalName) {
        this.terminalName = terminalName;
    }

    public Integer getTerminalCpu() {
        return terminalCpu;
    }

    public void setTerminalCpu(Integer terminalCpu) {
        this.terminalCpu = terminalCpu;
    }

    public Integer getTerminalTemperature() {
        return terminalTemperature;
    }

    public void setTerminalTemperature(Integer terminalTemperature) {
        this.terminalTemperature = terminalTemperature;
    }

    public Integer getTerminalCores() {
        return terminalCores;
    }

    public void setTerminalCores(Integer terminalCores) {
        this.terminalCores = terminalCores;
    }

    public Long getTerminalRam() {
        return terminalRam;
    }

    public void setTerminalRam(Long terminalRam) {
        this.terminalRam = terminalRam;
    }

    public Long getTerminalRamAv() {
        return terminalRamAv;
    }

    public void setTerminalRamAv(Long terminalRamAv) {
        this.terminalRamAv = terminalRamAv;
    }

    public GrantedContextDto getProject() {
        return project;
    }

    public void setProject(GrantedContextDto project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        WorkSessionDto heartBeatCode = (WorkSessionDto) o;
        if(heartBeatCode.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, heartBeatCode.resource_id);
    }
    @Override
    public String toString() {
        return "WorkSessionDto{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", sessionStart=" + sessionStart +
            ", sessionEnd=" + sessionEnd +
            ", accumTime=" + accumTime +
            ", editor='" + editor + '\'' +
            ", terminalUptime=" + terminalUptime +
            ", terminalArchitecture='" + terminalArchitecture + '\'' +
            ", terminalOs='" + terminalOs + '\'' +
            ", terminalIp='" + terminalIp + '\'' +
            ", terminalMac='" + terminalMac + '\'' +
            ", terminalName='" + terminalName + '\'' +
            ", terminalCpu=" + terminalCpu +
            ", terminalTemperature=" + terminalTemperature +
            ", terminalCores=" + terminalCores +
            ", terminalRam=" + terminalRam +
            ", terminalRamAv=" + terminalRamAv +
            ", project=" + project +
            '}';
    }
}
