package ec.edu.utpl.datalab.codelogs.server.repository.programmer_skills;

import ec.edu.utpl.datalab.codelogs.server.domain.programmer_skills.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
@Repository
public interface SkillRepository extends JpaRepository<Skill,Long> {
    @Query("SELECT  S FROM Skill S LEFT JOIN S.languaje L LEFT JOIN S.programmer P WHERE L.extension =:extension AND P.username =:username")
    Optional<Skill> findSkillProgrammerLnag(@Param("extension") String extension, @Param("username") String username);

    @Query("SELECT  S FROM Skill S LEFT JOIN S.programmer P WHERE P.username =:username")
    List<Skill> findAllSkillsProgrammer(@Param("username") String username);
}
