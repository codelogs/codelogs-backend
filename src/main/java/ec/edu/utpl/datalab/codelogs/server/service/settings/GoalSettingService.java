package ec.edu.utpl.datalab.codelogs.server.service.settings;

import ec.edu.utpl.datalab.codelogs.server.domain.security.User;
import ec.edu.utpl.datalab.codelogs.server.domain.settings.GoalSettings;
import ec.edu.utpl.datalab.codelogs.server.repository.security.UserRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.settings.GoalSettingsRepository;
import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * * Created by rfcardenas
 */
@Service
@Transactional
public class GoalSettingService {
    private final Logger log = LoggerFactory.getLogger(GoalSettingService.class);

    //Programar una hora al dia
    private static final Long GOAL_DAY = TimeUnit.HOURS.toSeconds(1);
    //Programar 5 horas a la semana
    private static final Long GOAL_WEEK = TimeUnit.HOURS.toSeconds(5);
    //Programar 20 horas a la semana
    private static final Long GOAL_MONTH = TimeUnit.HOURS.toSeconds(20);


    private UserRepository userRepository;
    private GoalSettingsRepository goalSettingsRepository;

    @Inject
    public GoalSettingService(UserRepository userRepository, GoalSettingsRepository goalSettingsRepository) {
        this.userRepository = userRepository;
        this.goalSettingsRepository = goalSettingsRepository;
    }

    public GoalSettings createDefaultGoals(){
        log.info("Creando Goals por defecto");
        String username = SecurityUtils.getCurrentUserLogin();
        Optional<User> userData = userRepository.findOneByLogin(username);
        Optional<GoalSettings> defaultGoals = goalSettingsRepository.findOneByUsername(username);
        if(!defaultGoals.isPresent()){
            GoalSettings goalSettings = new GoalSettings();
            goalSettings.setUser(userData.get());
            goalSettings.setGoalDay(GOAL_DAY);
            goalSettings.setGoalMonth(GOAL_MONTH);
            goalSettings.setGoalWeek(GOAL_WEEK);
            goalSettingsRepository.save(goalSettings);
            return goalSettings;
        }else{
            return defaultGoals.get();
        }
    }
    public GoalSettings updateGoals(Long month,Long week, Long day){
        String username = SecurityUtils.getCurrentUserLogin();
        Optional<GoalSettings> goalDefault = goalSettingsRepository.findOneByUsername(username);

        if(goalDefault.isPresent()){
            GoalSettings goalSettings = goalDefault.get();

            if((day<=TimeUnit.HOURS.convert(24,TimeUnit.SECONDS))&&(day>=TimeUnit.HOURS.convert(GOAL_DAY,TimeUnit.SECONDS))){
                log.debug("override goals day");
                goalSettings.setGoalWeek(day);
            }else{
                log.debug("ignorando day goal");
            }
            if((week<=TimeUnit.HOURS.convert(100,TimeUnit.SECONDS))&&(week>=TimeUnit.HOURS.convert(1,TimeUnit.SECONDS))){
                log.debug("override goals week");
                goalSettings.setGoalWeek(week);
            }else{
                log.debug("ignorando week goal");
            }

            if((month<=TimeUnit.HOURS.convert(320,TimeUnit.SECONDS))&& (month>TimeUnit.HOURS.convert(1,TimeUnit.SECONDS))){
                log.debug("override goals month");
                goalSettings.setGoalMonth(month);
            }else{
                log.debug("ignorando month goal");
            }
            goalSettingsRepository.save(goalSettings);
            return goalSettings;
        }else{
            return createDefaultGoals();
        }
    }
    public void reset(){
        log.debug("reset goals");
        String username = SecurityUtils.getCurrentUserLogin();
        Optional<GoalSettings> goalInDb  =  goalSettingsRepository.findOneByUsername(username);
        goalInDb.ifPresent(goalSettings -> {
            goalSettings.setGoalDay(GOAL_DAY);
            goalSettings.setGoalWeek(GOAL_WEEK);
            goalSettings.setGoalMonth(GOAL_MONTH);
        });
    }
    public GoalSettings getGoals(){
        log.debug("Recuperando goals");
        String username = SecurityUtils.getCurrentUserLogin();
        Optional<GoalSettings> goalInDb  =  goalSettingsRepository.findOneByUsername(username);
        if(goalInDb.isPresent()){
            return goalInDb.get();
        }else{
            return createDefaultGoals();
        }
    }
}
