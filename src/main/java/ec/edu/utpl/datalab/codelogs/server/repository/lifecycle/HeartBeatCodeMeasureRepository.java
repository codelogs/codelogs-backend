package ec.edu.utpl.datalab.codelogs.server.repository.lifecycle;


import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatMeasures;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Measure entity.
 */
@SuppressWarnings("unused")
public interface HeartBeatCodeMeasureRepository extends JpaRepository<HeartBeatMeasures,Long> {
    @Query("select hcm from HeartBeatMeasures hcm left join hcm.metric mt left join hcm.codeAnalysis ca where mt.id =:idmetric and ca.id=:idanalysis")
    Optional<HeartBeatMeasures> findMeasureByMetricAndAnalysis(@Param("idmetric") long idmetric, @Param("idanalysis") long idanalysis);
}
