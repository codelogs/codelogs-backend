package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.coding;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.FileCodeDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/coding/files",
    authorizations = {},
    tags = {"coding"},
    description = "Retorna los datos recolectados sobre el trabajo a nivel de archivos de código")
@RequestMapping("/api/v1/coding/files")
public interface WorkSourceCodeCoreApi {
    @ApiOperation(
        value = "API retorna el trabajo invertido en los diferenctes archivos de codigo" +
            "de un proyecto",
        nickname = "get_work_sourcecode",
        response = FileCodeDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "project/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<List<FileCodeDto>> heartBeats(
        @PathVariable("project") String project
    );
}
