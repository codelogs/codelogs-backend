package ec.edu.utpl.datalab.codelogs.server.service.transfers.metrics;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A Metric.
 */
public class MetricDto implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long resource_id;
    private String uuid;
    private String name;
    private String decription;
    private String provider;
    private ZonedDateTime createDate;
    private Boolean validated;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDecription() {
        return decription;
    }

    public void setDecription(String decription) {
        this.decription = decription;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public Boolean isValidated() {
        return validated;
    }

    public void setValidated(Boolean validated) {
        this.validated = validated;
    }

    public Boolean getValidated() {
        return validated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MetricDto metric = (MetricDto) o;
        if(metric.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, metric.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    @Override
    public String toString() {
        return "MetricDto{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", name='" + name + '\'' +
            ", decription='" + decription + '\'' +
            ", provider='" + provider + '\'' +
            ", createDate=" + createDate +
            ", validated=" + validated +
            '}';
    }
}
