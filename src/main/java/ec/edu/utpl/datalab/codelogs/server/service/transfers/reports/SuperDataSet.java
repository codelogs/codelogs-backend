package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * * Created by rfcardenas
 */
public class SuperDataSet extends ResourceSupport{
    private List<Dataset> datasets = new ArrayList<>();
    private Set<String> xData = new HashSet<>();

    public List<Dataset> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<Dataset> datasets) {
        this.datasets = datasets;
    }

    public Set<String> getxData() {
        return xData;
    }

    public void setxData(Set<String> xData) {
        this.xData = xData;
    }
}
