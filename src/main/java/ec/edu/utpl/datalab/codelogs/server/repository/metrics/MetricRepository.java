package ec.edu.utpl.datalab.codelogs.server.repository.metrics;

import ec.edu.utpl.datalab.codelogs.server.domain.metrics.Metric;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface MetricRepository extends JpaRepository<Metric,Long> {
    Optional<Metric> findOneByuuid(String uuid);
}
