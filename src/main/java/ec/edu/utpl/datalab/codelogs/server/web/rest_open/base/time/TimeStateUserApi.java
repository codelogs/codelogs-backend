package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.time;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimeStatusProgrammerDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/time/states",
    authorizations = {},
    tags = {"time"},
    description = "Retorna los estados del programador actualmente logeado")
@RequestMapping("/api/v1/time/states")
public interface TimeStateUserApi{

    @ApiOperation(
        value = "Retorna los estados del programador actualmente logeado" +
            "de un proyecto",
        nickname = "estados programador",
        response = TimeStatusProgrammerDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/global",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    TimeStatusProgrammerDto timeStateCurrentUserGlobal(
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );

    @ApiOperation(
        value = "Retorna los estados del programador  actualmente logeado en un proyecto",
        nickname = "estados programador",
        response = TimeStatusProgrammerDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/projects/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    TimeStatusProgrammerDto timeStateCurrentUserProject(
        @PathVariable("project") String project,
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );
}
