package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.metrics;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.metrics.CodeAnalysisDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URISyntaxException;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/analisis",
    authorizations = {},
    tags = {"metricas"},
    description = "Metricas")
@RequestMapping("/api/v1/analisis")
public interface ResultAnalysisApi {
    @ApiOperation(
        value = "retorna los resultados de analisis para un proyecto",
        nickname = "Metricas",
        response = ProjectDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "project/{project_uuid}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<CodeAnalysisDto> getAnalisis(@PathVariable("project_uuid") String project_uuid) throws URISyntaxException;
}
