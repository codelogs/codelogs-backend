package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.goals;

import ec.edu.utpl.datalab.codelogs.server.repository.reports.LoggTimeRepository;
import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.goals.GoalTransffer;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.settings.GoalSettingsTransfer;
import ec.edu.utpl.datalab.codelogs.server.web.rest.settings.GoalSettinsApi;
import org.apache.commons.lang.time.DateFormatUtils;
import org.joda.time.DateTime;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class GoalProgressApiImpl implements GoalProgressApi {
    private LoggTimeRepository loggTimeRepo;
    private GoalSettinsApi goalSettinsApi;

    @Inject
    public GoalProgressApiImpl(LoggTimeRepository loggTimeRepo, GoalSettinsApi goalSettinsApi) {
        this.loggTimeRepo = loggTimeRepo;
        this.goalSettinsApi = goalSettinsApi;
    }

    @Override
    public List<GoalTransffer> getWeekGoalProgress(@PathVariable("project_uuid") String project_uuid) throws URISyntaxException {
        GoalSettingsTransfer goals = goalSettinsApi.getGoals().getBody();

        System.out.println(goals);
        String username = SecurityUtils.getCurrentUserLogin();
        DateTime startDay = DateTime.now().withDayOfWeek(1);
        DateTime endDay = DateTime.now().withDayOfWeek(7);

        List<GoalTransffer> goalsWeek =  new ArrayList<>();

        for (int day = 1; day <= 7; day++) {
            GoalTransffer goalTransffer = new GoalTransffer();
            goalTransffer.setDate(DateTime.now().withDayOfWeek(day).toDate());
            goalsWeek.add(goalTransffer);
        }

        String startDateFix = DateFormatUtils.format(startDay.toDate(),"yyyy-MM-dd");
        String endDateFix = DateFormatUtils.format(endDay.toDate(),"yyyy-MM-dd");
        List<LoggTimeDto> loggtime = loggTimeRepo.customRangeLoggTime(username, startDateFix,endDateFix);

        for (GoalTransffer goalTransffer : goalsWeek) {
            for (LoggTimeDto loggTimeDto : loggtime) {
                String fixDate  = DateFormatUtils.format(goalTransffer.getDate(),"yyyy-MM-dd");
                System.out.println(loggtime);
                if(loggTimeDto.getDate().contains(fixDate)){
                    goalTransffer.setTotalSeconds((long) loggTimeDto.getTime_seconds());
                }
            }
            goalTransffer.setComment("week goals");
            goalTransffer.setGoalSeconds(goals.getGoalWeek());

        }
        return goalsWeek;
    }

}
