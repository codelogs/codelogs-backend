package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

import java.sql.Timestamp;

/**
 * * Created by rfcardenas
 */
public class TimeStatusProgrammerDto {
    private long totalTime; // total en segundos
    private long timeActive; // time active en segundos
    private Timestamp startSession; // date time start
    private Timestamp endSession; // date time end
    private String editor;
    private String project;

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }

    public long getTimeActive() {
        return timeActive;
    }

    public void setTimeActive(long timeActive) {
        this.timeActive = timeActive;
    }

    public Timestamp getStartSession() {
        return startSession;
    }

    public void setStartSession(Timestamp startSession) {
        this.startSession = startSession;
    }

    public Timestamp getEndSession() {
        return endSession;
    }

    public void setEndSession(Timestamp endSession) {
        this.endSession = endSession;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }
}
