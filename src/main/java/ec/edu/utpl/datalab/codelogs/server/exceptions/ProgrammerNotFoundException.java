package ec.edu.utpl.datalab.codelogs.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * * Created by rfcardenas
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND,reason="This programmer not found in server")
public class ProgrammerNotFoundException extends RuntimeException {
    public ProgrammerNotFoundException() {
        super();
    }

    public ProgrammerNotFoundException(String message) {
        super(message);
    }

    public ProgrammerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProgrammerNotFoundException(Throwable cause) {
        super(cause);
    }

    protected ProgrammerNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
