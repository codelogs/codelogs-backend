package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.coding;

import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.HeartBeatCompileRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle.HeartBeatCompilerMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.coding.BuildRunDto;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
@RestController
public class BuildsCoreApiImpl implements BuildsCoreApi {

    private HeartBeatCompileRepository compileRepository;
    private HeartBeatCompilerMapper heartBeatCompilerMapper;

    @Inject
    public BuildsCoreApiImpl(HeartBeatCompileRepository compileRepository, HeartBeatCompilerMapper heartBeatCompilerMapper) {
        this.compileRepository = compileRepository;
        this.heartBeatCompilerMapper = heartBeatCompilerMapper;
    }

    @Override
    public List<BuildRunDto> getBuildCollectionProject(@PathVariable("project_uuid") String project_uuid, Pageable pageable) {
        return compileRepository.findAllByProject(project_uuid,pageable)
            .getContent()
            .stream()
            .map(heartBeatCompilerMapper::mapTo)
            .collect(Collectors.toList());
    }

    @Override
    public List<BuildRunDto> getBuildCollectionSession(@PathVariable("session_uuid") String session_uuid, Pageable pageable) {
        return compileRepository.findAllByWorksession(session_uuid,pageable)
            .getContent()
            .stream()
            .map(heartBeatCompilerMapper::mapTo)
            .collect(Collectors.toList());
    }
}
