package ec.edu.utpl.datalab.codelogs.server.service.transfers.coding;

import com.fasterxml.jackson.annotation.JsonProperty;
import ec.edu.utpl.datalab.codelogs.server.domain.enumeration.TYPE_INSTRUCTION;
import org.springframework.hateoas.ResourceSupport;

import java.time.ZonedDateTime;

/**
 * * Created by rfcardenas
 */

public class BuildRunDto extends ResourceSupport {
    private static final long serialVersionUID = 1L;
    @JsonProperty("resource_id")
    private Long resource_id;
    @JsonProperty("uuid")
    private String uuid;
    //indicador del exito o fracaso
    @JsonProperty("status_success")
    private boolean success;
    // Mensaje principal capturado
    @JsonProperty("message_l1")
    private String messageL1 = "ejecución existosa";
    // Mensaje secundaior opcional
    @JsonProperty("message_l2")
    private String messageL2 = "";
    // Mensaje global
    @JsonProperty("message_l3")
    private String messageL3 = "";

    @JsonProperty("time")
    private long time;

    @JsonProperty("capture_date")
    private ZonedDateTime capturate_at;

    @JsonProperty("instruction")
    private TYPE_INSTRUCTION instruction;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessageL1() {
        return messageL1;
    }

    public void setMessageL1(String messageL1) {
        this.messageL1 = messageL1;
    }

    public String getMessageL2() {
        return messageL2;
    }

    public void setMessageL2(String messageL2) {
        this.messageL2 = messageL2;
    }

    public String getMessageL3() {
        return messageL3;
    }

    public void setMessageL3(String messageL3) {
        this.messageL3 = messageL3;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public ZonedDateTime getCapturate_at() {
        return capturate_at;
    }

    public void setCapturate_at(ZonedDateTime capturate_at) {
        this.capturate_at = capturate_at;
    }

    public TYPE_INSTRUCTION getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION instruction) {
        this.instruction = instruction;
    }

    @Override
    public String toString() {
        return "BuildStatusDto{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", success=" + success +
            ", messageL1='" + messageL1 + '\'' +
            ", messageL2='" + messageL2 + '\'' +
            ", messageL3='" + messageL3 + '\'' +
            ", time=" + time +
            ", capturate_at=" + capturate_at +
            ", instruction=" + instruction +
            '}';
    }
}
