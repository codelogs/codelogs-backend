package ec.edu.utpl.datalab.codelogs.server.repository.search;

import ec.edu.utpl.datalab.codelogs.server.domain.security.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * Spring Data ElasticSearch repository for the User entity.
 */
public interface UserSearchRepository extends ElasticsearchRepository<User, Long> {
}
