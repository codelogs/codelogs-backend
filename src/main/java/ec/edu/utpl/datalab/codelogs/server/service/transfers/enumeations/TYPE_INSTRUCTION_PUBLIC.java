package ec.edu.utpl.datalab.codelogs.server.service.transfers.enumeations;

/**
 * * Created by rfcardenas
 */
public enum TYPE_INSTRUCTION_PUBLIC {
    HEART_BEAT_COMPILE_SUCCESS ("public-beat:compile-success"),
    HEART_BEAT_COMPILE_FAILED  ("public-beat:compile-failed"),
    HEART_BEAT_RUN_SUCCESS     ("public-beat:run-success"),
    HEART_BEAT_RUN_FAILED      ("public-beat:run-failed"),
    HEART_BEAT_CODE_ACTIVITY   ("public-beat:code-activity"),
    HEART_BEAT_CODE_METRIC     ("public-beat:code-metric"),
    HEART_BEAT_CODE_ANALYSIS   ("public-beat:code-analysis");

    private String status;

    TYPE_INSTRUCTION_PUBLIC(String status) {
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public String toString() {
        return this.status;
    }
}
