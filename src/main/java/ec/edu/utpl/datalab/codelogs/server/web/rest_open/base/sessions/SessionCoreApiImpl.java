package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.sessions;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.WorkSessionRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle.WorksessionMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.WorkSessionDto;
import ec.edu.utpl.datalab.codelogs.server.web.rest.util.PaginationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils.getCurrentUserLogin;

/**
 * * Created by rfcardenas
 */
@RestController
public class SessionCoreApiImpl implements SessionCoreApi{
    private final Logger log = LoggerFactory.getLogger(SessionCoreApiImpl.class);
    private WorkSessionRepository workSessionRepository;
    private WorksessionMapper worksessionMapper;
    private EntityLinks entityLinks;
    @Inject
    public SessionCoreApiImpl(WorkSessionRepository workSessionRepository,
                              WorksessionMapper worksessionMapper,
                              EntityLinks entityLinks) {
        this.workSessionRepository = workSessionRepository;
        this.worksessionMapper = worksessionMapper;
        this.entityLinks = entityLinks;
    }

    @Override
    public ResponseEntity<List<WorkSessionDto>> getAllSessions(@PathVariable(name = "user") String user, Pageable pageable) throws URISyntaxException {
        Page<WorkSession> page = workSessionRepository.findForUserPag(user, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("api/ui/%s/sessions}", user));
        List<WorkSessionDto> result = page.getContent().stream()
            .map(worksessionMapper::map)
            .collect(Collectors.toList());

        return new ResponseEntity<>(result, headers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<List<WorkSessionDto>> getSessionsRecent(@PathVariable("user") String user, Pageable pageable) throws URISyntaxException {
        if (!isValidPageCfg(pageable)) {
            pageable = new PageRequest(0, 15, Sort.Direction.DESC);
        }
        log.info("Recuperando data de target {} sol por user {}", user, getCurrentUserLogin());
        Page<WorkSession> page = workSessionRepository.findForUserPag(user, pageable);
        List<WorkSessionDto> result = page.getContent().stream()
            .map(entrada->{
                WorkSessionDto data = worksessionMapper.map(entrada);
                Link links  = entityLinks.linkToSingleResource(Project.class,entrada.getProject().getUuid());
                data.add(links);
                return data;
            })
            .collect(Collectors.toList());

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("api/ui/%s/sessions}", user));

        return new ResponseEntity<>(result, headers, HttpStatus.OK);
    }



    @Override
    public ResponseEntity<List<WorkSessionDto>> getSessionsProject(@PathVariable("project_uuid") String project_uuid, Pageable pageable) throws URISyntaxException {
        Page<WorkSession> page = workSessionRepository.findLastSessionsProject(project_uuid,pageable);

        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, String.format("/%s/project/sessions",project_uuid));

        List<WorkSessionDto> workSessionDtos = page.getContent().stream()
            .map(entry->{
                WorkSessionDto data = worksessionMapper.map(entry);
                Link link  = entityLinks.linkToSingleResource(Project.class,entry.getProject().getUuid());
                data.add(link);
                return data;
            })
            .collect(Collectors.toList());

        return new ResponseEntity<>(workSessionDtos, headers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<WorkSessionDto> getSessionsByUuid(@PathVariable("session_uuid") String session_uuid) throws URISyntaxException {
        Optional<WorkSession> result = workSessionRepository.findOneByUuid(session_uuid);
        if(!result.isPresent()){
            return ResponseEntity.status(HttpStatus.NO_CONTENT)
                .body(null);
        }
        WorkSessionDto data = worksessionMapper.map(result.get());
        Link link  = entityLinks.linkToSingleResource(Project.class,result.get().getProject().getUuid());
        data.add(link);
        return ResponseEntity.ok(data);
    }

    private boolean isValidPageCfg(Pageable pageable) {
        if (pageable.getPageSize() > 100) {
            return false;
        }
        return true;
    }
}
