package ec.edu.utpl.datalab.codelogs.server.web.rest_open.util;

import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * * Created by rfcardenas
 */
@Service
public class DateIntervalUtil {
    /**
     * [0] -> start date
     * [1] -> end Date
     * @return
     */
    public String[] getCurrentWeekInterval(){
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int i = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
        c.add(Calendar.DATE, -i);
        Date start = c.getTime();
        c.add(Calendar.DATE, 6);
        Date end = c.getTime();

        SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd");

        String startDate = simpleDateFormat.format(start);
        String endDate = simpleDateFormat.format(end);

        return new String[]{startDate,endDate};
    }

    public String[] getLastWeekInterval(){
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int i = c.get(Calendar.DAY_OF_WEEK) - c.getFirstDayOfWeek();
        c.add(Calendar.DATE, -i - 7);
        Date start = c.getTime();
        c.add(Calendar.DATE, 6);
        Date end = c.getTime();

        SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd");

        String startDate = simpleDateFormat.format(start);
        String endDate = simpleDateFormat.format(end);

        return new String[]{startDate,endDate};
    }

}
