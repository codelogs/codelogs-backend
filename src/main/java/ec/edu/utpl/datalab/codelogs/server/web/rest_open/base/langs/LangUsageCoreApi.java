package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.langs;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LanguajeUsageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "api/v1/lang_usage",
    authorizations = {},
    tags = {"lang usage"},
    description = "API uso de lenguajes")
@RequestMapping("api/v1/lang_usage")
public interface LangUsageCoreApi {

    @ApiOperation(
        value = "API retorna el uso de lenguajes de programacion en una rango de fechas predeterminado",
        nickname = "get_lang_usage",
        response = LanguajeUsageDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}/global",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    List<LanguajeUsageDto> getLangUsage(
        @PathVariable(value = "user") String user,
        @RequestParam(value = "start") String startDate,
        @RequestParam(value = "end") String endDate
    );

    /***
     * API Rertona los lenguajes utilizados en un proyecto de un usuario especifico
     * @param user
     * @param project
     * @param startDate
     * @param endDate
     * @return
     */
    @ApiOperation(
        value = "API retorna el uso de lenguajes de programacion en una rango de fechas predeterminado de un proyecto en" +
            "especifico",
        nickname = "get_lang_usage",
        response = LanguajeUsageDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}/projects/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    List<LanguajeUsageDto> getLangUsage(
        @PathVariable(value = "user") String user,
        @PathVariable(value = "project") String project,
        @RequestParam(value = "start") String startDate,
        @RequestParam(value = "end") String endDate
    );
}
