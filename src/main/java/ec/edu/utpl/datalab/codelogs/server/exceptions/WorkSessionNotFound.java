package ec.edu.utpl.datalab.codelogs.server.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * * Created by rfcardenas
 */
@ResponseStatus(value= HttpStatus.NOT_FOUND,reason="WorkSessionNotFound in this server")
public class WorkSessionNotFound  extends RuntimeException{
    public WorkSessionNotFound(String worksession_uuid) {
        super("No se encontro la session con clave  " + worksession_uuid);
    }

    public WorkSessionNotFound(String worksession_uuid, String user) {
        super(String.format("Session de trabajo para el usuario %s no fue encontrado con el UUID local  %s",user,worksession_uuid));
    }
}
