package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.FileCodeDto;

/**
 * * Created by rfcardenas
 */
public class TimeFileDto {
    private FileCodeDto fileCodeDto;
    private long timeFixed = 0;
    private long totalTime = 0;

    public FileCodeDto getFileCodeDto() {
        return fileCodeDto;
    }

    public void setFileCodeDto(FileCodeDto fileCodeDto) {
        this.fileCodeDto = fileCodeDto;
    }

    public long getTimeFixed() {
        return timeFixed;
    }

    public void setTimeFixed(long timeFixed) {
        this.timeFixed = timeFixed;
    }

    public long getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(long totalTime) {
        this.totalTime = totalTime;
    }
}
