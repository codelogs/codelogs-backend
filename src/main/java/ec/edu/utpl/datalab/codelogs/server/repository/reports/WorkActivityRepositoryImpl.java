package ec.edu.utpl.datalab.codelogs.server.repository.reports;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.WorkActivityPulseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * * Created by rfcardenas
 */
@Repository
public class WorkActivityRepositoryImpl implements WorkActivityRepository {
    private static final Logger log = LoggerFactory.getLogger(WorkActivityRepositoryImpl.class);
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Inject
    public WorkActivityRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    private static final String WORK_ACTIVITY_PULSE = "SELECT LEVENSHTEIN , LINES_ADD  , LINES_DEL  ,CAPTURE_DATE, WS.UUID \n" +
        "FROM heart_beats_code              PC\n" +
        "LEFT JOIN WORK_SESSION WS ON PC.WORK_SESSION_ID = WS.ID \n" +
        "LEFT JOIN PROJECT              PJ  ON PJ.ID = WS.PROJECT_ID \n" +
        "LEFT JOIN PROGRAMMER    PR ON PR.ID  = PJ.PROGRAMMER_ID \n" +
        "WHERE PR.USERNAME  = :user\n" +
        "ORDER BY CAPTURE_DATE DESC\n" +
        "LIMIT 200\n";

    @Override
    public List<WorkActivityPulseDto> workActivity(String user) {
        log.info("Query app lang ussage" + user);
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("user", user);
        RowMapper<WorkActivityPulseDto> rowMapper = (rs, rowNum) -> {
            WorkActivityPulseDto activityPulseDto = new WorkActivityPulseDto();
            activityPulseDto.setLevenshtein(rs.getInt(1));
            activityPulseDto.setLinesadd(rs.getInt(2));
            activityPulseDto.setLinesdel(rs.getInt(3));
            activityPulseDto.setCapturedate(rs.getString(4));
            activityPulseDto.setSession_uid(rs.getString(5));
            return activityPulseDto;
        };
        return namedParameterJdbcTemplate
            .query(WORK_ACTIVITY_PULSE,queryParams,rowMapper);
    }


    private static final String WORK_ACTIVITY_PULSE_PUID = "SELECT LEVENSHTEIN , LINES_ADD  , LINES_DEL  ,CAPTURE_DATE, WS.UUID \n" +
        "FROM heart_beats_code              PC\n" +
        "LEFT JOIN WORK_SESSION WS ON PC.WORK_SESSION_ID = WS.ID \n" +
        "LEFT JOIN PROJECT              PJ  ON PJ.ID = WS.PROJECT_ID \n" +
        "LEFT JOIN PROGRAMMER    PR ON PR.ID  = PJ.PROGRAMMER_ID \n" +
        "WHERE PR.USERNAME  = :user\n" +
        "AND PJ.UUID = :project_uuid\n"+
        "ORDER BY CAPTURE_DATE DESC\n" +
        "LIMIT 200\n";

    @Override
    public List<WorkActivityPulseDto> workActivity(String user, String projectUuid) {
        log.info("Query app lang ussage" + user);
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("user", user);
        queryParams.put("project_uuid", projectUuid);
        RowMapper<WorkActivityPulseDto> rowMapper = (rs, rowNum) -> {
            WorkActivityPulseDto activityPulseDto = new WorkActivityPulseDto();
            activityPulseDto.setLevenshtein(rs.getInt(1));
            activityPulseDto.setLinesadd(rs.getInt(2));
            activityPulseDto.setLinesdel(rs.getInt(3));
            activityPulseDto.setCapturedate(rs.getString(4));
            activityPulseDto.setSession_uid(rs.getString(5));
            return activityPulseDto;
        };
        return namedParameterJdbcTemplate
            .query(WORK_ACTIVITY_PULSE,queryParams,rowMapper);
    }
}

