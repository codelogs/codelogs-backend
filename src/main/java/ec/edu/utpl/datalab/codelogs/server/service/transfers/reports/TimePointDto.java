package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;

import java.util.HashMap;

/**
 * * Created by rfcardenas
 */
public class TimePointDto {
    ProjectDto project;
    HashMap<Long,TimeFileDto> usage;
    long totalTimeLog;
    long timeFixedLog;
    double averageTotalTimeLog;
    double averageFixedTimeLog;

    public ProjectDto getProject() {
        return project;
    }

    public void setProject(ProjectDto project) {
        this.project = project;
    }

    public HashMap<Long, TimeFileDto> getUsage() {
        return usage;
    }

    public void setUsage(HashMap<Long, TimeFileDto> usage) {
        this.usage = usage;
    }

    public long getTotalTimeLog() {
        return totalTimeLog;
    }

    public void setTotalTimeLog(long totalTimeLog) {
        this.totalTimeLog = totalTimeLog;
    }

    public long getTimeFixedLog() {
        return timeFixedLog;
    }

    public void setTimeFixedLog(long timeFixedLog) {
        this.timeFixedLog = timeFixedLog;
    }

    public double getAverageTotalTimeLog() {
        return averageTotalTimeLog;
    }

    public void setAverageTotalTimeLog(double averageTotalTimeLog) {
        this.averageTotalTimeLog = averageTotalTimeLog;
    }

    public double getAverageFixedTimeLog() {
        return averageFixedTimeLog;
    }

    public void setAverageFixedTimeLog(double averageFixedTimeLog) {
        this.averageFixedTimeLog = averageFixedTimeLog;
    }
}
