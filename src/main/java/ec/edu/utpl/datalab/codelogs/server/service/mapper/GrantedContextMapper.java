package ec.edu.utpl.datalab.codelogs.server.service.mapper;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCode;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCompile;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatRun;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.FileCode;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.FileCodeGrantedContextDto;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring")
public interface GrantedContextMapper {
    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextDto map(Project source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextDto map(WorkSession source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextDto map(HeartBeatCode source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextDto map(HeartBeatRun source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextDto map(HeartBeatCompile source);

    @Mappings(value = {
        @Mapping(target = "uuid",source ="uuid" ),
        @Mapping(target = "pathFile",source ="pathFile" ),
    })
    FileCodeGrantedContextDto map(FileCode source);

    /***
     * Maps para los paquetes transferidos entre el monitor y el server
     */

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextPack mapToPack(Project source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextPack mapToPack(WorkSession source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextPack mapToPack(HeartBeatCode source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextPack mapToPack(HeartBeatRun source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextPack mapToPack(HeartBeatCompile source);

    @Mappings(@Mapping(target = "uuid",source ="uuid" ))
    GrantedContextPack mapToPack(FileCode source);
}
