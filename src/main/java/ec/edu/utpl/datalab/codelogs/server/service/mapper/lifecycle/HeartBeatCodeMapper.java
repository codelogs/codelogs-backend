package ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCode;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextRct;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.enumeations.TypeInstructionMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.HeartBeatCodeDto;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatCodePack;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    uses = {GrantedContextMapper.class, GrantedContextRct.class, TypeInstructionMapper.class},
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface HeartBeatCodeMapper {
    @Mappings({
        @Mapping(source = "resource_id",target = "id" )
    })
    HeartBeatCode map(HeartBeatCodeDto source);

    @Mappings({
        @Mapping(source = "resource_id",target = "id",ignore = true),
        @Mapping(source = "fileCode",target = "fileCode", ignore = true),
        @Mapping(source = "workSession",target = "workSession" ,ignore = true)
    })
    HeartBeatCode map(HeartBeatCodePack source);

    @Mappings({
        @Mapping(source = "id",target = "resource_id" )
    })
    HeartBeatCodeDto map(HeartBeatCode source);


}
