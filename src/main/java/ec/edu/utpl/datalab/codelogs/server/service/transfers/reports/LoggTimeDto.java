package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

import org.springframework.hateoas.ResourceSupport;

/**
 * DTO Log time
 * * Created by rfcardenas
 */
public class LoggTimeDto extends ResourceSupport {
    private String programmer;
    private String date;
    private int time_seconds;

    public String getProgrammer() {
        return programmer;
    }

    public void setProgrammer(String programmer) {
        this.programmer = programmer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getTime_seconds() {
        return time_seconds;
    }

    public void setTime_seconds(int time_seconds) {
        this.time_seconds = time_seconds;
    }
}
