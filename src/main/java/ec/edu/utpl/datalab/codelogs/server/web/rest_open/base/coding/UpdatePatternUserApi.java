package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.coding;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.SuperDataSet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/coding/update_pattern",
    authorizations = {},
    tags = {"coding","update pattern"},
    description = "Retorna los estados de un programador")
@RequestMapping("/api/v1/coding/update_pattern")
public interface UpdatePatternUserApi {
    @ApiOperation(
        value = "Retorna el patron de programación global del usuario actualmente logeado",
        nickname = "patron de programación",
        response = SuperDataSet.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/global",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    SuperDataSet dataSetUpdatePattern();

    @ApiOperation(
        value = "Retorna el patron de programación en un proyecto del usuario actualmente logeado",
        nickname = "patron de programación",
        response = SuperDataSet.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/project/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    SuperDataSet dataSetUpdatePatternProject(@PathVariable("project") String project);
}
