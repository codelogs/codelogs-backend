package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

/**
 * * Created by rfcardenas
 */
public class LanguajeUsageDto {
    private String languaje;
    private int usage;

    public String getLanguaje() {
        return languaje;
    }

    public void setLanguaje(String languaje) {
        this.languaje = languaje;
    }

    public int getUsage() {
        return usage;
    }

    public void setUsage(int usage) {
        this.usage = usage;
    }
}
