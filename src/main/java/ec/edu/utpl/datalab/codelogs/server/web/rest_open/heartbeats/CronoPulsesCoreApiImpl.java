package ec.edu.utpl.datalab.codelogs.server.web.rest_open.heartbeats;

import ec.edu.utpl.datalab.codelogs.server.repository.reports.PulsesCronoRepository;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.PulseCodeCronoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class CronoPulsesCoreApiImpl implements CronoPulsesCoreApi {
    private final Logger log = LoggerFactory.getLogger(CronoPulsesCoreApiImpl.class);


    private PulsesCronoRepository pulsesCronoRepository;

    @Inject
    public CronoPulsesCoreApiImpl(PulsesCronoRepository pulsesCronoRepository) {
        this.pulsesCronoRepository = pulsesCronoRepository;
    }

    @Override
    public List<PulseCodeCronoDto> heartBeatsUser(@PathVariable("user") String user, @PathVariable("project_uuid") String project_uuid) {
        List<PulseCodeCronoDto> result = pulsesCronoRepository.getSummaryLastWorkFile(project_uuid,user);
        return result;
    }
}
