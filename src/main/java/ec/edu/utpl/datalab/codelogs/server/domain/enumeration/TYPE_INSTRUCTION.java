package ec.edu.utpl.datalab.codelogs.server.domain.enumeration;

/**
 *
 * Representan los tipos de instrucciones procesadas por el monitor
 * * Created by rfcardenas
 */
public enum  TYPE_INSTRUCTION {
    HEART_BEAT_COMPILE_SUCCESS (1,"beat:compile-success"),
    HEART_BEAT_COMPILE_FAILED  (2,"beat:compile-failed"),
    HEART_BEAT_RUN_SUCCESS     (3,"beat:run-success"),
    HEART_BEAT_RUN_FAILED      (4,"beat:run-failed"),
    HEART_BEAT_CODE_ACTIVITY   (5,"beat:code-activity"),
    HEART_BEAT_CODE_METRIC     (6,"beat:code-metric"),
    HEART_BEAT_CODE_ANALYSIS   (7,"beat:code-analysis");

    private int id;
    private String status;

    TYPE_INSTRUCTION(int id, String status) {
        this.id = id;
        this.status = status;
    }

    public String getStatus() {
        return this.status;
    }

    public String toString() {
        return this.status;
    }
}
