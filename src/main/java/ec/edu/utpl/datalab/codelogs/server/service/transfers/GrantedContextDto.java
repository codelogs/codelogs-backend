package ec.edu.utpl.datalab.codelogs.server.service.transfers;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * * Created by rfcardenas
 */
public class GrantedContextDto {
    @NotEmpty(message = "Campo critico UUID para context granted")
    private String uuid;

    public GrantedContextDto(String uuid) {
        this.uuid = uuid;
    }

    public GrantedContextDto() {
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    @Override
    public String toString() {
        return "GrantedContext{" +
            "uuid='" + uuid + '\'' +
            '}';
    }
}
