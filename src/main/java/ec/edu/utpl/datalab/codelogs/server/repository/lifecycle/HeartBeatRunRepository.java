package ec.edu.utpl.datalab.codelogs.server.repository.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatRun;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * * Created by rfcardenas
 */
public interface HeartBeatRunRepository extends JpaRepository<HeartBeatRun,Long> {
    @Query("SELECT hc FROM HeartBeatRun hc WHERE hc.workSession.uuid=:worksession_uuid ")
    Page<HeartBeatRun> findAllByWorksession(@Param("worksession_uuid") String worksession_uuid, Pageable pageable);

    @Query("SELECT hc FROM HeartBeatRun hc WHERE hc.workSession.project.uuid=:project_uuid ")
    Page<HeartBeatRun> findAllByProject(@Param("project_uuid") String project_uuid, Pageable pageable);
}

