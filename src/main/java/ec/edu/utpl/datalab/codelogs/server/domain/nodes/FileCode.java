package ec.edu.utpl.datalab.codelogs.server.domain.nodes;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * A FileCode.
 */
@Entity
@Table(name = "file_code")
public class FileCode implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid",unique = true)
    private String uuid;

    @Column(name = "name")
    private String name;

    @Column(name = "path_file")
    private String pathFile;

    @Column(name = "extension")
    private String extension;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @Column(name = "update_date")
    private ZonedDateTime updateDate;

    @Column(name = "code_lines")
    private Integer codeLines;

    @Column(name = "available")
    private Boolean available;

    @Column(name = "update_counter")
    private Integer updateCounter;

    @Column(name = "fixed_timeactivity")
    private Long fixedTimeactivity;

    @ManyToOne
    private Project project;

    @ManyToOne
    private Languaje languaje;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(ZonedDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getCodeLines() {
        return codeLines;
    }

    public void setCodeLines(Integer codeLines) {
        this.codeLines = codeLines;
    }

    public Boolean isAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public Integer getUpdateCounter() {
        return updateCounter;
    }

    public void setUpdateCounter(Integer updateCounter) {
        this.updateCounter = updateCounter;
    }

    public Long getFixedTimeactivity() {
        return fixedTimeactivity;
    }

    public void setFixedTimeactivity(Long fixedTimeactivity) {
        this.fixedTimeactivity = fixedTimeactivity;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Languaje getLanguaje() {
        return languaje;
    }

    public void setLanguaje(Languaje languaje) {
        this.languaje = languaje;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FileCode fileCode = (FileCode) o;
        if(fileCode.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, fileCode.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "FileCode{" +
            "id=" + id +
            ", uuid='" + uuid + "'" +
            ", name='" + name + "'" +
            ", pathFile='" + pathFile + "'" +
            ", extension='" + extension + "'" +
            ", createDate='" + createDate + "'" +
            ", updateDate='" + updateDate + "'" +
            ", codeLines='" + codeLines + "'" +
            ", available='" + available + "'" +
            ", updateCounter='" + updateCounter + "'" +
            ", fixedTimeactivity='" + fixedTimeactivity + "'" +
            '}';
    }
}
