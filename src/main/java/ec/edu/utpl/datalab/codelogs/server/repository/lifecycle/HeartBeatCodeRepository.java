package ec.edu.utpl.datalab.codelogs.server.repository.lifecycle;


import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCode;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Spring Data JPA repository for the PulseCode entity.
 */
@SuppressWarnings("unused")
public interface HeartBeatCodeRepository extends JpaRepository<HeartBeatCode,Long> {
    @Query("select hc from HeartBeatCode hc where hc.workSession.id =:worksession_id")
    List<HeartBeatCode> findAllByWorksession(@Param("worksession_id") long worksession_id);

    @Query("select hc from HeartBeatCode hc where hc.workSession.uuid =:worksession_uuid")
    Page<HeartBeatCode> findAllByWorksession(@Param("worksession_uuid") String worksession_uuid, Pageable pageable);
}
