package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;

/**
 * * Created by rfcardenas
 */
public class WorkActivityPulseDto {
    private int levenshtein;
    private int linesadd;
    private int linesdel;
    private String capturedate;
    private String session_uid;

    public int getLevenshtein() {
        return levenshtein;
    }

    public void setLevenshtein(int levenshtein) {
        this.levenshtein = levenshtein;
    }

    public int getLinesadd() {
        return linesadd;
    }

    public void setLinesadd(int linesadd) {
        this.linesadd = linesadd;
    }

    public int getLinesdel() {
        return linesdel;
    }

    public void setLinesdel(int linesdel) {
        this.linesdel = linesdel;
    }

    public String getCapturedate() {
        return capturedate;
    }

    public void setCapturedate(String capturedate) {
        this.capturedate = capturedate;
    }

    public String getSession_uid() {
        return session_uid;
    }

    public void setSession_uid(String session_uid) {
        this.session_uid = session_uid;
    }
}
