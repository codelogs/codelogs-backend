package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.skills;

import ec.edu.utpl.datalab.codelogs.server.domain.programmer_skills.LevelSkill;
import ec.edu.utpl.datalab.codelogs.server.repository.programmer_skills.LevelSkillRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.skills.LevelSkillMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.skill.LevelSkillDto;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
@RestController
public class LevelsSkillsCoreApiImpl implements LevelsSkillsCoreApi {
    private LevelSkillRepository levelSkillRepository;
    private LevelSkillMapper levelSkillMapper;

    public LevelsSkillsCoreApiImpl(LevelSkillRepository levelSkillRepository, LevelSkillMapper levelSkillMapper) {
        this.levelSkillRepository = levelSkillRepository;
        this.levelSkillMapper = levelSkillMapper;
    }

    @Override
    public List<LevelSkillDto> getAllLevels() {
        List<LevelSkill> levels =levelSkillRepository.findAll();

        return levels.stream()
            .map(levelSkillMapper::map)
            .collect(Collectors.toList());
    }
}
