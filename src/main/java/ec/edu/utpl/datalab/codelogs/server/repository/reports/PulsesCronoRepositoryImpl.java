package ec.edu.utpl.datalab.codelogs.server.repository.reports;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.PulseCodeCronoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * retorna la cronologia de los pusos en este caso solo de los pustos de tipo codigo añadido
 * * Created by rfcardenas
 */
@Repository
public class PulsesCronoRepositoryImpl implements PulsesCronoRepository {
    private static final Logger log = LoggerFactory.getLogger(PulsesCronoRepositoryImpl.class);
    /** OJO LAS CONSULTAS NO ESTAN DESARROLLADAS APROPIADAMENTE POR EL HECHO DE QUE SE ESTA TRABAJANDO CON UNA BD
     * EMBEDDED LA CUAL NO CUENTAN CON CARACTERISTICAS AVANZADAS DE CONULSTA (ACTUALIZAR EN PRODUCCION)
    **/
    private final String PULSE_CODE_CROQUERY ="SELECT p.uuid as project, pc.uuid as pulseid , pc.capture_date as capturedate ,   fc.name as filename , pc.path_file,  pc.lines_add as counter \n" +
        "                          FROM heart_beats_code pc  \n" +
        "                          LEFT JOIN work_session as ws  ON pc.work_session_id=ws.id \n" +
        "                          LEFT JOIN file_code as fc ON fc.id=pc.file_code_id \n" +
        "                          LEFT JOIN project as p ON p.id =ws.project_id\n" +
        "                          LEFT JOIN programmer pr ON p.PROGRAMMER_ID =pr.id\n" +
        "                WHERE p.uuid = :uuid_project AND pr.username=:username AND pc.lines_add > 0\n" +
        "                ORDER BY pc.capture_date  DESC\n" +
        "                LIMIT 40 ";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Inject
    public PulsesCronoRepositoryImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    @Override
    public List<PulseCodeCronoDto> getSummaryLastWorkFile(String uuid_project, String username) {
        log.info("Query app " + uuid_project);
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("uuid_project", uuid_project);
        queryParams.put("username", username);
        return namedParameterJdbcTemplate
            .query(PULSE_CODE_CROQUERY,queryParams,new BeanPropertyRowMapper<>(PulseCodeCronoDto.class));
    }
}


