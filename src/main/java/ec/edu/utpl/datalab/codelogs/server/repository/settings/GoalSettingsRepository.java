package ec.edu.utpl.datalab.codelogs.server.repository.settings;

import ec.edu.utpl.datalab.codelogs.server.domain.settings.GoalSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface GoalSettingsRepository extends JpaRepository<GoalSettings,Long>{
    @Query("SELECT GL FROM GoalSettings GL WHERE  GL.user.login = :userlogin")
    Optional<GoalSettings> findOneByUsername(@Param("userlogin")String userlogin);
}
