package ec.edu.utpl.datalab.codelogs.server.repository.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface WorkSessionRepository extends JpaRepository<WorkSession,Long> {
    @Query("SELECT w FROM  WorkSession w JOIN  w.project.programmer p  where p.user=:user AND  w.uuid=:session_granted")
    Optional<WorkSession> loadWorkSession(@Param("user") String user, @Param("session_granted") String session_granted);

    @Query("SELECT w FROM  WorkSession w JOIN  w.project.programmer p  where p.username=:user AND  w.uuid=:session_granted")
    Optional<WorkSession> findOneByUuidAndUser(@Param("session_granted") String uuid, @Param("user") String user);

    Optional<WorkSession> findOneByUuid(String uuid);

    @Query("SELECT w FROM  WorkSession w JOIN  w.project.programmer p  where p.user.login=:username ORDER BY w.sessionStart desc")
    Page<WorkSession> findForUserPag(@Param("username") String username, Pageable pageable);


    @Query("SELECT w FROM  WorkSession w JOIN  w.project.programmer p  where p.user.login=:username  " +
        "AND w.project.uuid = :project_uuid  ORDER BY w.sessionStart desc")
    Page<WorkSession> findSession(@Param("username") String username, @Param("project_uuid") String project_uuid, Pageable pageable);

    @Query("SELECT w FROM  WorkSession w JOIN  w.project  WHERE w.project.uuid = :project_uuid ORDER BY w.sessionStart desc")
    Page<WorkSession> findLastSessionsProject(@Param("project_uuid") String project_uuid, Pageable pageable);
}
