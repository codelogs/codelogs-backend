package ec.edu.utpl.datalab.codelogs.server.web.rest_open.heartbeats;

import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.PulseCodeCronoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class CronoUserApiImpl implements CronoUserApi {
    private final Logger log = LoggerFactory.getLogger(CronoUserApiImpl.class);

    private CronoPulsesCoreApi cronoPulsesCoreApi;

    @Inject
    public CronoUserApiImpl(CronoPulsesCoreApi cronoPulsesCoreApi) {
        this.cronoPulsesCoreApi = cronoPulsesCoreApi;
    }

    @Override
    public List<PulseCodeCronoDto> heartBeats(@PathVariable("project_uuid") String project_uuid) {
        String user = SecurityUtils.getCurrentUserLogin();
        List response = cronoPulsesCoreApi.heartBeatsUser(user,project_uuid);
        return response;
    }
}
