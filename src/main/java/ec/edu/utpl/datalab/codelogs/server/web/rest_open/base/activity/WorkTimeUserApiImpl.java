package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.activity;

import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeProjectDto;
import ec.edu.utpl.datalab.codelogs.server.web.rest_open.util.DateIntervalUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class WorkTimeUserApiImpl implements WorkTimeUserApi {
    private WorkTimeCoreApi workTimeCoreApi;
    private DateIntervalUtil dateIntervalUtil;

    @Inject
    public WorkTimeUserApiImpl(WorkTimeCoreApi workTimeCoreApi, DateIntervalUtil dateIntervalUtil) {
        this.workTimeCoreApi = workTimeCoreApi;
        this.dateIntervalUtil = dateIntervalUtil;
    }

    @Override
    public ResponseEntity<List<LoggTimeDto>> getSimpleLogTime(@RequestParam("start") String start, @RequestParam("end") String end) {
        String user = SecurityUtils.getCurrentUserLogin();
        String[] currentInterval  = dateIntervalUtil.getCurrentWeekInterval();
        return workTimeCoreApi.getSimpleLogTime(user,currentInterval[0],currentInterval[1]);
    }

    @Override
    public ResponseEntity<LoggTimeProjectDto> getWorkTimeProject(@PathVariable("project") String project, @RequestParam("start") String start, @RequestParam("end") String end) {
        String user = SecurityUtils.getCurrentUserLogin();
        return workTimeCoreApi.getWorkTimeProject(user,project,start,end);
    }

    @Override
    public ResponseEntity<List<LoggTimeProjectDto>> getGlobalWorkTimeAllProjects(@RequestParam("start") String start, @RequestParam("end") String end) {
        String user = SecurityUtils.getCurrentUserLogin();
        return workTimeCoreApi.getGlobalWorkTimeAllProjects(start,end,user);
    }
}
