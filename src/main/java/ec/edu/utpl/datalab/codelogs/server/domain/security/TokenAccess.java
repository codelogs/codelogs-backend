package ec.edu.utpl.datalab.codelogs.server.domain.security;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * * Created by rfcardenas
 */
@Entity
@Table(name = "token_access")
public class TokenAccess {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "apikey",unique = true)
    private String apikey;

    @NotNull
    @Column(name = "header",length = 100)
    private String header;

    @NotNull
    @Column(name = "enable")
    private Boolean enable;

    @Column(name = "createat")
    private ZonedDateTime createAt;

    @Column(name = "generation")
    private int generation;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Programmer programmer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public ZonedDateTime getCreateAt() {
        return createAt;
    }

    public void setCreateAt(ZonedDateTime createAt) {
        this.createAt = createAt;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    public Programmer getProgrammer() {
        return programmer;
    }

    public void setProgrammer(Programmer programmer) {
        this.programmer = programmer;
    }

    @Override
    public int hashCode() {
        return apikey.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TokenAccess access = (TokenAccess) o;

        if (!apikey.equals(access.apikey)) {
            return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return "TokenAccess{" +
            "id=" + id +
            ", apikey='" + apikey + '\'' +
            ", header='" + header + '\'' +
            ", enable=" + enable +
            ", createAt=" + createAt +
            ", generation=" + generation +
            ", programmer=" + programmer +
            '}';
    }
}
