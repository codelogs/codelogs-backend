package ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextRct;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.ProjectPack;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    uses = {GrantedContextMapper.class,GrantedContextRct.class},
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProjectMapper {
    ProjectDto map(Project source);
    Project map(ProjectDto source);
    @Mappings({
        @Mapping(source = "resource_id",target = "id",ignore = true),
    })
    Project map(ProjectPack source);

}
