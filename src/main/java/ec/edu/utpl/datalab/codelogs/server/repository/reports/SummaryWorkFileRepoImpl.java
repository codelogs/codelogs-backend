package ec.edu.utpl.datalab.codelogs.server.repository.reports;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.FileSummaryTimeDto;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementacion del repositorio SummaryWorkFileRepo
 * * Created by rfcardenas
 */
@Repository
public class SummaryWorkFileRepoImpl implements SummaryWorkFileRepo {
    /**
     * Consulta temporal
     */
    private static final String SUMMARY_WORK_FILE_QUERY = "SELECT DISTINCT WS.UUID SESSION ,FC.UUID as FILE_CODE,FC.NAME FILE_CODE_NAME , COALESCE(\n" +
        "                                        (SELECT SUM (PCB.TIME_FIXED )  \n" +
        "                                                   FROM PULSE_CODE PCB  \n" +
        "                                                   LEFT JOIN WORK_SESSION WSB ON WSB.ID = PCB.WORK_SESSION_ID \n" +
        "                                                   WHERE PCB.PATH_FILE =FC.PATH_FILE AND WSB.ID = WS.ID  ),0) AS TIME \n" +
                                        "        FROM FILE_CODE  FC  \n" +
                                        "        RIGHT JOIN PULSE_CODE  PC       ON FC.ID = PC.FILE_CODE_ID   \n" +
                                        "        RIGHT JOIN WORK_SESSION  WS ON WS.ID=PC.WORK_SESSION_ID \n" +
                                        "        WHERE WS.UUID=:session_uuid \n" +
                                        "        GROUP BY PC.ID,FC.NAME,FC.UUID,WS.ID ,WS.UUID";

    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Inject
    public SummaryWorkFileRepoImpl(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }

    /**
     * Retorna una lista de los archivos editados durante una session
     * @param session_uuid
     * @return
     */

    @Override
    public List<FileSummaryTimeDto> getSummaryLastWorkFile(String session_uuid) {
        Map<String, String> queryParams = new HashMap<>();
        queryParams.put("session_uuid", session_uuid);
        return namedParameterJdbcTemplate
                .query(SUMMARY_WORK_FILE_QUERY,queryParams,new BeanPropertyRowMapper<>(FileSummaryTimeDto.class));
    }
}
