package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.langs;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LanguajeUsageDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * * Created by rfcardenas
 */

@Api(value = "api/v1/lang_usage",
    authorizations = {},
    tags = {"usuario actualmente logeado","lang usage"},
    description = "Retorna el uso de lenguajes del usuario actualmente logeado")
@RequestMapping("api/v1/lang_usage")
public interface LangUsageUserApi {
    /**
     * API RETORONA EL USU DE LENGUAJES DEL USUARIO ACTUALMENTE LOGEADO
     * @param startDate
     * @param endDate
     * @return
     */

    @RequestMapping(value = "/global",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(
        value = "API retorna el uso de lenguajes de programacion en rango especifico, del usuario actualmente logeado",
        nickname = "get_lang_usage",
        response = LanguajeUsageDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @Timed
    List<LanguajeUsageDto> getLangUsageCurrentUser(
        @RequestParam(value = "start") String startDate,
        @RequestParam(value = "end") String endDate
    );

    /**
     * API retorna el uso del lenguaje del usuario actualmente logeado, de un proyecto en
     * especifico.
     * @param project
     * @param startDate
     * @param endDate
     * @return
     */
    @RequestMapping(value = "/project/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(
        value = "API retorna el uso de lenguajes de programacion en rango especifico, de un proyecto x" +
            "del usuario actualmente logeado",
        nickname = "get_lang_usage",
        response = LanguajeUsageDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @Timed
    List<LanguajeUsageDto> getLangUsageCurrentUser(
        @PathVariable(value = "project") String project,
        @RequestParam(value = "start") String startDate,
        @RequestParam(value = "end") String endDate
    );
}
