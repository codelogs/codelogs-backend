package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.metrics;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.metrics.MetricDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URISyntaxException;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/metrics",
    authorizations = {},
    tags = {"metricas"},
    description = "Metricas")
@RequestMapping("/api/v1/metricas")
public interface MetricsApi {
    @ApiOperation(
        value = "retorna la lista de metricas registadas del servidor",
        nickname = "Metricas",
        response = ProjectDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<List<MetricDto>> getAllMetrics(Pageable pageable) throws URISyntaxException;
}
