package ec.edu.utpl.datalab.codelogs.server.repository.security;


import ec.edu.utpl.datalab.codelogs.server.domain.security.Programmer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Programmer entity.
 */
@SuppressWarnings("unused")
public interface ProgrammerRepository extends JpaRepository<Programmer,Long> {

    Optional<Programmer> findOneByUsername(String user);
}
