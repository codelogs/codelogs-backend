package ec.edu.utpl.datalab.codelogs.server.domain.programmer_skills;

import javax.persistence.*;

/**
 * * Created by rfcardenas
 */
@Entity
@Table(name = "level_skill")
public class LevelSkill {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "level")
    private int level;
    @Column(name = "min_points")
    private long min_points;
    @Column(name = "max_points")
    private long max_points;
    @Column(name = "name")
    private String name;
    @Column(name = "label")
    private String label;
    @Column(name = "acronimo")
    private String acronimo;
    @Column(name = "description")
    private String description;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getMin_points() {
        return min_points;
    }

    public void setMin_points(long min_points) {
        this.min_points = min_points;
    }

    public long getMax_points() {
        return max_points;
    }

    public void setMax_points(long max_points) {
        this.max_points = max_points;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getAcronimo() {
        return acronimo;
    }

    public void setAcronimo(String acronimo) {
        this.acronimo = acronimo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
