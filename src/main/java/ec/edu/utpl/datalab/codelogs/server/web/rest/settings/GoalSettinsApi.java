package ec.edu.utpl.datalab.codelogs.server.web.rest.settings;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.settings.GoalSettingsTransfer;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

/**
 * * Created by rfcardenas
 */
@Api(value = "api/v1/settings/",
    authorizations = {},
    tags = {"work_time","time"},
    description = "Goal ajustes")
@RequestMapping("api/v1/settings/goals")
public interface GoalSettinsApi {
    @ApiOperation(
        value = "Crea metas por defecto",
        nickname = "create_goal",
        responseContainer = "Array",
        httpMethod = "POST",
        protocols = "HTTP"
    )
    @PostMapping("/goals")
    @Timed
    ResponseEntity<?> createDefault() throws URISyntaxException;


    @ApiOperation(
        value = "Actualiza metas para el usuario actualmente logeado",
        nickname = "update_goal",
        responseContainer = "Array",
        httpMethod = "PUT",
        protocols = "HTTP"
    )
    @PutMapping("/goals")
    @Timed
    ResponseEntity<?> updateGoal(@RequestBody GoalSettingsTransfer dto) throws URISyntaxException;


    @ApiOperation(
        value = "Reinicia las metas al valor por defecto",
        nickname = "reset_goal",
        responseContainer = "Array",
        httpMethod = "PUT",
        protocols = "HTTP"
    )
    @PutMapping("/goals/reset")
    @Timed
    ResponseEntity<?> resetGoals() throws URISyntaxException;

    @ApiOperation(
        value = "Recupera las metas de un usuario",
        nickname = "reset_goal",
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping("/goals")
    @Timed
    ResponseEntity<GoalSettingsTransfer> getGoals() throws URISyntaxException;
}
