package ec.edu.utpl.datalab.codelogs.server.web.rest_spyder;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.domain.security.Programmer;
import ec.edu.utpl.datalab.codelogs.server.exceptions.ProgrammerNotFoundException;
import ec.edu.utpl.datalab.codelogs.server.exceptions.UserNotFoundException;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.ProjectRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.security.ProgrammerRepository;
import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes.ProjectMapper;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.ProjectPack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.UUID;

/**
 * * Created by rfcardenas
 */
@RestController
public class ProjectApiImpl implements ProjectApi {
    private final Logger log = LoggerFactory.getLogger(ProjectApiImpl.class);

    private ProjectRepository projectRepository;
    private ProgrammerRepository programmerRepository;
    private ProjectMapper projectMapper;
    private GrantedContextMapper contextMapper;
    @Inject
    public ProjectApiImpl(ProjectRepository projectRepository,
                          ProgrammerRepository programmerRepository,
                          ProjectMapper projectMapper,
                          GrantedContextMapper contextMapper) {
        this.projectRepository = projectRepository;
        this.programmerRepository = programmerRepository;
        this.projectMapper = projectMapper;
        this.contextMapper = contextMapper;
    }

    @Override
    public ResponseEntity<GrantedContextPack> createProjectNode(@RequestBody ProjectPack dto) throws URISyntaxException {
        String currentUser = SecurityUtils.getCurrentUserLogin();
        log.info("Current user " + currentUser);
        if(currentUser.isEmpty()){
            log.error("User no valido");
            new UserNotFoundException("Usuario no valido " + currentUser);
        }
        log.info("Creando desde dto");
        Optional<Project> projectdb = projectRepository.findByPathUser(dto.getPath(), SecurityUtils.getCurrentUserLogin());
        if(projectdb.isPresent()){
            log.info("Proyeto encontrado en db {} con ref ",dto.getPath());
            return ResponseEntity.ok(contextMapper.mapToPack(projectdb.get()));
        }
        Optional<Programmer> programmer = programmerRepository.findOneByUsername(currentUser);
        if(!programmer.isPresent()){
            log.error("El usuario {} intento acceder sin contar datos de uso para esta API",currentUser);
            throw new ProgrammerNotFoundException(String.format("El usuario %s intento acceder sin contar datos de uso para esta API",currentUser));
        }
        log.info("Registrando nuevo proyecto");
        Project project = projectMapper.map(dto);
        project.setUuid(UUID.randomUUID().toString());
        project.setProgrammer(programmer.get());
        log.info("Proyecto registrado correctamento");
        projectRepository.save(project);
        return ResponseEntity.ok(contextMapper.mapToPack(project));
    }

    public Optional<Project> loadProject(String uuid) {
        return projectRepository.findOneByUuid(uuid);
    }
}
