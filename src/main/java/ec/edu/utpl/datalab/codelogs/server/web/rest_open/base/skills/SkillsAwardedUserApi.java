package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.skills;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.skill.SkillDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@RequestMapping("/api/v1/skills")
@Api(value = "/api/v1/skills",
    authorizations = {},
    tags = {"skills"},
    description = "Retorna un punto en la serie de tiempo, de la actividad del programdor")
public interface SkillsAwardedUserApi {
    @ApiOperation(
        value = "API retorna todas habilidades adquiridas del usuario actualmente logeado en los lenguajes de programación",
        response = SkillDto.class,
        responseContainer = "Array",
        httpMethod = "GET",
        protocols = "HTTP"
    )
        @GetMapping(value = "/all")
        List<SkillDto> getSkills();
}
