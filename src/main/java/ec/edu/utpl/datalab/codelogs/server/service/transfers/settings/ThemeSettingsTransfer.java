package ec.edu.utpl.datalab.codelogs.server.service.transfers.settings;

import org.springframework.hateoas.ResourceSupport;

/**
 * * Created by rfcardenas
 */
public class ThemeSettingsTransfer extends ResourceSupport{
    private Long resource_id;
    private String name;
    private String css;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCss() {
        return css;
    }

    public void setCss(String css) {
        this.css = css;
    }
}
