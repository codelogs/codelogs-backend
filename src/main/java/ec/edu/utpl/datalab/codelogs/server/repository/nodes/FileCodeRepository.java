package ec.edu.utpl.datalab.codelogs.server.repository.nodes;


import ec.edu.utpl.datalab.codelogs.server.domain.nodes.FileCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.Optional;

/**
 * Spring Data JPA repository for the FileCode entity.
 */
@SuppressWarnings("unused")
public interface FileCodeRepository extends JpaRepository<FileCode,Long> {
    @Query("select f from FileCode f left join f.project p  where p.uuid = :uuid and f.pathFile=:pathFile")
    Optional<FileCode> findByPathUser(@Param("uuid") String uuid, @Param("pathFile") String pathFile);

    /**
     * Encontra un archivsso por un identificar unico
     * @param uuid
     * @return
     */
    Optional<FileCode> findByUuid(String uuid);

    @Query("select f from FileCode f left join f.project.programmer p  where p.username = :user and f.pathFile=:pathfile")
    Optional<FileCode> findByUserPath(@Param("user") String user, @Param("pathfile") String pathfile);

    /**
     * Retorna todos los archivos de un proyecto
     * @param project
     * @return
     */
    @Query("select f from FileCode f where f.project.uuid = :project")
    Collection<FileCode> collectionFilesByProject(@Param("project") String project);
}
