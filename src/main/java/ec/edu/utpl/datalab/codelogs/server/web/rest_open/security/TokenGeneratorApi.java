package ec.edu.utpl.datalab.codelogs.server.web.rest_open.security;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.security.AuthoritiesConstants;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.security.TokenAccessDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * * Created by rfcardenas
 */

@RequestMapping("/api/v1/spyder/access")
@Api(value = "api/v1/spyder/access",
    authorizations = {},
    tags = {"token access"},
    description = "Genera un token de acceso para {Spyder monitor}")
public interface TokenGeneratorApi {
    @ApiOperation(
        value = "API genera un token para el monitor, este token identifica a quien pertece el monitor" +
            "el servidor utiliza los datos embebidos de este token para almacenar la información recolectada" ,
        nickname = "get_lang_usage",
        response = TokenAccessDto.class,
        httpMethod = "POST",
        protocols = "HTTP",
        code = 200
    )
    @PostMapping(value = "/jwt_generic",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    @Secured({AuthoritiesConstants.USER, AuthoritiesConstants.ADMIN})
    ResponseEntity<TokenAccessDto> generate();
}
