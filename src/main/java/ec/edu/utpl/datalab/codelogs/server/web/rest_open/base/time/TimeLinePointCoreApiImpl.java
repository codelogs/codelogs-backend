package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.time;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCode;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.FileCode;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.exceptions.ProjectNotFoundException;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.HeartBeatCodeRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.ProjectRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.reports.WorkSessionRangeRepo;
import ec.edu.utpl.datalab.codelogs.server.repository.security.UserRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes.FileCodeMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes.ProjectMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.WorkSessionLessZTM;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimeFileDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimePointDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.sql.Timestamp;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
@RestController
public class TimeLinePointCoreApiImpl implements TimeLinePointCoreApi {
    private ProjectMapper projectMapper;
    private FileCodeMapper fileCodeMapper;
    private UserRepository userRepository;
    private HeartBeatCodeRepository pulseCodeRepository;
    private ProjectRepository projectRepository;
    private WorkSessionRangeRepo workSessionRangeRepo;

    @Inject
    public TimeLinePointCoreApiImpl(ProjectMapper projectMapper,
                                    FileCodeMapper fileCodeMapper,
                                    UserRepository userRepository,
                                    HeartBeatCodeRepository pulseCodeRepository,
                                    ProjectRepository projectRepository,
                                    WorkSessionRangeRepo workSessionRangeRepo) {
        this.projectMapper = projectMapper;
        this.fileCodeMapper = fileCodeMapper;
        this.userRepository = userRepository;
        this.pulseCodeRepository = pulseCodeRepository;
        this.projectRepository = projectRepository;
        this.workSessionRangeRepo = workSessionRangeRepo;
    }

    public TimePointDto getTimePoint(@PathVariable("project") String project, @RequestParam("start") String start, @RequestParam("end") String end) {
        Optional<Project> result = projectRepository.findOneByUuid(project);
        if (!result.isPresent()) {
            throw new ProjectNotFoundException();
        }
        System.out.println(workSessionRangeRepo);
        List<WorkSessionLessZTM> sessions = workSessionRangeRepo.workSession(project, start, end);
        TimePointDto timePointDto = new TimePointDto();
        System.out.println(sessions);
        if(sessions.size()>0){
            Timestamp initWork = sessions.get(0).getSessionStart();
            Timestamp lastWork = sessions.get(sessions.size()-1).getSessionEnd();

            System.out.println(initWork.toLocalDateTime());
            System.out.println(lastWork.toLocalDateTime());

            long totaFixedTime = sessions.stream().mapToLong(WorkSessionLessZTM::getAccumTime).sum();
            long totaTime = ChronoUnit.SECONDS.between(initWork.toLocalDateTime(),lastWork.toLocalDateTime());
            double avFixedtime = sessions.stream().mapToLong(WorkSessionLessZTM::getAccumTime).average()
                .getAsDouble();
            double avTotaltime = sessions.stream().mapToDouble(value -> ChronoUnit.SECONDS.
                between(value.getSessionStart().toLocalDateTime(),value.getSessionEnd().toLocalDateTime()))
                .average().getAsDouble();


            timePointDto.setProject(projectMapper.map(result.get()));

            timePointDto.setTotalTimeLog(totaTime);
            timePointDto.setTimeFixedLog(totaFixedTime);
            timePointDto.setAverageTotalTimeLog(avTotaltime);
            timePointDto.setAverageFixedTimeLog(avFixedtime);
            HashMap<Long,TimeFileDto> map = new HashMap<>();
            for (WorkSessionLessZTM session : sessions) {
                List<HeartBeatCode> resultPulses = pulseCodeRepository.findAllByWorksession(session.getId());
                makeTimeFile(resultPulses,map);
            }

            timePointDto.setUsage(map);
        }
        return timePointDto;
    }
    private HashMap<Long,TimeFileDto> makeTimeFile(List<HeartBeatCode> pageResult, HashMap<Long,TimeFileDto> map){

        for (HeartBeatCode pulseCode : pageResult) {
            FileCode fileCode = pulseCode.getFileCode();
            if(map.containsKey(fileCode.getId())){
                TimeFileDto timeFile = map.get(fileCode.getId());
                long value = timeFile.getTimeFixed();
                value+= pulseCode.getTimeFixed();
                timeFile.setTimeFixed(value);
            }else{
                TimeFileDto timeFileDto = new TimeFileDto();
                timeFileDto.setFileCodeDto(fileCodeMapper.map(fileCode));
                map.put(fileCode.getId(),timeFileDto);
            }
        }
        return map;
    }
}
