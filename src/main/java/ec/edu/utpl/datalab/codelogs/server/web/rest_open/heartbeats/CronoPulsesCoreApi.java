package ec.edu.utpl.datalab.codelogs.server.web.rest_open.heartbeats;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.coding.BuildRunDto;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.PulseCodeCronoDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "api/v1/crono",
    authorizations = {},
    tags = {"coding","run pattern"},
    description = "Api cronologia de codificación")
@RequestMapping("api/v1/crono")
public interface CronoPulsesCoreApi {

    @ApiOperation(
        value = "Retorna la cantidad de cambios en el código de un proyecto de usuario",
        nickname = "patron de programación",
        response = BuildRunDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping(value = "/users/{user}/project/{project_uuid}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    List<PulseCodeCronoDto> heartBeatsUser(
        @PathVariable("user") String user,
        @PathVariable("project_uuid") String project_uuid
    );
}
