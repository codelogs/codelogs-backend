package ec.edu.utpl.datalab.codelogs.server.web.rest_spyder;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatCode;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.FileCode;
import ec.edu.utpl.datalab.codelogs.server.domain.programmer_skills.LevelSkill;
import ec.edu.utpl.datalab.codelogs.server.domain.programmer_skills.Skill;
import ec.edu.utpl.datalab.codelogs.server.domain.security.Programmer;
import ec.edu.utpl.datalab.codelogs.server.exceptions.FileCodeNotfoundException;
import ec.edu.utpl.datalab.codelogs.server.exceptions.ProgrammerNotFoundException;
import ec.edu.utpl.datalab.codelogs.server.exceptions.WorkSessionNotFound;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.HeartBeatCodeRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.WorkSessionRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.FileCodeRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.programmer_skills.LevelSkillRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.programmer_skills.SkillRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.security.ProgrammerRepository;
import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle.HeartBeatCodeMapper;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.FileCodeGrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatCodePack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * * Created by rfcardenas
 */
@RestController
public class HeartBeatCodeApiImpl implements HeartBeatCodeApi {
    private final Logger log = LoggerFactory.getLogger(HeartBeatCodeApiImpl.class);

    private HeartBeatCodeRepository pulseCodeRepository;
    private WorkSessionRepository workSessionRepository;
    private FileCodeRepository fileCodeRepository;
    private SkillRepository skillRepository;
    private LevelSkillRepository levelSkillRepository;
    private ProgrammerRepository programmerRepository;
    private HeartBeatCodeMapper heartBeatCodeMapper;
    private GrantedContextMapper contextMapper;

    @Inject
    public HeartBeatCodeApiImpl(HeartBeatCodeRepository pulseCodeRepository,
                                WorkSessionRepository workSessionRepository,
                                FileCodeRepository fileCodeRepository,
                                SkillRepository skillRepository,
                                LevelSkillRepository levelSkillRepository,
                                ProgrammerRepository programmerRepository,
                                HeartBeatCodeMapper heartBeatCodeMapper,
                                GrantedContextMapper contextMapper) {
        this.pulseCodeRepository = pulseCodeRepository;
        this.workSessionRepository = workSessionRepository;
        this.fileCodeRepository = fileCodeRepository;
        this.skillRepository = skillRepository;
        this.levelSkillRepository = levelSkillRepository;
        this.programmerRepository = programmerRepository;
        this.heartBeatCodeMapper = heartBeatCodeMapper;
        this.contextMapper = contextMapper;
    }

    @Override
    public ResponseEntity<GrantedContextPack> unitRuns(@RequestBody HeartBeatCodePack data) throws URISyntaxException {
        log.info("Procesando {}",data);
        String user = SecurityUtils.getCurrentUserLogin();
        Optional<WorkSession> resultsession = workSessionRepository.findOneByUuid(data.getWorkSession().getUuid());
        Optional<Programmer> programmer = programmerRepository.findOneByUsername(user);
        if(!programmer.isPresent()){
            throw new ProgrammerNotFoundException();
        }
        if(!resultsession.isPresent()){
            log.error(data.toString());
            log.error("Session de trabajo invalida..{}",data);
            throw new WorkSessionNotFound(data.getUuid());
        }
        System.out.println(data.getFileCode());
        Optional<FileCode> fileCode = loadFileColde(data.getFileCode());
        if(!fileCode.isPresent()){
            log.error("No exista referencia de codigo para este pulso {}",data);
            throw new FileCodeNotfoundException(data.getUuid());
        }
        log.info("Session de trabajo valida.. registrando pulso");
        HeartBeatCode heartBeatCode = heartBeatCodeMapper.map(data);
        heartBeatCode.setFileCode(fileCode.get());
        heartBeatCode.setUuid(UUID.randomUUID().toString());
        heartBeatCode.setWorkSession(resultsession.get());

        updateTimeSession(resultsession,heartBeatCode);
        updateTimeFile(fileCode,heartBeatCode);
        //   createOrUpdateSkill(fileCode.get(),programmer);
        updateSkill(fileCode.get(),programmer.get(),resultsession.get());
        pulseCodeRepository.save(heartBeatCode);

        return ResponseEntity.ok(contextMapper.mapToPack(heartBeatCode));
    }
    private void updateTimeSession(Optional<WorkSession> workSession, HeartBeatCode heartBeatCode){
        if(workSession.isPresent()){
            WorkSession ws = workSession.get();
            long accum = workSession.get().getAccumTime() == null ? 0 : ws.getAccumTime();
            accum += heartBeatCode.getTimeFixed();

            ws.setAccumTime(accum);
            ws.setSessionEnd(ZonedDateTime.now());
            workSessionRepository.save(ws);
        }
    }
    private void updateTimeFile(Optional<FileCode> fileCode, HeartBeatCode heartBeatCode){
        if(fileCode.isPresent()){
            FileCode fl = fileCode.get();
            long accum = fl.getFixedTimeactivity() == null ? 0 : fl.getFixedTimeactivity();
            int updatecounter = fl.getUpdateCounter() == null?0:fl.getUpdateCounter();
            accum += heartBeatCode.getTimeFixed();
            fl.setFixedTimeactivity(accum);
            fl.setUpdateCounter(++updatecounter);
            fileCodeRepository.save(fl);
        }
    }
    private Optional<FileCode> loadFileColde(@Validated FileCodeGrantedContextPack fileCodeDataRel){
        String user = SecurityUtils.getCurrentUserLogin();
        Optional<FileCode> result = fileCodeRepository.findByUuid(fileCodeDataRel.getUuid());
        if(result.isPresent()){
            return result;
        }
        result = fileCodeRepository.findByUserPath(user,fileCodeDataRel.getPathFile());
        if(result.isPresent()){
            return result;
        }
        return Optional.empty();
    }
    private void updateSkill(FileCode fileCode, Programmer programmer , WorkSession workSession){
        List<LevelSkill> levelSkills = levelSkillRepository.findAll();
        Skill skill = null;
        Optional<Skill> resultDbSkill = skillRepository.findSkillProgrammerLnag(fileCode.getExtension(),programmer.getUsername());
        if(!resultDbSkill.isPresent()){
            skill = new Skill();
            skill.setLanguaje(fileCode.getLanguaje());
            skill.setProgrammer(programmer);
            skill.setTotal_usage(0);

        }else{
            skill = resultDbSkill.get();
            long skillAccum = skill.getTotal_usage();
            skill.setLastUpdate(ZonedDateTime.now());
            ZonedDateTime z1 = workSession.getSessionStart();
            ZonedDateTime z2 = workSession.getSessionEnd();
            skillAccum += ChronoUnit.SECONDS.between(z1,z2);
            skill.setTotal_usage(skillAccum);
            skill.setLastUpdate(ZonedDateTime.now());
            System.out.println("-> " +ChronoUnit.SECONDS.between(z1,z2));
        }

        for (LevelSkill levelSkill : levelSkills) {
            long currentValue = skill.getTotal_usage();
            long maxLevel = levelSkill.getMax_points();
            long minLevel = levelSkill.getMin_points();

            if(currentValue>=minLevel && currentValue<=maxLevel){
                skill.setLevelSkill(levelSkill);
                break;
            }
        }

        log.info("Update skill ");
        skillRepository.save(skill);
    }



    /** private void createOrUpdateSkill(FileCode fileCode,Programmer programmer ){
     Optional<Skill> resultDbSkill = levelSkillRepository.findSkillProgrammerLnag(fileCode.getExtension(),programmer.getUsername());
     Skill skill;
     if(!resultDbSkill.isPresent()){
     List<LevelSkill> skills = skillRepository.findAll();
     Optional<LevelSkill> levelSkill = skills.stream().filter(ls -> ls.getLevel() == 1).findFirst();
     if(!levelSkill.isPresent()){
     throw new SkillNotFoundException("No se puede continuar sin un nivel DB");
     }
     skill = new Skill();
     skill.setLevelSkill(levelSkill.get());
     skill.setLanguaje(fileCode.getLanguaje());
     skill.setProgrammer(programmer);

     }else{
     skill = resultDbSkill.get();
     skill.setLastUpdate(ZonedDateTime.now());
     }
     levelSkillRepository.save(skill);
     }**/
}

