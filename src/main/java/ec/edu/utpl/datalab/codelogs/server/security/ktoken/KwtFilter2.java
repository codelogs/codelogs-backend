package ec.edu.utpl.datalab.codelogs.server.security.ktoken;

import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * * Created by rfcardenas
 */
public class KwtFilter2 extends GenericFilterBean {
    TokenProviderKw providerKw = new TokenProviderKw();
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        String stringToken = req.getHeader("Authorization");
        try {
            if(stringToken!=null){
                if(stringToken.contains("KWT-Bearer ")){
                    String token = resolveToken(req);
                    if(providerKw.validateToken(token)){
                        Authentication authentication =new TokenProviderKw().getAuthentication(token);
                        SecurityContextHolder.getContext().setAuthentication(authentication);
                    }
                }
                chain.doFilter(request, response);
            }
        }catch (ExpiredJwtException e){
            e.printStackTrace();
        }

    }
    private String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader(TokenAuthorization.TOKEN_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("KWT-Bearer ")) {
            String jwt = bearerToken.substring(11, bearerToken.length());
            return jwt;
        }
        return null;
    }
}
