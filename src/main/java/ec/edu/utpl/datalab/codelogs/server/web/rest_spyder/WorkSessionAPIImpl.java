package ec.edu.utpl.datalab.codelogs.server.web.rest_spyder;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.exceptions.ProjectNotFoundException;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.WorkSessionRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.ProjectRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle.WorksessionMapper;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.WorkSessionPack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.Optional;
import java.util.UUID;

/**
 * * Created by rfcardenas
 */
@RestController
public class WorkSessionAPIImpl implements WorkSessionAPI {
    private final Logger log = LoggerFactory.getLogger(WorkSessionAPIImpl.class);

    private WorkSessionRepository workSessionRepository;
    private ProjectRepository projectRepository;
    private WorksessionMapper worksessionMapper;
    private GrantedContextMapper contextMapper;
    @Inject
    public WorkSessionAPIImpl(WorkSessionRepository workSessionRepository, ProjectRepository projectRepository, WorksessionMapper worksessionMapper, GrantedContextMapper contextMapper) {
        this.workSessionRepository = workSessionRepository;
        this.projectRepository = projectRepository;
        this.worksessionMapper = worksessionMapper;
        this.contextMapper = contextMapper;
    }

    @Override
    public ResponseEntity<GrantedContextPack> createWorkSession(@RequestBody WorkSessionPack dto) throws URISyntaxException, InterruptedException {
        log.info("Registrando session");
        Optional<Project> resulProject = projectRepository.findOneByUuid(dto.getProject().getUuid());
        if(!resulProject.isPresent()){
            log.error("Proyecto no registrado, @See ref project uuid{}",dto);
            throw  new ProjectNotFoundException("Proyecto no fue encontrado ");
        }
        System.out.println(resulProject.get());
        WorkSession workSession = worksessionMapper.map(dto);
        workSession.setUuid(UUID.randomUUID().toString());
        workSession.setProject(resulProject.get());
        workSessionRepository.save(workSession);
        return ResponseEntity.ok(contextMapper.mapToPack(workSession));
    }
}
