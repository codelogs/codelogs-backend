package ec.edu.utpl.datalab.codelogs.server.repository.programmer_skills;

import ec.edu.utpl.datalab.codelogs.server.domain.programmer_skills.LevelSkill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * * Created by rfcardenas
 */
@Repository
public interface LevelSkillRepository extends JpaRepository<LevelSkill,Long> {
}
