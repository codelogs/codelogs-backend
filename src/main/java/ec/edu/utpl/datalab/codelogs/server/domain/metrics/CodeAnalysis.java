package ec.edu.utpl.datalab.codelogs.server.domain.metrics;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatMeasures;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A ProjectAnalysis.
 */
@Entity
@Table(name = "code_analysis")
public class CodeAnalysis implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid",unique = true)
    private String uuid;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @Column(name = "last_update_date")
    private ZonedDateTime lastUpdateDate;

    @Column(name = "time_execution")
    private Long timeExecution;

    @OneToMany(mappedBy = "codeAnalysis",fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<HeartBeatMeasures> heartBeatMeasures = new HashSet<>();

    @OneToOne
    @JoinColumn(unique = true)
    private Project project;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(ZonedDateTime lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Long getTimeExecution() {
        return timeExecution;
    }

    public void setTimeExecution(Long timeExecution) {
        this.timeExecution = timeExecution;
    }

    public Set<HeartBeatMeasures> getHeartBeatMeasures() {
        return heartBeatMeasures;
    }

    public void setHeartBeatMeasures(Set<HeartBeatMeasures> heartBeatMeasures) {
        this.heartBeatMeasures = heartBeatMeasures;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CodeAnalysis codeAnalysis = (CodeAnalysis) o;
        if(codeAnalysis.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, codeAnalysis.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "ProjectAnalysis{" +
            "id=" + id +
            ", uuid='" + uuid + "'" +
            ", createDate='" + createDate + "'" +
            ", lastUpdateDate='" + lastUpdateDate + "'" +
            ", timeExecution='" + timeExecution + "'" +
            '}';
    }
}
