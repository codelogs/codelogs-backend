package ec.edu.utpl.datalab.codelogs.server.web.rest_open.heartbeats;

import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.HeartBeatCodeRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.lifecycle.HeartBeatCodeMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.HeartBeatCodeDto;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
public class HeartBeatCodeOpenApiImpl implements HeartBeatCodeOpenApi {
    private HeartBeatCodeRepository beatCodeRepository;
    private HeartBeatCodeMapper codeMapper;

    @Inject
    public HeartBeatCodeOpenApiImpl(HeartBeatCodeRepository beatCodeRepository, HeartBeatCodeMapper codeMapper) {
        this.beatCodeRepository = beatCodeRepository;
        this.codeMapper = codeMapper;
    }

    @Override
    public ResponseEntity<Collection<HeartBeatCodeDto>> getBeatsCode(@PathVariable("project_uuid") String project_uuid, Pageable pageable) throws URISyntaxException {
        Collection<HeartBeatCodeDto> dtos = beatCodeRepository.findAllByWorksession(project_uuid,pageable)
            .getContent()
            .stream()
            .map(codeMapper::map)
            .collect(Collectors.toList());
        return ResponseEntity.ok(dtos);
    }
}
