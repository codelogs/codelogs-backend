package ec.edu.utpl.datalab.codelogs.server.repository.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.WorkSessionLessZTM;

import java.util.List;

/**
 * * Created by rfcardenas
 */
public interface WorkSessionRangeRepo {
    List<WorkSessionLessZTM> workSession(String project, String startDate, String endDate);
}
