package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.time;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimeStatusProgrammerDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * * Created by rfcardenas
 */
@RequestMapping("/api/v1/time/states")
@Api(value = "/api/v1/time/states",
    authorizations = {},
    tags = {"time"},
    description = "Proporciona informacion sobre los estados de tiempo inactivo y estados de tiempo activos de programación")
public interface TimeStateCoreApi {
    @ApiOperation(
        value = "Retorna los estados de un programdor, el tiempo que ha estado activo o inactivo en una sesion" +
            "de un proyecto",
        nickname = "estados programador",
        response = TimeStatusProgrammerDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}/global",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    TimeStatusProgrammerDto timeStateUserGlobal(
        @PathVariable("user") String user,
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );

    @ApiOperation(
        value = "Retorna los estados de un programdor, el tiempo que ha estado activo o inactivo en el desarrollo de un proyecto" +
            "de un proyecto",
        nickname = "estados del programador",
        response = TimeStatusProgrammerDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/users/{user}/projects/{project}",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    TimeStatusProgrammerDto timeStateUserProject(
        @PathVariable("user") String user,
        @PathVariable("project") String project,
        @RequestParam("start") String start,
        @RequestParam("end") String end
    );
}
