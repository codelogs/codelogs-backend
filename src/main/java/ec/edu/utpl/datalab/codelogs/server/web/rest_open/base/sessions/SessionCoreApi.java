package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.sessions;

import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle.WorkSessionDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URISyntaxException;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/sessions/",
    authorizations = {},
    tags = {"sessions"},
    description = "Sesiones de trabajo")
@RequestMapping("/api/v1/sessions/")
@ExposesResourceFor(WorkSession.class)
public interface SessionCoreApi {
    /**
     * retorna las sesiones de programacion con un alcance global
     * @param user
     * @param pageable
     * @return
     * @throws URISyntaxException
     */
    @ApiOperation(
        value = "retorna las sessiones de programación de un programador",
        nickname = "sessiones",
        response = WorkSessionDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping(value = "/user/{user}/all",
        produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<WorkSessionDto>> getAllSessions(@PathVariable(name = "user") String user, Pageable pageable) throws URISyntaxException;

    /**
     * Retorna las sesiones de trabajo recientes de un usuario
     * @param user
     * @param pageable
     * @return
     * @throws URISyntaxException
     */
    @ApiOperation(
        value = "retorna las sessiones de programación mas recientes de un programador",
        nickname = "sessiones",
        response = WorkSessionDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping(value = "/user/{user}/recent",
        produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<WorkSessionDto>> getSessionsRecent(@PathVariable("user") String user, Pageable pageable) throws URISyntaxException;

    /**
     * metodo que retorna las sesiones de un proyecto
     * prooject uuid es identificador universal unico
     * @param project_uuid
     * @param pageable
     * @return
     * @throws URISyntaxException
     */
    @ApiOperation(
        value = "retorna las sessiones de programación correspondiente a un programa (proyecto)",
        nickname = "sessiones",
        response = WorkSessionDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping(value = "project/{project_uuid}/all",
        produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<List<WorkSessionDto>> getSessionsProject(@PathVariable("project_uuid") String project_uuid, Pageable pageable) throws URISyntaxException;

    /**
     *
     * @param session_uuid
     * @return
     * @throws URISyntaxException
     */
    @ApiOperation(
        value = "retorna detalles de una session de trabajo",
        nickname = "sessiones",
        response = WorkSessionDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @GetMapping(value = "/{session_uuid}",
        produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<WorkSessionDto> getSessionsByUuid(@PathVariable("session_uuid") String session_uuid) throws URISyntaxException;
}
