package ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.FileCode;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextRct;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.FileCodeDto;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.FileCodePack;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    uses = {GrantedContextMapper.class,GrantedContextRct.class},
    unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FileCodeMapper {
    FileCode map(FileCodeDto source);
    FileCodeDto map(FileCode source);

    @Mappings({
        @Mapping(source = "resource_id",target = "id",ignore = true),
        @Mapping(source = "project",target = "project",ignore = true)
    })
    FileCode map(FileCodePack source);

}
