package ec.edu.utpl.datalab.codelogs.server.repository.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeDto;

import java.util.List;

/**
 * * Created by rfcardenas
 */
public interface LoggTimeRepository {
    /**
     *
     * @param username
     * @param fromDate
     * @param toDate
     * @return
     */
    List<LoggTimeDto> customRangeLoggTime(String username, String fromDate, String toDate);

    /**
     *
     * @param username
     * @param project
     * @param fromDate
     * @param toDate
     * @return
     */

    List<LoggTimeDto> customRangeLoggTimeProject(String username, String project, String fromDate, String toDate);
}
