package ec.edu.utpl.datalab.codelogs.server.web.rest_spyder;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.FileCode;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Languaje;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.exceptions.GrantedContextInvalid;
import ec.edu.utpl.datalab.codelogs.server.exceptions.LangNotFoundException;
import ec.edu.utpl.datalab.codelogs.server.exceptions.ProjectNotFoundException;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.FileCodeRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.LanguajeRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.ProjectRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.GrantedContextMapper;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes.FileCodeMapper;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.FileCodePack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

/**
 * * Created by rfcardenas
 */
@RestController
public class FilesApiImpl implements FilesApi {
    private final Logger log = LoggerFactory.getLogger(FilesApiImpl.class);

    private FileCodeRepository fileCodeRepository;
    private LanguajeRepository languajeRepository;
    private FileCodeMapper filecodeMapper;
    private GrantedContextMapper contextMapper;
    private ProjectRepository projectRepository;
    @Inject
    public FilesApiImpl(FileCodeRepository fileCodeRepository,
                        ProjectRepository projectRepository,
                        LanguajeRepository languajeRepository,
                        FileCodeMapper filecodeMapper,
                        GrantedContextMapper contextMapper) {
        this.fileCodeRepository = fileCodeRepository;
        this.projectRepository = projectRepository;
        this.languajeRepository = languajeRepository;
        this.filecodeMapper = filecodeMapper;
        this.contextMapper = contextMapper;
    }

    @Override
    public ResponseEntity<GrantedContextPack> createFileNode(@RequestBody FileCodePack dto) throws URISyntaxException {
        System.out.println(dto);
        log.debug("Registrando FileCodeDTO");

        if(dto.getProject().getUuid() == null){
            log.error("La referencia de UUID granted proyecto no es valida");
            throw new GrantedContextInvalid();
        }
        //Buscar archivo para un proyecto --> por PATH
        Optional<FileCode> result=  fileCodeRepository.findByPathUser(dto.getProject().getUuid(),dto.getPathFile());

        if(result.isPresent()){
            log.info("Source code encontrado en db");
            return ResponseEntity.ok(contextMapper.mapToPack(result.get()));
        }else{
            log.warn("Source code no encontrado en db.. creando nueva referencia");
            log.info("Cargado proyecto....");
            Optional<Project> resProject = projectRepository.findOneByUuid(dto.getProject().getUuid());
            if(!resProject.isPresent()){
                log.info("Proyecto no encontrado... {} uuid project ref no valida" ,dto.getProject().getUuid());
                throw new ProjectNotFoundException();
            }
            FileCode sourcecode = filecodeMapper.map(dto);
            Languaje languaje = loadLanguaje(dto);
            if(languaje == null){
                throw new LangNotFoundException();
            }
            sourcecode.setProject(resProject.get());
            sourcecode.setUuid(UUID.randomUUID().toString());
            sourcecode.setLanguaje(languaje);

            fileCodeRepository.save(sourcecode);

            return ResponseEntity.ok(contextMapper.mapToPack(sourcecode));
        }
    }
    private Languaje loadLanguaje(FileCodePack fileCodeDto){
        Languaje languaje = null;
        Optional<Languaje> languajeDb = languajeRepository.findByExtension(fileCodeDto.getExtension());
        if(!languajeDb.isPresent()){
            languaje = new Languaje();
            languaje.setCreateDate(ZonedDateTime.now());
            languaje.setExtension(fileCodeDto.getExtension());
            languaje.setNombre("No catalogado");
            languaje.setUuid(UUID.randomUUID().toString());
            languaje = languajeRepository.save(languaje);
        }else{
            languaje = languajeDb.get();
        }
        return languaje;
    }
}
