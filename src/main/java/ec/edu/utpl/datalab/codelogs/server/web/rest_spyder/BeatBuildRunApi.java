package ec.edu.utpl.datalab.codelogs.server.web.rest_spyder;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatBuildRunPack;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.net.URISyntaxException;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/spyder",
    authorizations = {},
    tags = {"spyder"},
    description = "API interactuar servidor con el monitor spyder")
@RequestMapping("api/v1/spyder")
public interface BeatBuildRunApi {
    @ApiOperation(
        value = "crea un pulso de compilacion/ejecucion de codigo, contiene datos sobre el comportamiento",
        nickname = "heartbeatBuildRun",
        response = GrantedContextDto.class,
        responseContainer = "Array",
        httpMethod = "POST",
        protocols = "HTTP"
    )
    @PostMapping(value = "/beat_run_compile",
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<GrantedContextPack> unitRuns(
        @RequestBody HeartBeatBuildRunPack data
    ) throws URISyntaxException;
}
