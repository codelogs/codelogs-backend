package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.time;

import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.TimeStatusProgrammerDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;

/**
 * * Created by rfcardenas
 */
@RestController
public class TimeStateUserApiImpl implements TimeStateUserApi {
    private TimeStateCoreApi timeStateCoreApi;

    @Inject
    public TimeStateUserApiImpl(TimeStateCoreApi timeStateCoreApi) {
        this.timeStateCoreApi = timeStateCoreApi;
    }

    @Override
    public TimeStatusProgrammerDto timeStateCurrentUserGlobal(@RequestParam("start") String start, @RequestParam("end") String end) {
        String username = SecurityUtils.getCurrentUserLogin();
        return timeStateCoreApi.timeStateUserGlobal(username,start,end);
    }

    @Override
    public TimeStatusProgrammerDto timeStateCurrentUserProject(@PathVariable("project") String project, @RequestParam("start") String start, @RequestParam("end") String end) {
        String username = SecurityUtils.getCurrentUserLogin();
        return timeStateCoreApi.timeStateUserProject(username,project,start,end);
    }
}
