package ec.edu.utpl.datalab.codelogs.server.repository.reports;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LoggTimeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@Repository
public class LoggTimeRepositoryDefault implements LoggTimeRepository {
    private static final Logger log = LoggerFactory.getLogger(LoggTimeRepositoryDefault.class);


    private final String CUSTOM_RANGE_LOGG_TIME_PR = "SELECT (SESSION_START) :: date as DATE , sum( ACCUM_TIME) TIME_SECONDS\n" +
        "FROM WORK_SESSION WS \n" +
        "JOIN PROJECT P  ON WS.PROJECT_ID = P.ID \n" +
        "JOIN PROGRAMMER PR   ON PR.ID  = P.PROGRAMMER_ID \n" +
        "WHERE SESSION_START::date >= :fromDate AND SESSION_START::date <= :toDate \n"+
        "AND USERNAME = :username \n" +
        "group by (DATE)";


    private final String CUSTOM_RANGE_LOGG_TIME_PROJECT = "SELECT (SESSION_START) :: date as DATE , sum( ACCUM_TIME) TIME_SECONDS\n" +
        "FROM WORK_SESSION WS \n" +
        "JOIN PROJECT P  ON WS.PROJECT_ID = P.ID \n" +
        "JOIN PROGRAMMER PR   ON PR.ID  = P.PROGRAMMER_ID \n" +
        "WHERE (SESSION_START::date >= :fromDate AND SESSION_START::date <= :toDate)\n"+
        "AND (USERNAME = :username  AND P.UUID = :projectid) \n" +
        "group by (DATE)";


    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Inject
    public LoggTimeRepositoryDefault(NamedParameterJdbcTemplate namedParameterJdbcTemplate) {
        this.namedParameterJdbcTemplate = namedParameterJdbcTemplate;
    }


    @Override
    public List<LoggTimeDto> customRangeLoggTime(String username, String fromDate, String toDate) {
        log.info("Query custom range logg time user {}  start {} end {}" ,username,fromDate,toDate);

        RowMapper<LoggTimeDto> rowMapper = (rs, rowNum) -> mapResult(rs, rowNum, username);
        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("username", username);
        queryParams.addValue("fromDate", fromDate, Types.DATE);
        queryParams.addValue("toDate", toDate,Types.DATE);
        return namedParameterJdbcTemplate
            .query(CUSTOM_RANGE_LOGG_TIME_PR, queryParams, rowMapper);
    }


    @Override
    public List<LoggTimeDto> customRangeLoggTimeProject(String username, String project, String fromDate, String toDate) {
        log.info("Query custom range logg time user  in project{}  start {} end {}" ,username,fromDate,toDate);
        RowMapper<LoggTimeDto> rowMapper = (rs, rowNum) -> mapResult(rs, rowNum, username);
        MapSqlParameterSource queryParams = new MapSqlParameterSource();
        queryParams.addValue("username", username);
        queryParams.addValue("fromDate", fromDate, Types.DATE);
        queryParams.addValue("toDate", toDate,Types.DATE);
        queryParams.addValue("projectid", project);


        return namedParameterJdbcTemplate
            .query(CUSTOM_RANGE_LOGG_TIME_PROJECT, queryParams, rowMapper);
    }


    /**
     * Mapear el resultado de DB al DTO
     *
     * @param rs
     * @param rowNum
     * @param username
     * @return
     */
    private LoggTimeDto mapResult(ResultSet rs, int rowNum, String username) {
        LoggTimeDto loggTimeDto = new LoggTimeDto();
        try {
            loggTimeDto.setProgrammer(username);
            loggTimeDto.setDate(rs.getString(1));
            loggTimeDto.setTime_seconds(rs.getInt(2));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return loggTimeDto;
    }


}
