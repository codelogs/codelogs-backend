package ec.edu.utpl.datalab.codelogs.server.service.transfers.reports;


import com.fasterxml.jackson.annotation.JsonProperty;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;

import java.util.List;

/**
 * * Created by rfcardenas
 */
public class LoggTimeProjectDto   {

    private String user_owner;

    private ProjectDto project;

    private List<LoggTimeDto> timeDtoList;

    @JsonProperty("owner")
    public String getUser_owner() {
        return user_owner;
    }

    public void setUser_owner(String user_owner) {
        this.user_owner = user_owner;
    }

    @JsonProperty("project")
    public ProjectDto getProject() {
        return project;
    }

    public void setProject(ProjectDto project) {
        this.project = project;
    }

    @JsonProperty("log_time")
    public List<LoggTimeDto> getTimeDtoList() {
        return timeDtoList;
    }

    public void setTimeDtoList(List<LoggTimeDto> timeDtoList) {
        this.timeDtoList = timeDtoList;
    }



}
