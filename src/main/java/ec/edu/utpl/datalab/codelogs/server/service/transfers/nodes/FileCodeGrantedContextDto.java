package ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * * Created by rfcardenas
 */
public class FileCodeGrantedContextDto extends GrantedContextDto {
    @NotEmpty
    private String pathFile;

    public String getPathFile() {
        return pathFile;
    }

    public void setPathFile(String pathFile) {
        this.pathFile = pathFile;
    }
}
