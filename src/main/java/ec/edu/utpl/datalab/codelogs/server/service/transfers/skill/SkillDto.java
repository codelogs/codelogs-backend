package ec.edu.utpl.datalab.codelogs.server.service.transfers.skill;

import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.LanguajeDto;
import org.springframework.hateoas.ResourceSupport;

import java.time.ZonedDateTime;

/**
 * * Created by rfcardenas
 */
public class SkillDto extends ResourceSupport{
    private Long resource_id;
    private ZonedDateTime lastUpdate;
    private long total_usage;
    private LanguajeDto languaje;
    private LevelSkillDto levelSkill;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public ZonedDateTime getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(ZonedDateTime lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public long getTotal_usage() {
        return total_usage;
    }

    public void setTotal_usage(long total_usage) {
        this.total_usage = total_usage;
    }

    public LanguajeDto getLanguaje() {
        return languaje;
    }

    public void setLanguaje(LanguajeDto languaje) {
        this.languaje = languaje;
    }

    public LevelSkillDto getLevelSkill() {
        return levelSkill;
    }

    public void setLevelSkill(LevelSkillDto levelSkill) {
        this.levelSkill = levelSkill;
    }
}
