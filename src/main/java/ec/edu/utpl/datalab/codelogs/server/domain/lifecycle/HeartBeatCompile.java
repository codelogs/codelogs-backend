package ec.edu.utpl.datalab.codelogs.server.domain.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.enumeration.TYPE_INSTRUCTION;

import javax.persistence.*;
import java.time.ZonedDateTime;

/**
 * Entidad general representa el resultado de una compilacion
 * o una ejecucion un programa
 *
 * * Created by rfcardenas
 */
@Entity
@Table(name = "heart_beats_compile")
public class HeartBeatCompile {
    private static final long serialVersionUID = 1L;

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid",unique = true)
    private String uuid;

    //indicador del exito o fracaso
    @Column(name = "success")
    private boolean success;

    // Mensaje principal capturado
    @Column(name = "message_l1")
    private String messageL1 = "ejecución existosa";
    // Mensaje secundaior opcional
    @Column(name = "message_l2")
    private String messageL2 = "";
    // Mensaje global
    @Column(name = "message_l3")
    private String messageL3 = "";

    @Column(name = "time")
    private long time;

    @Column(name = "capturate_at")
    private ZonedDateTime capturate_at;

    @Column(name ="type_instruction")
    private TYPE_INSTRUCTION instruction;

    @ManyToOne
    private WorkSession workSession;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessageL1() {
        return messageL1;
    }

    public void setMessageL1(String messageL1) {
        this.messageL1 = messageL1;
    }

    public String getMessageL2() {
        return messageL2;
    }

    public void setMessageL2(String messageL2) {
        this.messageL2 = messageL2;
    }

    public String getMessageL3() {
        return messageL3;
    }

    public void setMessageL3(String messageL3) {
        this.messageL3 = messageL3;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public ZonedDateTime getCapturate_at() {
        return capturate_at;
    }

    public void setCapturate_at(ZonedDateTime capturate_at) {
        this.capturate_at = capturate_at;
    }

    public WorkSession getWorkSession() {
        return workSession;
    }

    public void setWorkSession(WorkSession workSession) {
        this.workSession = workSession;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public HeartBeatCompile() {
    }

    public TYPE_INSTRUCTION getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION instruction) {
        this.instruction = instruction;
    }

    @Override
    public String toString() {
        return "ResultUnitRun{" +
            "id=" + id +
            ", success=" + success +
            ", messageL1='" + messageL1 + '\'' +
            ", messageL2='" + messageL2 + '\'' +
            ", messageL3='" + messageL3 + '\'' +
            ", time=" + time +
            ", capturate_at=" + capturate_at +
            '}';
    }
}
