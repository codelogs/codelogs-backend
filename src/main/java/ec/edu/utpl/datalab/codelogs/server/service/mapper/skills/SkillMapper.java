package ec.edu.utpl.datalab.codelogs.server.service.mapper.skills;

import ec.edu.utpl.datalab.codelogs.server.domain.programmer_skills.Skill;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.nodes.LanguajeMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.skill.SkillDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
 * * Created by rfcardenas
 */
@Mapper(componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    uses = {LevelSkillMapper.class,LanguajeMapper.class})
public interface SkillMapper {
    @Mappings({
        @Mapping(source = "resource_id",target = "id",ignore = false),
    })
    Skill map(SkillDto source);
    @Mappings({
        @Mapping(source = "id",target = "resource_id",ignore = false),
    })
    SkillDto map(Skill source);
}
