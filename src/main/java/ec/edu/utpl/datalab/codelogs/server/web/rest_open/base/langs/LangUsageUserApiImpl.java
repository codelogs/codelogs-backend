package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.langs;

import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LanguajeUsageDto;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class LangUsageUserApiImpl implements LangUsageUserApi {
    private LangUsageCoreApi langUsageCoreApi;

    @Inject
    public LangUsageUserApiImpl(LangUsageCoreApi langUsageCoreApi) {
        this.langUsageCoreApi = langUsageCoreApi;
    }

    @Override
    public List<LanguajeUsageDto> getLangUsageCurrentUser(@RequestParam(value = "start") String startDate, @RequestParam(value = "end") String endDate) {
        String currentUser = SecurityUtils.getCurrentUserLogin();
        return langUsageCoreApi.getLangUsage(currentUser,startDate,endDate);
    }

    @Override
    public List<LanguajeUsageDto> getLangUsageCurrentUser(@PathVariable(value = "project") String project, @RequestParam(value = "start") String startDate, @RequestParam(value = "end") String endDate) {
        String currentUser = SecurityUtils.getCurrentUserLogin();
        return langUsageCoreApi.getLangUsage(currentUser,project,startDate,endDate);
    }
}
