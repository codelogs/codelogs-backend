package ec.edu.utpl.datalab.codelogs.server.repository.nodes;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Languaje;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Spring Data JPA repository for the Languaje entity.
 */
@SuppressWarnings("unused")
public interface LanguajeRepository extends JpaRepository<Languaje,Long> {
    Optional<Languaje> findByExtension(String extension);
}
