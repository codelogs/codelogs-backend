package ec.edu.utpl.datalab.codelogs.server.web.rest_spyder;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.GrantedContextDto;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.CodeAnalysisPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URISyntaxException;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/spyder",
    authorizations = {},
    tags = {"spyder"},
    description = "API interactuar servidor con el monitor spyder")
@RequestMapping("api/v1/spyder")
public interface HeartBeatCodeAnalysis {
    @ApiOperation(
        value = "Crea una entrada en el análisis de código del programa, métricas de código recolectadas",
        nickname = "code_analysis",
        response = GrantedContextDto.class,
        responseContainer = "Array",
        httpMethod = "POST",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/code_analysis",
        method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_VALUE)
    @Timed
    ResponseEntity<GrantedContextPack> unitRuns(
        @RequestBody CodeAnalysisPack data
    ) throws URISyntaxException;
}
