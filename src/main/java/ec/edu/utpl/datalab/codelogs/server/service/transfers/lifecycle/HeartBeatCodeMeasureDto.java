package ec.edu.utpl.datalab.codelogs.server.service.transfers.lifecycle;

import ec.edu.utpl.datalab.codelogs.server.domain.metrics.Metric;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.enumeations.TYPE_INSTRUCTION_PUBLIC;

import java.util.Objects;

/**
 * * Created by rfcardenas
 */
public class HeartBeatCodeMeasureDto {
    private static final long serialVersionUID = 1L;

    private Long resource_id;
    private String uuid;
    private Double value;
    private TYPE_INSTRUCTION_PUBLIC instruction;
    private Metric metric;

    public Long getResource_id() {
        return resource_id;
    }

    public void setResource_id(Long resource_id) {
        this.resource_id = resource_id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public TYPE_INSTRUCTION_PUBLIC getInstruction() {
        return instruction;
    }

    public void setInstruction(TYPE_INSTRUCTION_PUBLIC instruction) {
        this.instruction = instruction;
    }

    public Metric getMetric() {
        return metric;
    }

    public void setMetric(Metric metric) {
        this.metric = metric;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        HeartBeatCodeMeasureDto beat = (HeartBeatCodeMeasureDto) o;
        if(beat.resource_id == null || resource_id == null) {
            return false;
        }
        return Objects.equals(resource_id, beat.resource_id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(resource_id);
    }

    @Override
    public String toString() {
        return "HeartBeatCodeMeasureDto{" +
            "resource_id=" + resource_id +
            ", uuid='" + uuid + '\'' +
            ", value=" + value +
            ", instruction=" + instruction +
            ", metric=" + metric +
            '}';
    }
}
