package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.projects;

import com.codahale.metrics.annotation.Timed;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.nodes.ProjectDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.ExposesResourceFor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URISyntaxException;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@Api(value = "/api/v1/projects",
    authorizations = {},
    tags = {"projects"},
    description = "Projects")
@RequestMapping("/api/v1/projects")
@ExposesResourceFor(Project.class)
public interface ProjectUserApi {

    @ApiOperation(
        value = "retorna todos los proyectos del usuario actualmente logeado",
        nickname = "projects",
        response = ProjectDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/all",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)

    @Timed
    ResponseEntity<List<ProjectDto>> getAllProjects(Pageable pageable) throws URISyntaxException;


    @ApiOperation(
        value = "retorna los proyectos recientes del usuario  actualmente logeado",
        nickname = "projects",
        response = ProjectDto.class,
        httpMethod = "GET",
        protocols = "HTTP"
    )
    @RequestMapping(value = "/recent",
        method = RequestMethod.GET,
        produces = MediaType.APPLICATION_JSON_VALUE)

    @Timed
    ResponseEntity<List<ProjectDto>> getRecentProjects(Pageable pageable) throws URISyntaxException;
}
