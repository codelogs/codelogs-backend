package ec.edu.utpl.datalab.codelogs.server.web.rest_spyder;

import ec.edu.utpl.datalab.codelogs.server.domain.enumeration.TYPE_INSTRUCTION;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.HeartBeatMeasures;
import ec.edu.utpl.datalab.codelogs.server.domain.metrics.CodeAnalysis;
import ec.edu.utpl.datalab.codelogs.server.domain.metrics.Metric;
import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import ec.edu.utpl.datalab.codelogs.server.repository.lifecycle.HeartBeatCodeMeasureRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.metrics.CodeAnalysisRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.metrics.MetricRepository;
import ec.edu.utpl.datalab.codelogs.server.repository.nodes.ProjectRepository;
import ec.edu.utpl.datalab.codelogs.server.security.SecurityUtils;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.CodeAnalysisPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.GrantedContextPack;
import ec.edu.utpl.datalab.codelogs.spyder.libs.transfers.HeartBeatCodeMeasurePack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.net.URISyntaxException;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * * Created by rfcardenas
 */
@RestController
public class HeartBeatCodeAnalysisImpl implements HeartBeatCodeAnalysis {
    private final Logger log = LoggerFactory.getLogger(HeartBeatCodeAnalysisImpl.class);


    private CodeAnalysisRepository analysisRepository;
    private ProjectRepository projectRepository;
    private MetricRepository metricRepository;
    private HeartBeatCodeMeasureRepository measureRepository;

    @Inject
    public HeartBeatCodeAnalysisImpl(CodeAnalysisRepository analysisRepository, ProjectRepository projectRepository, MetricRepository metricRepository, HeartBeatCodeMeasureRepository measureRepository) {
        this.analysisRepository = analysisRepository;
        this.projectRepository = projectRepository;
        this.metricRepository = metricRepository;
        this.measureRepository = measureRepository;
    }

    @Override
    public ResponseEntity<GrantedContextPack> unitRuns(@RequestBody CodeAnalysisPack data) throws URISyntaxException {
        log.info("Persitiendo analisis project");
        String currentUser = SecurityUtils.getCurrentUserLogin();
        String pathProject = data.getPathContext();
        CodeAnalysis projectAnalysis = null;
        Optional<Project> resultProject = projectRepository.findByPathUser(pathProject,currentUser);
        if(resultProject.isPresent()){
            log.info("Project granted");
            List<HeartBeatMeasures> arrayList = new ArrayList<>();
            projectAnalysis = recoverAnalysis(resultProject.get());
            for (HeartBeatCodeMeasurePack measureDto : data.getHeartBeatCodeMeasures()) {
                log.info("Registrando medición " , measureDto);
                Optional<Metric> resultMetric = metricRepository.findOneByuuid(measureDto.getMetric().getUuid());
                if(resultMetric.isPresent()){
                    Metric metric = resultMetric.get();
                    HeartBeatMeasures measure = getMeasureRef(metric,projectAnalysis);
                    measure.setValue(measureDto.getValue());
                    measure.setInstruction(TYPE_INSTRUCTION.HEART_BEAT_CODE_ANALYSIS);
                    measureRepository.save(measure);
                }else{
                    log.debug("La metrica no esta en el servidor ",measureDto);
                }
            }
        }else{
            log.error("El proyecto no fue encontrado en disco  " + data.getPathContext());
        }
        return ResponseEntity.ok().body(new GrantedContextPack(projectAnalysis.getUuid()));
    }
    private HeartBeatMeasures getMeasureRef(Metric metric, CodeAnalysis analysis)
    {
        System.out.println(" LA -> " + metric.getId());
        System.out.println(" LA -> " + analysis.getId());
        Optional<HeartBeatMeasures> result = measureRepository.findMeasureByMetricAndAnalysis(metric.getId(),analysis.getId());
        if(result.isPresent()){
            return result.get();
        }else{
            HeartBeatMeasures measure = new HeartBeatMeasures();
            measure.setMetric(metric);
            measure.setCodeAnalysis(analysis);
            return measure;
        }
    }

    private CodeAnalysis recoverAnalysis(Project project)
    {
        Optional<CodeAnalysis> result = analysisRepository.findAnalysisByProject(project.getId());
        if(!result.isPresent()){
            CodeAnalysis projectAnalysis = new CodeAnalysis();
            projectAnalysis.setCreateDate(ZonedDateTime.now());
            projectAnalysis.setLastUpdateDate(ZonedDateTime.now());
            projectAnalysis.setUuid(UUID.randomUUID().toString());
            projectAnalysis.setProject(project);
            return  analysisRepository.save(projectAnalysis);
        }else{
            result.get().setLastUpdateDate(ZonedDateTime.now());
            return analysisRepository.save(result.get());
        }
    }

}
