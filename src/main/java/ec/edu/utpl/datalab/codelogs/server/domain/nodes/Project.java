package ec.edu.utpl.datalab.codelogs.server.domain.nodes;

import com.fasterxml.jackson.annotation.JsonIgnore;
import ec.edu.utpl.datalab.codelogs.server.domain.lifecycle.WorkSession;
import ec.edu.utpl.datalab.codelogs.server.domain.security.Programmer;

import javax.persistence.*;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A Project.
 */
@Entity
@Table(name = "project")
public class Project implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "uuid",unique = true)
    private String uuid;

    @Column(name = "name")
    private String name;

    @Column(name = "path")
    private String path;

    @Column(name = "create_date")
    private ZonedDateTime createDate;

    @Column(name = "last_access")
    private ZonedDateTime lastAccess;

    @Column(name = "git")
    private String git;

    @ManyToOne
    private Programmer programmer;

    @OneToMany(mappedBy = "project")
    @JsonIgnore
    private Set<WorkSession> sessions = new HashSet<>();

    @OneToMany(mappedBy = "project")
    @JsonIgnore
    private Set<FileCode> files = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ZonedDateTime getCreateDate() {
        return createDate;
    }

    public void setCreateDate(ZonedDateTime createDate) {
        this.createDate = createDate;
    }

    public ZonedDateTime getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(ZonedDateTime lastAccess) {
        this.lastAccess = lastAccess;
    }

    public String getGit() {
        return git;
    }

    public void setGit(String git) {
        this.git = git;
    }

    public Programmer getProgrammer() {
        return programmer;
    }

    public void setProgrammer(Programmer programmer) {
        this.programmer = programmer;
    }

    public Set<WorkSession> getSessions() {
        return sessions;
    }

    public void setSessions(Set<WorkSession> workSessions) {
        this.sessions = workSessions;
    }

    public Set<FileCode> getFiles() {
        return files;
    }

    public void setFiles(Set<FileCode> fileCodes) {
        this.files = fileCodes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Project project = (Project) o;
        if(project.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, project.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Project{" +
            "id=" + id +
            ", uuid='" + uuid + "'" +
            ", name='" + name + "'" +
            ", path='" + path + "'" +
            ", createDate='" + createDate + "'" +
            ", lastAccess='" + lastAccess + "'" +
            ", git='" + git + "'" +
            '}';
    }
}
