package ec.edu.utpl.datalab.codelogs.server.repository.reports;


import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.FileSummaryTimeDto;

import java.util.List;

/**
 * * Created by rfcardenas
 */
public interface SummaryWorkFileRepo {
    List<FileSummaryTimeDto> getSummaryLastWorkFile(String uuid_project);
}
