package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.langs;

import ec.edu.utpl.datalab.codelogs.server.repository.reports.LanguajeUsageRepository;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.reports.LanguajeUsageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;

/**
 * * Created by rfcardenas
 */
@RestController
public class LangUsageCoreApiImpl implements LangUsageCoreApi {
    private final Logger log = LoggerFactory.getLogger(LangUsageCoreApiImpl.class);

    private LanguajeUsageRepository languajeUsageRepository;

    @Inject
    public LangUsageCoreApiImpl(LanguajeUsageRepository languajeUsageRepository) {
        this.languajeUsageRepository = languajeUsageRepository;
    }

    @Override
    public List<LanguajeUsageDto> getLangUsage(@PathVariable(value = "user") String user, @RequestParam(value = "start") String startDate, @RequestParam(value = "end") String endDate) {
        return languajeUsageRepository.usageLangWorkGlobalRange(user,startDate,endDate);
    }

    @Override
    public List<LanguajeUsageDto> getLangUsage(@PathVariable(value = "user") String user, @PathVariable(value = "project") String project, @RequestParam(value = "start") String startDate, @RequestParam(value = "end") String endDate) {
        return languajeUsageRepository.usageLangWorkProjectRange(user,project,startDate,endDate);
    }
}
