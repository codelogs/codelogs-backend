package ec.edu.utpl.datalab.codelogs.server.repository.nodes;

import ec.edu.utpl.datalab.codelogs.server.domain.nodes.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

/**
 * * Created by rfcardenas
 */
public interface ProjectRepository extends JpaRepository<Project,Long> {
    @Query("select p from Project p left join p.programmer dv  where p.path = :path and dv.username=:username")
    Optional<Project> findByPathUser(@Param("path") String path, @Param("username") String username);

    @Query("select p from Project p left join p.programmer dv  where p.name like :name and dv.username=:username")
    List<Project> findByNameUser(@Param("name") String name, @Param("username") String username);

    @Query("select p from Project p left join p.programmer dv  where dv.username=:username AND p.uuid=:project_uid")
    Optional<Project> findProjectByUuidAndUser(@Param("username") String username, @Param("project_uid") String project_uid);

    Optional<Project> findOneByUuid(String uuid);

    @Query("select p from Project p left join p.programmer dv where dv.username=:username")
    Page<Project> findAllFromUser(@Param("username") String username, Pageable pageable);

    @Query("select p from Project p left join p.programmer dv where dv.username=:username")
    List<Project> findAllFromUser(@Param("username") String username);
}
