package ec.edu.utpl.datalab.codelogs.server.web.rest_open.base.skills;

import ec.edu.utpl.datalab.codelogs.server.repository.programmer_skills.SkillRepository;
import ec.edu.utpl.datalab.codelogs.server.service.mapper.skills.SkillMapper;
import ec.edu.utpl.datalab.codelogs.server.service.transfers.skill.SkillDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.util.List;
import java.util.stream.Collectors;

/**
 * * Created by rfcardenas
 */
@RestController
public class SkillsAwardedCoreApiImpl implements SkillsAwardedCoreApi {
    private final Logger log = LoggerFactory.getLogger(SkillsAwardedCoreApiImpl.class);

    private SkillRepository skillRepository;
    private SkillMapper skillMapper;

    @Inject
    public SkillsAwardedCoreApiImpl(SkillRepository skillRepository, SkillMapper skillMapper) {
        this.skillRepository = skillRepository;
        this.skillMapper = skillMapper;
    }


    @Override
    public List<SkillDto> getAllSkills(@PathVariable("user") String user) {
        log.info("Petición habilidades de usuario {}",user);
        return skillRepository.findAllSkillsProgrammer(user).stream()
            .map(skillMapper::map)
            .collect(Collectors.toList());
    }
}
