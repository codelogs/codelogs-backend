# Estado
[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)
[![Gitter](https://img.shields.io/gitter/room/nwjs/nw.js.svg)](https://gitter.im/codelogs-fragent/Lobby?utm_source=share-link&utm_medium=link&utm_campaign=share-link)
[![Packagist](https://img.shields.io/packagist/l/doctrine/orm.svg)](https://opensource.org/licenses/MIT)
[![coverage](https://gitlab.com/codelogs/codelogs-backend/badges/master/build.svg)](https://gitlab.com/codelogs/codelogs-backend/commits/master)

[![Deploy to Heroku](https://camo.githubusercontent.com/c0824806f5221ebb7d25e559568582dd39dd1170/68747470733a2f2f7777772e6865726f6b7563646e2e636f6d2f6465706c6f792f627574746f6e2e706e67)](https://heroku.com/deploy?template=https://gitlab.com/codelogs/codelogs-backend/tree/master)

# Servidor

Esta aplicación se ha desarrollado para el almacenamiento de datos implementa una API REST para interactuar con el programa encargado de recolectar los datos instalado en la maquina del programador. ("Programa en la maquina del estudiante"


# Demo

https://codelogsopensource.herokuapp.com/#/

Cuenta del proyecto y Apis vinculadas (Facebook, Google)
mail codelogsvm@gmail.com
Contraseña codelogs92

# Importante

Si el demo no esta disponible, puede deberse a que Heroku mantiene vivas a las 
aplicaciones por 30 minutos, si no se detecta actvidad el servicio es destruido.

Al ser una cuenta gratuita el tiempo minimo de arranque no debe superar a los
60 segundos, cuando heroku esta muy congestionado puede no cargar la aplicación
adecuadamente utiliza las credenciales para reiniciar los dynos en heroku. o descarga el
war de las líneas de contrucción y empaquetado y ejecutalo localmente.

## Desarrollo

Los siguientes componentes son la base, para el desarrollo de lado del servidor, existe una versión mas
completa basada en microservicios la cual utiliza hazelcast para acelerar y procesar datos 
en memoria pero esta fuera del alcance de este TFT.

### Tecnología : 
 - Spring 
 - Hibernate
 - liquidbase
 - ElasticSearch
 - maven
 - Cache un solo nodo
 

## Construcción

Para construir el servidor en modo producción utiliza el perfil prod:
perfiles [dev, prod , swagger,liquidbase]
  

## Pruebas 

Ejecución de pruebas.

## Despliegue en heroku

## Enlaces de interés
[Using JHipster in development]: https://jhipster.github.io/documentation-archive/v3.10.0/development/
[Using Docker and Docker-Compose]: https://jhipster.github.io/documentation-archive/v3.10.0/docker-compose
[Using JHipster in production]: https://jhipster.github.io/documentation-archive/v3.10.0/production/
[Running tests page]: https://jhipster.github.io/documentation-archive/v3.10.0/running-tests/
[Setting up Continuous Integration]: https://jhipster.github.io/documentation-archive/v3.10.0/setting-up-ci/
[Gatling]: http://gatling.io/
[Node.js]: https://nodejs.org/
[Bower]: http://bower.io/
